find . -name "*.pyc" -exec rm -rf {} \;
find . -name "*.po~" -exec rm -rf {} \;
find . -name "*.DS_Store" -exec git rm -f {} \;
find . -name "*.csv#" -exec rm -rf {} \;
