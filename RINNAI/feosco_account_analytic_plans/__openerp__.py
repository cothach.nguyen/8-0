{
    'name': 'FEOSCO Accounting Plan',
    'version': '1.1',
    'author': 'Feosco',
    'category': 'Feosco',
    'sequence': 21,
    'website': 'http://www.feosco.com',
    'description': """
        Accounting Plan
    """,
    'author': 'Feosco',
    'website': 'http://www.feosco.com',
    'images': [
    ],
    'depends': ['account_analytic_plans'],
    'data': [
             'view/account_analytic_plan.xml'
    ],
    'demo': [],
    'test': [
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
    'css': [],
}