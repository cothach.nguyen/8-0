from openerp.osv import osv

class account_analytic_plan_instance(osv.osv):
    _inherit = "account.analytic.plan.instance"

    def search(self, cr, user, args, offset=0, limit=None, order=None, context={}, count=False):
        for arg in args:
            if arg and arg[0] and arg[0] == 'plan_id':
                args.remove(arg)
        res = super(account_analytic_plan_instance, self).search(cr, user, args, offset=offset, limit=limit, order=order,
                                                                 context=context, count=count)
        return res
