# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import datetime

from openerp import addons
import logging
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import tools

_logger = logging.getLogger(__name__)



class feosco_hr_business_trip(osv.osv):
    _name = "feosco.hr.business.trip"
    _description = "Employee Business Trip Management"
    _inherit = ['ir.needaction_mixin']
    
    def _employee_get(self, cr, uid, context=None):
        ids = self.pool.get('hr.employee').search(cr, uid, [('user_id', '=', uid)], context=context)
        if ids:
            return ids[0]
        return False
    
    def _department_get(self, cr, uid, context=None):
        employee_pool = self.pool.get('hr.employee')
        ids = employee_pool.search(cr, uid, [('user_id', '=', uid)], context=context)
        
        if ids:
            employee = employee_pool.browse(cr, uid, ids[0], context=context)
            return employee.department_id.id
        
        return False
        
    def _check_date(self, cr, uid, ids, context=None):
        business_trip_obj = self.browse(cr, uid, ids, context=context)
        for business_trip in business_trip_obj:
            if business_trip.start_date and business_trip.end_date and business_trip.start_date > business_trip.end_date:
                return False
        return True
    
    def _check_is_manager(self, cr, uid, ids, field, arg, context=None):
        res = dict.fromkeys(ids, False)
        for exit in self.browse(cr, uid, ids, context=context):
            if exit.employee_id.user_id.id == uid:
                res[exit.id] = True
        return res
    
    def _get_type(self, cr, uid, context=None):
        
        master_data_obj = self.pool.get('feosco.master.data')
        agrs = [
                ('type','=','business_trip_type')
                ]
        ids = master_data_obj.search(cr, uid, agrs, context=context)
        
        res = master_data_obj.read(cr, uid, ids, ['code', 'name'], context=context)
        
        res = [(r['code'], r['name']) for r in res]

        return res
        
    _columns = {
                'name': fields.char('Name', size=64),
                'employee_id': fields.many2one('hr.employee', 'Employee'),
                'department_id':fields.many2one('hr.department','Department'),
                'start_date': fields.date('From Date'),
                'end_date': fields.date('To Date'),
                'address': fields.many2one('res.partner', 'Address'),
                'purpose': fields.text('Purpose of trip'),
                'note': fields.text('Note'),
                'budget': fields.many2one('hr.expense.expense', 'Budget'),
#                 'budget':  fields.float('Budget'),
                'state': fields.selection([('new',u'New'),
                                           ('refuse',u'Refuse'),
                                           ('waiting_dept_head',u'Waiting Department Head Approve'),
                                           ('waiting_director',u'Waiting Director Approve'),
                                           ('approved',u'Approved')],
                                            'Status', readonly=True),
                'employee_is_manager': fields.function(_check_is_manager, type='boolean', string='Employee is manager'),
                'type': fields.selection(_get_type, 'Type'),
    }
    
    _constraints = [
                    (_check_date,'input to date again,please !', ['end_date']),
    ]
    
    
    _defaults = {
        'state':'new',
        'employee_id': _employee_get,
        'department_id': _department_get
    }
    
    def create(self, cr, uid, vals, context=None):
        
        vals.update({'state': 'waiting_dept_head'})
        
        return super(feosco_hr_business_trip, self).create(cr, uid, vals, context=context)
    
    def set_new(self, cr ,uid, ids, context=None):
        
        return self.write(cr, uid, ids, {'state': 'new'}, context=context)
    
    def set_dept_approved(self, cr ,uid, ids, context=None):
        
        return self.write(cr, uid, ids, {'state': 'waiting_director'}, context=context)
    
    def set_dir_approved(self, cr ,uid, ids, context=None):
        
        return self.write(cr, uid, ids, {'state': 'approved'}, context=context)
    
    def set_refuse(self, cr ,uid, ids, context=None):
        
        return self.write(cr, uid, ids, {'state': 'refuse'}, context=context)
    
    
    def onchange_employee_id(self, cr, uid, ids, employee_id, context=None):
        
        result = {'value':{}}
        if employee_id:
            employee_obj = self.pool.get('hr.employee').browse(cr, uid, employee_id, context = context)
        
            result['value'].update({'department_id': employee_obj.department_id.id})
        
        return result
    
    def _needaction_domain_get(self, cr, uid, context=None):
        dom = False
        user_pool = self.pool.get('res.users')
        emp_obj = self.pool.get('hr.employee')
        
        if user_pool.has_group(cr, uid, 'base.group_hr_manager') or user_pool.has_group(cr, uid, 'base.group_hr_user'):
            dom = [('state', '=', 'waiting_director')]
            
        elif user_pool.has_group(cr, uid, 'base.group_hr_dept_head'):
            employee_ids = emp_obj.search(cr, uid, [('department_id.manager_id.user_id.id', '=', uid)], context=context)
            if employee_ids:
                dom = [('state', '=','waiting_dept_head'),
                       ('employee_id', 'in', employee_ids)]
                
        elif user_pool.has_group(cr, uid, 'base.group_user'):
            employee_ids = emp_obj.search(cr, uid, [('user_id.id', '=', uid)], context=context)
            current_date = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
            dom = [('state', 'in', ('refuse','approved')),
                       ('employee_id', 'in', employee_ids),
                       ('start_date', '>=', current_date)]             
        return dom

feosco_hr_business_trip()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
