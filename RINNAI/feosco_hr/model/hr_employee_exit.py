# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from datetime import datetime


class feosco_hr_work_handover(osv.osv):
    _name = "feosco.hr.work.handover"
    _description = "Minutes of work handover when an employee terminate"
    
    _columns = {
                'name': fields.char('Content', size=32, help='Content',readonly=True,
                                                      states={
                                                              'new': [('readonly', False)]
                                                              }),
                'state': fields.selection([('new',u'New'), 
                                            ('done',u'Done'),],
                                             'Status', readonly=True),
                
                'employee_exit_id': fields.many2one('feosco.hr.employee.exit', 'Employee Exit',ondelete='cascade'),
                
                'responsible_user_id': fields.many2one('res.users', 'Responsible User', readonly=True),
                }
    
    _defaults = {
        'state':'new',
        }
    
    def set_done(self, cr, uid, ids, context=None):
        
        return self.write(cr, uid, ids, {'state': 'done', 'responsible_user_id': uid }, context=context)
    
feosco_hr_work_handover()

class feosco_hr_employee_exit(osv.osv):
    _name = "feosco.hr.employee.exit"
    _description = "Employee's exit information"
    _inherit = ['ir.needaction_mixin']
    
    def default_get(self, cr, uid, fields, context=None):
        if context is None:
            context = {}
        res = super(feosco_hr_employee_exit, self).default_get(cr, uid, fields, context=context)
        
        # Default feosco_employee_id
        if context.has_key('default_employee'):
            res.update({'feosco_employee_id': context.get('default_employee', False) })
            
        # Default work_handover_ids
        handover_pool = self.pool.get('feosco.hr.work.handover')
        handover_ids = handover_pool.search(cr, uid, [('employee_exit_id','=',False)], context=context)
        if handover_ids:
            default_handover_lst = []
            for handover in handover_pool.browse(cr, uid, handover_ids, context=context):
                default_handover_lst.append((0,0,{'name': handover.name, 'state': handover.state}))
                
            res.update({'work_handover_ids': default_handover_lst })
            
        return res
    
    def _get_exit_type(self, cr, uid, context=None):
        
        master_data_obj = self.pool.get('feosco.master.data')
        agrs = [
                ('type','=','exit_type')
                ]
        ids = master_data_obj.search(cr, uid, agrs, context=context)
        
        res = master_data_obj.read(cr, uid, ids, ['code', 'name'], context=context)
        
        res = [(r['code'], r['name']) for r in res]

        return res
    
    def _employee_get(self, cr, uid, context=None):
        ids = self.pool.get('hr.employee').search(cr, uid, [('user_id', '=', uid)], context=context)
        if ids:
            return ids[0]
        return False
    
    def _check_is_manager(self, cr, uid, ids, field, arg, context=None):
        res = dict.fromkeys(ids, False)
        for exit in self.browse(cr, uid, ids, context=context):
            if exit.feosco_employee_id.user_id.id == uid:
                res[exit.id] = True
        return res
    
    _columns = {
                'name': fields.char('Code', size=32, help='Code',
                                           readonly=True,
                                           states={
                                                   'waiting_approve': [('readonly', False)]
                                                    }),
                'feosco_type': fields.selection(_get_exit_type, "Type",
                                                readonly=True,
                                                states={
                                                   'draft': [('readonly', False)]
                                                    }),
                'feosco_working_end_date': fields.date('End Date',readonly=True,
                                                 states={
                                                   'draft': [('readonly', False)]
                                                    }),
                'feosco_approve_date': fields.date('Approve Date', readonly=True,
                                                   states={
                                                   'draft': [('readonly', False)]
                                                    }),
                'feosco_employee_id': fields.many2one('hr.employee', 'Employee',
                                                      readonly=True,
                                                      states={
                                                              'draft': [('readonly', False)]
                                                              }),
                
                'state': fields.selection([('draft',u'New'), 
                                           ('refuse',u'Refuse'), 
                                                  ('waiting_dept_approve',u'Waiting department head approve'),
                                                  ('waiting_hr_approve',u'Waiting HR Approved'),
                                                  ('approve',u'Approved')
                                                  ],
                                                 'Status', readonly=True),
                
                'feosco_reason': fields.text('Reason', size=256, help='Reason',
                                           readonly=True,
                                           states={
                                                   'draft': [('readonly', False)]
                                                    }),
                
                'employee_is_manager': fields.function(_check_is_manager, type='boolean', string='Employee is manager'),
                
                'work_handover_ids' : fields.one2many('feosco.hr.work.handover', 'employee_exit_id', 'Work Handover'),
    }
    
    _defaults = {
        'state':'draft',
        'feosco_employee_id': _employee_get,
        'name': '/'
    }
    
    def _get_sequence(self, cr, uid, context=None):
        
        obj_seq = self.pool.get('ir.sequence')
        args_search = [('code','=','feosco.hr.employee.exit')]
        sequence_ids = obj_seq.search(cr, uid, args_search, context=context)
        
        return obj_seq.next_by_id(cr, uid, sequence_ids[0], context=context)
    
    def create(self, cr, uid, vals, context=None):
        
        vals['name'] = self._get_sequence(cr, uid, context)
        
        return super(feosco_hr_employee_exit, self).create(cr, uid, vals, context=context)
    
    def set_refuse(self, cr, uid, ids, context=None):
        
        return self.write(cr, uid, ids, {'state': 'refuse'}, context=context)        
    
    def set_waiting_dept_approve(self, cr, uid, ids, context=None):
        
        return self.write(cr, uid, ids, {'state': 'waiting_dept_approve'}, context=context)
    
    def set_waiting_hr_approve(self, cr, uid, ids, context=None):
        
         return self.write(cr, uid, ids, {'state': 'waiting_hr_approve'}, context=context)
    
    def set_approve(self, cr, uid, ids, context=None):
        
        date = datetime.now().strftime('%Y-%m-%d')
        
        return self.write(cr, uid, ids, {'state': 'approve', 'feosco_approve_date': date }, context=context)
    
    def _needaction_domain_get(self, cr, uid, context=None):
        dom = False
        user_pool = self.pool.get('res.users')
        emp_obj = self.pool.get('hr.employee')
        
        if user_pool.has_group(cr, uid, 'base.group_hr_manager') or user_pool.has_group(cr, uid, 'base.group_hr_user'):
            dom = [('state', '=', 'waiting_hr_approve')]
            
        elif user_pool.has_group(cr, uid, 'base.group_hr_dept_head'):
            employee_ids = emp_obj.search(cr, uid, [('department_id.manager_id.user_id.id', '=', uid)], context=context)
            if employee_ids:
                dom = [('state', '=','waiting_dept_approve'),
                       ('feosco_employee_id', 'in', employee_ids)]
                
        elif user_pool.has_group(cr, uid, 'base.group_user'):
            employee_ids = emp_obj.search(cr, uid, [('user_id.id', '=', uid)], context=context)
            current_date = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
            dom = [('state', 'in', ('refuse','approve')),
                   ('feosco_employee_id', 'in', employee_ids),
                   ('feosco_working_end_date', '>=', current_date)]             
        return dom

feosco_hr_employee_exit()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
