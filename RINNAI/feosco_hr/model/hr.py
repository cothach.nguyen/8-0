# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import addons
import logging
from openerp.osv import fields, osv
from datetime import datetime
import openerp

_logger = logging.getLogger(__name__)


class hr_employee(osv.osv):
    _name = "hr.employee"
    _description = "Employee"
    _inherit = "hr.employee"

    def check_birthday(self, cr, uid, ids, context=None):
        employee_obj = self.browse(cr, uid, ids, context=context)

        for emp in employee_obj:
            if emp.birthday and datetime.strptime(emp.birthday, '%Y-%m-%d') >= datetime.today():
                return False

        return True

    def _get_education_type(self, cr, uid, context=None):

        master_data_obj = self.pool.get('feosco.master.data')
        agrs = [
            ('type', '=', 'education_type')
        ]
        ids = master_data_obj.search(cr, uid, agrs, context=context)

        res = master_data_obj.read(cr, uid, ids, ['code', 'name'], context=context)

        res = [(r['code'], r['name']) for r in res]

        return res

    def _get_country_id(self, cr, uid, ids, context=None):
        search_country = [('code', '=', 'VN')]
        country_ids = self.pool.get('res.country').search(cr, uid, search_country)
        return country_ids[0] if country_ids else False

    def _get_ethnic_id(self, cr, uid, ids, context=None):
        search_ethnic = [('code', '=', 'E_Kinh'), ('type', '=', 'ethnic'), ]
        ethnic_ids = self.pool.get('feosco.master.data').search(cr, uid, search_ethnic)
        return ethnic_ids[0] if ethnic_ids else False

    def _get_religious_id(self, cr, uid, ids, context=None):
        search_religious = [('code', '=', 'E_Khong'), ('type', '=', 'religious'), ]
        religious_ids = self.pool.get('feosco.master.data').search(cr, uid, search_religious)
        return religious_ids[0] if religious_ids else False

    #return get res.partnerID
    def _get_partner_id(self, cr, uid, ids, name, args, context=None):
        eployee_obj = self.browse(cr, uid, ids)
        result = {}
        for employee in eployee_obj:
            if employee.address_home_id:
                partner_pool = self.pool.get('res.partner').browse(cr, uid, employee.address_home_id.id,
                                                                   context=context)

                #Get address of employee 
                street = partner_pool.street and partner_pool.street or ''
                street2 = partner_pool.street2 and partner_pool.street2 or ''
                district = partner_pool.feosco_district_id and partner_pool.feosco_district_id.name or ''
                city = partner_pool.feosco_city_id and partner_pool.feosco_city_id.name or ''
                country = partner_pool.country_id and partner_pool.country_id.name or ''

                re_address = street + '\n' + street2 + '\n' + district + '\n' + city + '\n' + country

                #Get temp address of employee
                # temp_street = partner_pool.feosco_temp_street and partner_pool.feosco_temp_street or ''
                # temp_strest2 = partner_pool.feosco_temp_street2 and partner_pool.feosco_temp_street2 or ''
                # temp_district = partner_pool.feosco_temp_district and partner_pool.feosco_temp_district.name or ''
                # temp_city = partner_pool.feosco_temp_city and partner_pool.feosco_temp_city.name or ''
                # temp_country = partner_pool.feosco_temp_country_id and partner_pool.feosco_temp_country_id.name or ''
                #
                # temp_address = temp_street + '\n' + temp_strest2 + '\n' + temp_district + '\n' + temp_city + '\n' + temp_country

                #Get phone
                mobile = partner_pool.mobile and partner_pool.mobile or ''

                result[employee.id] = {'feosco_address': re_address.replace('\n\n', '\n'),
                                       'feosco_temp_phone': mobile
                }
            else:
                result[employee.id] = {'feosco_address': None,
                                       'feosco_temp_phone': None
                }

        return result


    def _check_invisible_permission(self, cr, uid, ids, field, arg, context=None):
        '''
            This function check role of user.
            It used to hide information on View if user has no role.
                return False if user can see (no hide information)
                return True if user can not see ( hide information)
        '''
        res = dict.fromkeys(ids, True)
        for employee in self.browse(cr, uid, ids, context=context):
            # 1. Check group of user
            user_pool = self.pool.get('res.users')

            # 2. Check that user can see information.
            if user_pool.has_group(cr, uid, 'base.group_hr_manager') or user_pool.has_group(cr, uid,
                                                                                            'base.group_hr_user'):
                res[employee.id] = False
            elif user_pool.has_group(cr, uid, 'base.group_hr_dept_head') and employee.department_id \
                    and employee.department_id.manager_id \
                    and employee.department_id.manager_id.user_id \
                    and employee.department_id.manager_id.user_id.id == uid:
                res[employee.id] = False
            elif user_pool.has_group(cr, uid, 'base.group_user') and employee.user_id.id == uid:
                res[employee.id] = False
            else:
                res[employee.id] = True

        return res

    _columns = {
        'feosco_code': fields.char('Employee Code', size=64, help='Employee Code'),
        'feosco_ethnic': fields.many2one('feosco.master.data', 'Ethinic', domain=[('type', '=', 'ethnic')]),
        'feosco_religious': fields.many2one('feosco.master.data', 'Religious', domain=[('type', '=', 'religious')]),
        'feosco_driver_license': fields.char('', size=32, help='Employee Code'),
        'feosco_identification_issue': fields.many2one('feosco.city', 'Identification Issue'),
        'feosco_identification_date': fields.date('Identification Date'),
        'feosco_education_level': fields.selection(_get_education_type, 'Education Level'),
        'feosco_language_ids': fields.many2many('feosco.master.data', 'employee_lang_rel', 'emp_id', 'lang_id',
                                                'Languages', domain=[('type', '=', 'language')]),
        'feosco_exit_info_ids': fields.one2many('feosco.hr.employee.exit', 'feosco_employee_id', 'Exit Information'),
        # 'feosco_branch_id': fields.many2one('feosco.hr.branch', 'Branch'),
        'feosco_department_history_ids': fields.one2many('feosco.hr.employee.work.history', 'employee_id',
                                                         'Work history'),
        'feosco_employee_history_ids': fields.one2many('feosco.hr.employee.history', 'employee_id',
                                                       'Work history orther'),
        'feosco_address': fields.function(_get_partner_id, string='Home Address', type='text', multi='address'),

        'feosco_temp_phone': fields.function(_get_partner_id, string='Mobile', type='char', multi='phone'),
        'feosco_invisible': fields.function(_check_invisible_permission, type='boolean', string='Can see'),
    }
    _constraints = [
        (check_birthday, 'input birthday again,please !', ['birthday']),
    ]

    _defaults = {
        'feosco_code': '/',
        'country_id': _get_country_id,
        'feosco_ethnic': _get_ethnic_id,
        'feosco_religious': _get_religious_id
    }

    def _get_sequence(self, cr, uid, context=None):

        obj_seq = self.pool.get('ir.sequence')
        args_search = [('code', '=', 'hr.employee')]
        sequence_ids = obj_seq.search(cr, uid, args_search, context=context)

        return obj_seq.next_by_id(cr, uid, sequence_ids[0], context=context)

    def create(self, cr, uid, vals, context=None):

        if context == None:
            context = {}
        vals['feosco_code'] = self._get_sequence(cr, uid, context)
        employee_id = super(hr_employee, self).create(cr, uid, vals, context=context)
        return employee_id

    def write(self, cr, uid, ids, vals, context=None):

        return super(hr_employee, self).write(cr, uid, ids, vals, context=context)

    def unlink(self, cr, uid, ids, context=None):
        """
        unlink():
        Function used to override the unlink method
        """

        # Get job id of employee
        lst_ids = []
        job_pool = self.pool.get('hr.job')
        for employee in self.browse(cr, uid, ids, context=context):
            if employee.job_id:
                lst_ids.append(employee.job_id.id)

        # Delete these employees
        result = super(hr_employee, self).unlink(cr, uid, ids, context=context)

        if result and lst_ids:
            # Update job id
            res = self.pool.get('hr.job')._no_of_employee(cr, uid, lst_ids, 'no_of_employee', None, context=context)
            for job_id in res.keys():
                no_of_employee = res[job_id]['no_of_employee']
                expected_employees = res[job_id]['expected_employees']
                cr.execute('update hr_job set no_of_employee = %s, expected_employees = %s where id = %s',
                           (no_of_employee, expected_employees, job_id))

        return result

    def onchange_user(self, cr, uid, ids, user_id, context=None):
        result = super(hr_employee, self).onchange_user(cr, uid, ids, user_id, context=context)

        if user_id:
            user_obj = self.pool.get('res.users').browse(cr, uid, user_id, context=context)

            result['value'].update({'address_home_id': user_obj.partner_id.id})

        return result


hr_employee()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
