# -*- coding: utf-8 -*-
from openerp.osv import fields, osv
import logging
from datetime import datetime, date, timedelta


class feosco_hr_insurance_cronjob(osv.osv):
    _name = "feosco.hr.cronjob"
    __logger = logging.getLogger(_name)
    _auto = False

    def feosco_auto_disable_employee(self, cr, uid, automatic=False, use_new_cursor=False, context=None):
        self.__logger.info('START: feosco_auto_disable_employee')
        employee_ids = []
        try:
            if context is None:
                context = {}
            employee_exit_pool = self.pool.get('feosco.hr.employee.exit')
            employee_pool = self.pool.get('hr.employee')
            # Search nhan vien trong bang feosco.hr.employee.exit
            agrs = [
                ('feosco_working_end_date', '<=', datetime.now().strftime('%Y-%m-%d')),
                ('state', '=', 'approve')]
            employee_exit_ids = employee_exit_pool.search(cr, uid, agrs, context=context)

            if employee_exit_ids:
                # Get id of that employees
                query = """select feosco_employee_id from feosco_hr_employee_exit where id IN %s"""
                cr.execute(query, (tuple(employee_exit_ids),))
                query_rs = cr.fetchall()
                employee_ids = [x[0] for x in query_rs]
                # Neu co, inactive nhan vien do
                write_agrs = {'active': False}
                employee_pool.write(cr, uid, employee_ids, write_agrs, context=context)


        except Exception as exc:
            self.__logger.info('Can NOT disable employee %s' % (exc))

        self.__logger.info('Employees are disabled %s' % (str(employee_ids)))
        self.__logger.info('END: feosco_auto_disable_employee')
        return True

feosco_hr_insurance_cronjob()