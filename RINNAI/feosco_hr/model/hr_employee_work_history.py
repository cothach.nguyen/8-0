# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from datetime import datetime
import logging


class feosco_hr_employee_work_history(osv.osv):
    _name = "feosco.hr.employee.work.history"
    _description = "Department History"
    __logger = logging.getLogger(_name)

    # Default employee
    def default_get(self, cr, uid, fields, context=None):
        if context is None:
            context = {}
        res = super(feosco_hr_employee_work_history, self).default_get(cr, uid, fields, context=context)

        # Default feosco_employee_id
        if context.has_key('default_employee'):
            res.update({'employee_id': context.get('default_employee', False)})
        return res

    _columns = {
        'from_date': fields.date('From Date'),
        'end_date': fields.date('To Date'),
        'department_id': fields.many2one('hr.department', 'Department'),
        'job_id': fields.many2one('hr.job', 'Job'),
        # 'branch_id': fields.many2one('feosco.hr.branch', 'Branch'),
        'employee_id': fields.many2one('hr.employee', 'Employee', ondelete='cascade'),
        'state': fields.selection([('new', 'New'), ('working', 'Working'), ('left', 'Left')],
                                  'Status'),
    }

    _order = 'from_date desc'

    _defaults = {
        'state': 'new',
    }

    def create_work_history_by_employee(self, cr, uid, employee_ids=False,
                                        department_id=None,
                                        job_id=None,
                                        context=None):
        self.__logger.info("action=START function=create_work_history_by_employee")
        self.__logger.info(employee_ids)
        self.__logger.info(department_id)

        result = []

        if employee_ids:
            # 0 Init agrs
            employee_pool = self.pool.get('hr.employee')
            date_now = datetime.now().strftime('%Y-%m-%d')

            # 1. Search old department of employee by employee id, department id, state = 'working'
            agrs_search = [('employee_id', 'in', employee_ids), ('state', '=', 'working')]
            res_history_ids = self.search(cr, uid, agrs_search, context=context)

            # 2. Update old department to left, end_date = current date
            if res_history_ids:
                agrs_write = {'state': 'left', 'end_date': date_now}
                self.write(cr, uid, res_history_ids, agrs_write, context=context)

            # 3. Update new department for employee
            employee_objs = employee_pool.browse(cr, uid, employee_ids, context=context)

            for employee_obj in employee_objs:
                create_data = {
                    'department_id': employee_obj.department_id.id if not department_id else department_id,
                    'job_id': employee_obj.job_id.id if not job_id else job_id,
                    'employee_id': employee_obj.id,
                    'from_date': date_now,
                    'state': 'working'
                }

                history_id = self.create(cr, uid, create_data, context=context)

                result.append(history_id)

        self.__logger.info("action=END function=create_work_history_by_employee")
        self.__logger.info(result)
        return result


feosco_hr_employee_work_history()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
