# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from datetime import datetime
import logging

class feosco_hr_employee__history(osv.osv):
    _name = "feosco.hr.employee.history"
    _description = "work porcess history of employee"
    __logger = logging.getLogger(_name)
    
    # check date input
    def _check_date(self, cr, uid, ids, context=None):
            emphis_obj = self.browse(cr, uid, ids, context=context)
            for emphis in emphis_obj:
                if datetime.strptime(emphis.from_date, '%Y-%m-%d') > datetime.strptime(emphis.end_date, '%Y-%m-%d'):
                    return False
            return True
    
    # Default employee
    def default_get(self, cr, uid, fields, context=None):
        if context is None:
            context = {}
        res = super(feosco_hr_employee__history, self).default_get(cr, uid, fields, context=context)
        
        # Default feosco_employee_id
        if context.has_key('default_employee'):
            res.update({'employee_id': context.get('default_employee', False) })  
        return res
    
    _columns = {
                'from_date': fields.date('From Date'),
                'end_date': fields.date('To Date'),
                'position': fields.char('Position'),
                'job':fields.char('Job', size=256),
                'description':fields.text('Description', size=256),
                'employee_id': fields.many2one('hr.employee', 'Employee', ondelete='cascade'),
    }  
    _order = 'from_date desc'
    
    _constraints = [
                    (_check_date, 'input to date again,please !', ['end_date']),
    ]
    
feosco_hr_employee__history()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
