# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp import tools

class hr_department(osv.osv):
    _inherit = "hr.department"
    
    def _addorremove_user_to_dep_head_group(self, cr, uid, employee_id, type = 'add', context=None):
        res = False
        if employee_id and type in ('add','remove'):
            employee_pool = self.pool.get('hr.employee')
            user_pool = self.pool.get('res.users')
            category_pool = self.pool.get('ir.module.category')
            group_pool = self.pool.get('res.groups')
            
            employee = employee_pool.browse(cr, uid, employee_id, context=context)
            user_id = employee.user_id and employee.user_id.id or False
            
            if user_id:
                
                agrs_search = [('name','=','Human Resources')]
                category_ids = category_pool.search(cr, uid, agrs_search, context=context)
                
                if category_ids:
                    agrs_search = [('category_id','=',category_ids[0]),
                                   ('name','=','Department Head')]
                    group_ids = group_pool.search(cr, uid, agrs_search, context=context)
                
                if group_ids:
                    write_args = {}
                    if type == 'add':
                        write_args = {'groups_id': [(4, group_ids[0])]}
                    else: # type == remove
                        write_args = {'groups_id': [(3, group_ids[0])]}
                        
                    res = user_pool.write(cr, uid, [user_id], write_args, context=context)
        return res
    
    def create(self, cr, uid, vals, context=None):
        department_id = super(hr_department, self).create(cr, uid, vals, context=context)
        
        self._addorremove_user_to_dep_head_group(cr, uid, vals.get('manager_id', False), 'add', context=context)
        
        return department_id
    
    def write(self, cr, uid, ids, values, context=None):      
        employee_id = values.get('manager_id', False)
        if employee_id:
             # Remove old manager from group dept head
            for department in self.browse(cr, uid, ids, context=context):
                old_employee_id = department.manager_id and department.manager_id.id or False
                if old_employee_id:
                    # Chech manager nay co dang la dept head cua phong ban khac khong
                    agrs_search = [('manager_id','=',old_employee_id),
                                   ('id','!=',department.id)]
                    res_department_ids = self.search(cr, uid, agrs_search, context=context)
                    if not res_department_ids:
                        self._addorremove_user_to_dep_head_group(cr, uid, old_employee_id, 'remove', context=context)
            
            # Add new manager to group dept head
            self._addorremove_user_to_dep_head_group(cr, uid, employee_id, 'add', context=context)
            
        return super(hr_department, self).write(cr, uid, ids, values, context=context)
            
            

hr_department()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
