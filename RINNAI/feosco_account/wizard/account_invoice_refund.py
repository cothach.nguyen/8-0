# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import osv
import logging
from openerp.tools.translate import _

class account_invoice_refund(osv.osv_memory):



    """Refunds invoice"""

    _inherit = "account.invoice.refund"

    __logger = logging.getLogger(_inherit)


    def invoice_refund(self, cr, uid, ids, context=None):
        self.__logger.info('begin invoice_refund')
        invoice_from = None
        if context and context.has_key('active_ids'):
            orm_invoice = self.pool.get('account.invoice')
            invoice_ids = context.get('active_ids')
            for invoice in orm_invoice.browse(cr, uid, invoice_ids):
                invoice_from = invoice
                if invoice.type == 'out_invoice':
                    if invoice.origin:
                        out_refund_ids = orm_invoice.search(cr, uid, [('origin', '=', invoice.origin), ('type', '=', 'out_refund')])
                        if out_refund_ids:
                            raise osv.except_osv(_('Error!'), _('Can not refund customer, please go to Menu [Customer Refund] and search Invoice Refund the same Customer : %s' % invoice.partner_id.name if invoice.partner_id else ''))
                    else:
                        self.__logger.warning('Refund Customer Number [ %s ] have null Origin (Source Document)' % invoice.number if invoice.number else 'null')

                if invoice.type == 'in_invoice': #Supplier Invoice
                    if invoice.reference:
                        in_refund_ids = orm_invoice.search(cr, uid, [('partner_id', '=', invoice.partner_id.id), ('id', '!=', invoice.id), ('state', '!=', 'cancel'), ('reference', '=', invoice.reference), ('type', '=', 'in_refund')])
                        if in_refund_ids:
                            raise osv.except_osv(_('Error!'),_('Can not refund supplier, please go to Menu [Supplier Refund] and search Supplier Refund the same Supplier : %s' % invoice.partner_id.name if invoice.partner_id else ''))
                    if invoice.origin:
                        in_refund_ids = orm_invoice.search(cr, uid, [('partner_id', '=', invoice.partner_id.id), ('id', '!=', invoice.id), ('state', '!=', 'cancel'), ('origin', '=', invoice.origin), ('type', '=', 'in_refund')])
                        if in_refund_ids:
                            raise osv.except_osv(_('Error!'),_('Can not refund supplier, please go to Menu [Supplier Refund] and search Supplier Refund the same Supplier : %s' % invoice.partner_id.name if invoice.partner_id else ''))
                    else:
                         self.__logger.warning('Refund Supplier Number [ %s ] have null Reference (Source Document)' % invoice.number if invoice.number else 'null')
        res = super(account_invoice_refund, self).invoice_refund(cr, uid, ids, context=context)
        if invoice_from and res and res.has_key('domain') and len(res.get('domain')) >= 2:
            if invoice_from.origin !=  orm_invoice.browse(cr, uid, res.get('domain')[1][2][0]):
                if invoice_from.origin:
                    orm_invoice.write(cr, uid, res.get('domain')[1][2][0], {'origin': invoice_from.origin, 'name': invoice_from.name})
                else:
                    orm_invoice.write(cr, uid, res.get('domain')[1][2][0], {'origin': invoice_from.number, 'name': invoice_from.name})
        self.__logger.info('res, %s' % res)
        self.__logger.info('end invoice_refund')
        return res

account_invoice_refund()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
