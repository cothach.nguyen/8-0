{
    'name': 'FEOSCO Accounting',
    'version': '0.1',
    'author': 'Feosco',
    'category': 'Feosco',
    'sequence': 21,
    'website': 'http://www.feosco.com',
    'description': """
    Fix duplicate Customer/Supplier refund  the same Customer/Supplier
    """,
    'author': 'Feosco',
    'website': 'http://www.feosco.com',
    'images': [
    ],
    'depends': [
        'account',
        'sale',
                ],
    'data': [
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}