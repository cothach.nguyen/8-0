from openerp.osv import osv
import logging

class account_invoice(osv.osv):

    _inherit = "account.invoice"
    __logger = logging.getLogger(_inherit)
    _columns = {

    }


    def create(self, cr, uid, vals, context={}):
        self.__logger.info('begin create')
        self.__logger.info('vals, %s' % vals)
        self.__logger.info('end create')
        return super(account_invoice, self).create(cr, uid, vals, context=context)

