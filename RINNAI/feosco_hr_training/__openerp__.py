# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'FEOSCO HR Training',
    'version': '1.1',
    'author': 'Feosco',
    'category': 'Feosco',
    'sequence': 21,
    'website': 'http://www.feosco.com',
    'summary': 'Training Management',
    'description': """
        This module is used to mange training in company
    """,
    'author': 'Feosco',
    'website': 'http://www.feosco.com',
    'images': [
    ],
    'depends': ['base',
                'hr',
                'feosco_base',
                'feosco_hr',
                'survey',
                'hr_evaluation'
                ],
    'data': [
           'wizard/view/wizard_create_trainee_appraisal.xml',
             'wizard/view/wizard_create_lecturer_appraisal.xml',
            'wizard/view/wizard_create_training_exam.xml',
             'view/course_master.xml',
             'view/lecturer.xml',
             'view/location.xml',
             'view/plan_course.xml',
             'view/course.xml',
            'view/training_exam.xml',
             'view/hr_employee.xml',
              'view/trainee_appraisal.xml',
              'view/lecturer_appraisal.xml',
             'view/menu.xml',      
             'security/ir.model.access.csv',
             'security/survey_security.xml'    
    ],
    'demo': [],
    'test': [
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
    'css': [],
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
