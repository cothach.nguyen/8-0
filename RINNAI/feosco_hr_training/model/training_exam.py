#-*- coding:utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv

class feosco_hr_training_exam(osv.osv):
    _name = 'feosco.hr.training.exam'
    _inherit =  'mail.thread'
    _inherits = {
                 'survey.user_input':'request_id'
                 }
      
    _columns = {
                'plan_course_id': fields.many2one('feosco.hr.plan.course', 'Plan course',ondelete='cascade'), 
#                 'request_id': fields.many2one('survey.request', 'Request',ondelete='cascade'),
                             
                }
    
    def create(self, cr, uid, vals, context=None):
        if vals.get('state',False):
            vals.update({'state': 'new'})
        return super(feosco_hr_training_exam, self).create(cr, uid, vals, context=context)
    
    def survey_req_done(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, { 'state' : 'done'})
        return True
    
    
    def action_survey_resent(self, cr, uid, ids, context=None):
        
        res_id = []
        for this in self.browse(cr, uid, ids, context=context):
            res_id.append(this.request_id.id)
        
        return self.pool.get('survey.user_input').action_survey_resent(cr, uid, res_id, context=context)
    
feosco_hr_training_exam()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
