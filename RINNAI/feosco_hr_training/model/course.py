#-*- coding:utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp import pooler, tools
from openerp.tools.translate import _
from openerp.tools.yaml_import import is_comment


class feosco_hr_course(osv.osv):
    _name = 'feosco.hr.course'
    _inherit = ['ir.needaction_mixin']

    #get image of lecturer

    def _get_image_lecturer(self, cr, uid, ids, name, args, context=None):

        result = dict.fromkeys(ids, False)


    #get regis total number
    def get_regis_number(self, cr, uid, ids, name, args, context=None):

        result = {}

        course_obj = self.browse(cr, uid, ids, context=context)

        for course in course_obj:

            result[course.id] = len(course.participant_ids)

            if len(course.participant_ids) == 0:
                result[course.id] = 0

        return result

    _columns = {
        'name': fields.char('Name', size=64, readonly=True, states={'draft': [('readonly', False)]}),
        'objective': fields.text('Objective', size=256, readonly=True, states={'draft': [('readonly', False)]}),
        'category': fields.many2one('feosco.hr.course.master', 'Category', domain=[('type', '=', 'category')],
                                    readonly=True, states={'draft': [('readonly', False)]}),
        'theme': fields.many2many('feosco.hr.course.master', 'course_theme_rel', 'course_id', 'theme_id',
                                  'Theme', readonly=True, states={'draft': [('readonly', False)]},
                                  domain=[('type', '=', 'theme')]),
        'level': fields.many2many('feosco.hr.course.master', 'course_level_rel', 'course_id', 'level_id',
                                  'Level', readonly=True, states={'draft': [('readonly', False)]},
                                  domain=[('type', '=', 'level')]),
        'method': fields.many2one('feosco.hr.course.master', 'Method', domain=[('type', '=', 'method')], readonly=True,
                                  states={'draft': [('readonly', False)]}),
        'language': fields.many2one('feosco.master.data', 'Language', domain=[('type', '=', 'language')], readonly=True,
                                    states={'draft': [('readonly', False)]}),
        'lecturer': fields.many2one('feosco.hr.lecturer', 'Lecturer', readonly=True,
                                    states={'draft': [('readonly', False)]}),
        #'image_lecturer': fields.function(_get_image_lecturer, string="Image lecturer", type="binary"),
        'state': fields.selection((('draft', 'Draft'),
                                   ('validated', 'Validated'),
                                   ('registration', 'Registration'),
                                   ('in_progress', 'In Progress'),
                                   ('done', 'Done'),
                                   ('cancel', 'Cancel')), 'Status'),
        'target_audience': fields.text('Target Audience', readonly=True, states={'draft': [('readonly', False)]}),
        'note': fields.text('Note', readonly=True, states={'draft': [('readonly', False)]}),
        'participant_ids': fields.many2many('hr.employee', 'course_employee_rel', 'course_id', 'employee_id',
                                            'Participant', readonly=True, states={'draft': [('readonly', False)]}),
        'registration': fields.boolean('Registration', help='Check if Employee can register this course', readonly=True,
                                       states={'draft': [('readonly', False)]}),
        'document': fields.one2many('ir.attachment', 'res_id', 'Documents'),
        'color': fields.integer(string="Color Index"),
        'regis_total': fields.function(get_regis_number, string='Registered seat', readonly=True, type='integer',
                                       method=True),
    }

    _defaults = {
        'state': 'draft',
        'registration': True
    }

    def set_cancel(self, cr, uid, ids, context=None):

        return self.write(cr, uid, ids, {'state': 'cancel'}, context=context)

    def set_draft(self, cr, uid, ids, context=None):

        return self.write(cr, uid, ids, {'state': 'draft'}, context=context)

    def set_validate(self, cr, uid, ids, context=None):

        return self.write(cr, uid, ids, {'state': 'validated'}, context=context)

    def set_registration(self, cr, uid, ids, context=None):

        return self.write(cr, uid, ids, {'state': 'registration'}, context=context)

    def set_in_progress(self, cr, uid, ids, context=None):

        return self.write(cr, uid, ids, {'state': 'in_progress'}, context=context)

    def set_done(self, cr, uid, ids, context=None):

        return self.write(cr, uid, ids, {'state': 'done'}, context=context)

    def acc_RegisCourse(self, cr, uid, ids, context=None):

        emp_search = [('user_id', '=', uid)]

        employee_id = self.pool.get('hr.employee').search(cr, uid, emp_search, context=context)

        if employee_id:
            return self.write(cr, uid, ids[0], {'participant_ids': [[4, employee_id[0]]]}, context=context)

        return False

    def _needaction_domain_get(self, cr, uid, context=None):
        dom = False

        return dom


feosco_hr_course()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
