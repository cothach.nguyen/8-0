#-*- coding:utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv

class feosco_hr_lecturer(osv.osv):
    _name = 'feosco.hr.lecturer'
    _inherits = {
                 'res.partner':'partner_id'
                 }
    
    _columns = {
                    'partner_id' : fields.many2one('res.partner', 'Partner', ondelete='cascade'),
                }
    
    #return city and country default when have option  district value
    def on_change_district(self, cr, uid, ids,feosco_district,addressOrtemp=1, context=None):
        if feosco_district_id:
            district_pool=self.pool.get('feosco.district').read(cr, uid, feosco_district_id,['city_id'])
            tupe_city_id=district_pool['city_id']
            country_pool=self.pool.get('feosco.city').read(cr, uid, tupe_city_id[0],['country'])
            if addressOrtemp==1:
                return {'value': {'city': district_pool['city_id'],'country_id': country_pool['country']}}
            elif addressOrtemp==2:
                return {'value': {'feosco_temp_city': district_pool['city_id'],'feosco_temp_country_id': country_pool['country']}}
        else:
            return {}
    
feosco_hr_lecturer()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
