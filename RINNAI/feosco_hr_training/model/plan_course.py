#-*- coding:utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv


class feosco_hr_plan_course(osv.osv):
    _name = 'feosco.hr.plan.course'

    _inherits = {
        'feosco.hr.course': 'course_id'
    }


    def get_regist_seat_number(self, cr, uid, ids, name, args, context=None):

        result = {}

        this = self.browse(cr, uid, ids[0], context=context)

        result[ids[0]] = len(this.participant_ids)

        if len(this.participant_ids) == 0:
            result[ids[0]] = 0

        return result

    #return seat location by location id

    def _get_seat_location(self, cr, uid, ids, name, args, context=None):

        result = {}

        plan_obj = self.browse(cr, uid, ids[0], context=context)

        result[ids[0]] = 0

        if plan_obj.location:
            pool_location = self.pool.get('feosco.hr.course.location')
            location_dict = pool_location.browse(cr, uid, plan_obj.location.id, context=context)
            seat_total = location_dict.seat
            result[ids[0]] = "(" + (seat_total).__str__() + " seats)"

        return result

    #check visible botton register course: if return true --> bt regis is visible

    def check_visible(self, cr, uid, ids, name, args, context=None):

        result = {}

        this = self.browse(cr, uid, ids[0], context=context)

        check_bt = 0
        check_status = 0

        if this.available_seat <= this.regist_seat:
            check_bt = 1

        if this.registration and this.state == 'validated':
            check_status = 1

        result[ids[0]] = {'visible_bt_regis': check_bt, 'visble_bt_in_progress': check_status}

        return result

        #set value for available_seat by location

    def on_change_location(self, cr, uid, ids, location, context=None):

        if location:

            pool_location = self.pool.get('feosco.hr.course.location')

            location_dict = pool_location.browse(cr, uid, location, context=context)

            seat_total = location_dict.seat

            return {'value': {'available_seat': seat_total}}

        else:
            return {'value': {'available_seat': 0}}


    def on_change_course_id(self, cr, uid, ids, course_id, context=None):


        couse_obj = self.pool.get('feosco.hr.course').browse(cr, uid, course_id, context=context)

        res = {'lecturer': couse_obj.lecturer.id if couse_obj.lecturer.id else False}

        return {'value': res}

    #check plan course id

    def check_plan_course(self, cr, uid, ids, context=None):

        plan_obj = self.browse(cr, uid, ids[0], context=context)

        plan_search_check = self.search(cr, uid, [('course_id', '=', plan_obj.course_id.id)], context=context)
        if plan_search_check.__contains__(plan_obj.id):
            plan_search_check.remove(plan_obj.id)

        if plan_search_check:
            return False

        return True

    #get res_partner id for set image
    def _get_image_lecturer(self, cr, uid, ids, name, args, context=None):

        result = {}

        plan_course_obj = self.browse(cr, uid, ids, context=context)

        pool_lecturer = self.pool.get('feosco.hr.lecturer')

        res_partner_id = 0

        for plan_course in plan_course_obj:

            if plan_course.lecturer.id:

                partnerid_in_lecturer = pool_lecturer.read(cr, uid, plan_course.lecturer.id, ['partner_id'],
                                                           context=context)

                if partnerid_in_lecturer['partner_id'][0]:
                    res_partner_id = partnerid_in_lecturer['partner_id'][0]

                result[plan_course.id] = res_partner_id

        return result

    def _check_date(self, cr, uid, ids, context=None):
        plan_course_obj = self.browse(cr, uid, ids, context=context)
        for plan_course in plan_course_obj:
            if plan_course.start_date and plan_course.end_date and plan_course.start_date > plan_course.end_date:
                return False
        return True

    #check duration(hours)
    def _check_duration(self, cr, uid, ids, context=None):
        plan_course_obj = self.browse(cr, uid, ids, context=context)
        for plan_course in plan_course_obj:
            if plan_course.duration <= 0:
                return False

        return True

    def validate(self, cr, uid, ids, context=None):
        plan_course_obj = self.browse(cr, uid, ids, context=context)
        for plan_course in plan_course_obj:

            if plan_course.minimum_threshold and plan_course.minimum_threshold <= 0:
                return False

            if plan_course.available_seat and plan_course.available_seat <= 0:
                return False

        return True

    _columns = {
        'start_date': fields.date('Start Date'),
        'end_date': fields.date('End Date'),
        'start_hour': fields.float('Start Time'),
        'end_hour': fields.float('End Time'),
        'duration': fields.float('Duration'),
        'minimum_threshold': fields.integer('Minimum Threshold'),
        'available_seat': fields.integer('Available seat'),
        'regist_seat': fields.function(get_regist_seat_number, string='Registered seat', readonly=True, type='integer',
                                       method=True),
        'location': fields.many2one('feosco.hr.course.location', 'Location'),
        'seat_total': fields.function(_get_seat_location, string='Seat Total', type='char'),
        'responsible_id': fields.many2one('res.users', 'Responsible'),
        'course_id': fields.many2one('feosco.hr.course', 'Courses', ondelete='cascade'),
        'visible_bt_regis': fields.function(check_visible, type="integer", string="Check visible",
                                            multi='check visible'),
        'visble_bt_in_progress': fields.function(check_visible, type="integer", string="Visible in progress",
                                                 multi='check visible'),
        'trainee_evaluation_ids': fields.one2many('feosco.hr.trainee.evaluation', 'course_id', 'Trainee Appraisal'),
        'lecturer_evaluation_ids': fields.one2many('feosco.hr.lecturer.evaluation', 'plan_course_id',
                                                   'Lecturer Appraisal'),
        'survey_request': fields.one2many('feosco.hr.training.exam', 'plan_course_id', 'Training Exam'),
        'plan_color': fields.integer(string="Color Index"),
        'res_partner': fields.function(_get_image_lecturer, string="Res_partner_id", type="integer"),
    }

    _constraints = [
        (check_plan_course, '\nThis course is was used!', ['course_id']),
        (_check_date, '\n Wrong Start Date or End Date. Please Enter Again', ['end_date']),
        (_check_duration, '\n Please Enter Time Total !', ['duration']),
    ]

    _defaults = {
        'responsible_id': lambda self, cr, uid, context: uid,
    }

    def create(self, cr, uid, vals, context=None):
        course_id = vals.get('course_id', False)

        if course_id:
            course_pool = self.pool.get('feosco.hr.course')
            course_obj = course_pool.browse(cr, uid, course_id, context=context)
            vals.update({'state': course_obj.state})

        return super(feosco_hr_plan_course, self).create(cr, uid, vals, context=context)

    def set_registration(self, cr, uid, ids, context=None):

        this = self.browse(cr, uid, ids[0], context=context)

        return self.pool.get('feosco.hr.course').set_registration(cr, uid, this.course_id.id, context=context)

    def set_in_progress(self, cr, uid, ids, context=None):

        this = self.browse(cr, uid, ids[0], context=context)

        return self.pool.get('feosco.hr.course').set_in_progress(cr, uid, this.course_id.id, context=context)

    def set_done(self, cr, uid, ids, context=None):

        this = self.browse(cr, uid, ids[0], context=context)

        return self.pool.get('feosco.hr.course').set_done(cr, uid, this.course_id.id, context=context)

    def action_RegisCourse(self, cr, uid, ids, context=None):

        this = self.browse(cr, uid, ids[0], context=context)

        return self.pool.get('feosco.hr.course').acc_RegisCourse(cr, uid, [this.course_id.id], context=context)


feosco_hr_plan_course()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
