#-*- coding:utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from datetime import datetime
from dateutil.relativedelta import relativedelta
from dateutil import parser
import time

from openerp.osv import fields, osv

class feosco_hr_lecturer_evaluation(osv.osv):
    _name = 'feosco.hr.lecturer.evaluation'
    _inherits = {'survey.user_input': 'request_id'}
    _description = "Lecturer Appraisal"
    _inherit =  'mail.thread'
    _columns = {
                    'lecturer_to_interview_id': fields.many2one('feosco.hr.lecturer', 'Lecturer', ondelete='cascade'),
                    'plan_course_id': fields.many2one('feosco.hr.plan.course','Course', ondelete='cascade'),
                }
    
    def create(self, cr, uid, vals, context=None):
        
        vals.update({'state': 'new'})
        return super(feosco_hr_lecturer_evaluation, self).create(cr, uid, vals, context=context)
    
    def survey_req_done(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, { 'state' : 'done'})
        return True
    
    def action_survey_resent(self, cr, uid, ids, context=None):
        
        res_id = []
        for this in self.browse(cr, uid, ids, context=context):
            res_id.append(this.request_id.id)
        
        return self.pool.get('survey.user_input').action_survey_resent(cr, uid, res_id, context=context)
    
    
feosco_hr_lecturer_evaluation()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
