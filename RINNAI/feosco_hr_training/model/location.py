#-*- coding:utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv

class feosco_hr_course_location(osv.osv):
    _name = 'feosco.hr.course.location'

    _columns = {
                'name':fields.char('Name', size=64),
                'address': fields.many2one('res.partner', 'Address'),
                'room': fields.integer('Room'),
                'seat': fields.integer('Seats'),
                'description': fields.text('Description',size=256),
                }
feosco_hr_course_location()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
