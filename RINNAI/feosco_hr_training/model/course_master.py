#-*- coding:utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv

class feosco_hr_course_master(osv.osv):
    _name = 'feosco.hr.course.master'
    
    def default_get(self, cr, uid, fields, context=None):
        if context is None:
            context = {}
        res = super(feosco_hr_course_master, self).default_get(cr, uid, fields, context=context)
        
        # Default type
        if context.has_key('type'):
            res.update({'type': context.get('type', False) })
            
        return res
    
    _columns = {
                'name':fields.char('Name', size=64),
                'description': fields.text('Description',size=256),
                'type': fields.selection((('category','Category'),
                                          ('level','Level'),
                                          ('method','Method'),
                                          ('theme','Theme'),
                                          ('rating','Rating'),
                                           ),'Type'),
                }
feosco_hr_course_master()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
