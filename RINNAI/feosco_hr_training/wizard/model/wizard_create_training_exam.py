#-*- coding:utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from datetime import datetime
from dateutil.relativedelta import relativedelta
from dateutil import parser
import time

from openerp.osv import fields, osv


class feosco_wizard_hr_training_exam(osv.osv_memory):
    _name = 'feosco.wizard.hr.training.exam'

    _description = "Wizard to create training exam"

    _columns = {
        'survey_id': fields.many2one('survey.survey', 'Survey', ondelete='cascade'),
        'deadline': fields.date('Deadline'),
    }

    def check_deadline(self, cr, uid, ids, context=None):

        for appraisal_obj in self.browse(cr, uid, ids, context=context):

            if appraisal_obj.deadline and datetime.strptime(appraisal_obj.deadline, '%Y-%m-%d') < datetime.today():
                return False

        return True

    _constraints = [
        (check_deadline, '\n Deadline must be greater than Current Date!', ['deadline'])
    ]

    def auto_create_training_exam(self, cr, uid, ids, context=None):
        training_exam_pool = self.pool.get('feosco.hr.training.exam')
        plan_course_id = context.get('active_id', False)
        plan_course_obj = self.pool.get('feosco.hr.plan.course').browse(cr, uid, plan_course_id, context=context)
        this = self.browse(cr, uid, ids[0], context=context)

        lecturer_id = plan_course_obj.course_id.lecturer.id
        for participant in plan_course_obj.participant_ids:
            if participant.user_id:
                user_obj = self.pool.get('res.users')
                
                user_data = user_obj.read(cr, uid, participant.user_id.id, fields=['partner_id','login'], context=context)
                partner_id = user_data['partner_id'][0]
                email = user_data['login']

                create_data = {
                    'survey_id': this.survey_id.id,
                    'deadline': this.deadline,
                    'partner_id': partner_id,
                    'email': email if email else None,
                    'type': 'link',
                    'state': 'new',
                    'plan_course_id': plan_course_obj.id
                }
                training_exam_pool.create(cr, uid, create_data, context=context)

            else:
                continue

        return True


feosco_wizard_hr_training_exam()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
