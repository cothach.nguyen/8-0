# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import datetime
from openerp import addons
import logging
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import tools

_logger = logging.getLogger(__name__)



class feosco_hr_asset_allocation(osv.osv):
    _name = "feosco.hr.asset.allocation"
    _description = "Manage allocate to employee"
    
    _columns = {
                'name': fields.char('Name', size=64),
                'asset_id': fields.many2one('account.asset.asset', 'Asset'),
                'employee_id':fields.many2one('hr.employee','Employee'),
                'employee_exit_id':fields.many2one('feosco.hr.employee.exit','Employee Exit'),
                'state': fields.selection([('draft',u'New'), 
                                            ('refuse',u'Refused'), 
                                              ('wait_approval',u'Wait approval'),
                                              ('wait_allocation',u'wait allocation'),
                                              ('allocated',u'Allocated'),
                                              ('returned',u'Returned')
                                              ],'Status', readonly=True),
                'allocation_date': fields.date('Allocation Date'),
                'return_date': fields.date('Return Date'),
                'note': fields.text('Note'),
    }
    
    def check_asset_employee(self, cr, uid, ids, context=None):        
        for allocation in self.browse(cr, uid, ids, context=context):
            search_agrs = [('asset_id','=',allocation.asset_id.id),('state','=','allocated'),('id','!=',allocation.id)]
            res_search = self.search(cr, uid, search_agrs, context=context)
            if res_search:
                return False
            
        return True
    
    _constraints = [
                    (check_asset_employee,'\nAsset is allocated to other employee!', ['asset_id']),
                  
    ]
    
    def _employee_get(self, cr, uid, context=None):
        ids = self.pool.get('hr.employee').search(cr, uid, [('user_id', '=', uid)], context=context)
        if ids:
            return ids[0]
        return False
    
    _defaults = {
        'state':'draft',
        'employee_id': _employee_get
    }
    
    def submit(self, cr ,uid, ids, context=None):
        
        return self.write(cr, uid, ids, {'state': 'wait_approval'}, context=context)
    
    def approve(self, cr ,uid, ids, context=None):
        
        return self.write(cr, uid, ids, {'state': 'wait_allocation'}, context=context)
    
    def set_allocate(self, cr ,uid, ids, context=None):
        
        alloc_date = datetime.now().strftime('%Y-%m-%d')
        
        return self.write(cr, uid, ids, {'state': 'allocated','allocation_date' : alloc_date}, context=context)
    
    def set_return(self, cr ,uid, ids, context=None):
        
        return_date = datetime.now().strftime('%Y-%m-%d')
        
        return self.write(cr, uid, ids, {'state': 'returned','return_date':return_date}, context=context)
    
    def set_refuse(self, cr ,uid, ids, context=None):
        
        return self.write(cr, uid, ids, {'state': 'refuse'}, context=context)

feosco_hr_asset_allocation()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
