# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'FEOSCO HR',
    'version': '1.1',
    'author': 'Feosco',
    'category': 'Feosco',
    'sequence': 21,
    'website': 'http://www.feosco.com',
    'summary': 'Install all module',
    'description': """
        Feosco HR Module: allow to manage HR function in company
    """,
    'author': 'Feosco',
    'website': 'http://www.feosco.com',
    'images': [
    ],
    'depends': ['feosco_base',
                'feosco_hr',
                'feosco_hr_business_trip',
                'feosco_hr_contract',
                'feosco_hr_payroll',
                'feosco_hr_recruitment',
                'feosco_hr_holidays',
                'feosco_hr_insurance',
                'feosco_hr_training',
                'translate',
                'feosco_web_adblock',
                # 'feosco_account_asset_printer',
                
                
                ],
    'data': [
        
        
    ],
    'demo': [],
    'test': [
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'css': [],
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
