# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name': 'FEOSCO Insurance Management',
    'version': '1.5',
    'author': 'Feosco',
    'category': 'Feosco',
    'sequence': 27,
    'summary': 'Insurance Management',
    'website': 'http://www.feosco.com',
    'description': """
            This module alow to manage insurance
""",
    'images': [],
    'depends': ['base', 'feosco_base','hr','feosco_hr'],
    'data': [
             'view/menu.xml',
             'view/health_insurance.xml',
             'view/social_insurance.xml',
             'view/unemp_insurance.xml',
             'view/health_insurance_tracking.xml',
             'view/health_insurance_config_view.xml',
             'security/ir.model.access.csv',
             'demo/cronjob_init.xml',
             'demo/registration_place.xml'
        ],
    'demo': [],
    'test': [
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
