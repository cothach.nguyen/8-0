# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import fields, osv


class feosco_hr_health_insurance_tracking(osv.osv):
    _name = "feosco.hr.health.insurance.tracking"
    _description = "Employee's health insurance Management"
    
    def default_get(self, cr, uid, fields, context=None):
        if context is None:
            context = {}
        res = super(feosco_hr_health_insurance_tracking, self).default_get(cr, uid, fields, context=context)
        
        if context.has_key('default_insurance'):
            res.update({'health_insurance_id': context.get('default_insurance', False) })
            
        return res
    
    _columns = {
                'health_insurance_id': fields.many2one('feosco.hr.health.insurance', 'Health Insurance'),
                'exam_place': fields.char('Exam Place', size=64, help='Registration Place'),
                'from_date': fields.date('From Date'),
                'to_date': fields.date('To Date'),
                'note': fields.text('Note')
    }


    _constraints = [
    ]
    
    _defaults = {
                 
                }

feosco_hr_health_insurance_tracking()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
