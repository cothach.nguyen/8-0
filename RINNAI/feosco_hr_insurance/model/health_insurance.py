# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import fields, osv


class feosco_hr_health_insurance(osv.osv):
    _name = "feosco.hr.health.insurance"
    _description = "Employee's health insurance Management"
    
    
    def _check_date(self, cr, uid, ids, context=None):
        health_obj = self.browse(cr, uid, ids, context=context)
        for health in health_obj:
            if health.from_date and health.to_date and health.from_date > health.to_date:
                return False
        return True
    
    _columns = {
                'employee_id': fields.many2one('hr.employee', 'Employee', readonly=True,
                                                 states={
                                                   'new': [('readonly', False)],
                                                   'active': [('readonly', False)]
                                                    }),
                
                'name': fields.char('Insurance Number', size=64, help='Insurance Number', readonly=True,
                                                 states={
                                                   'new': [('readonly', False)],
                                                   'active': [('readonly', False)]
                                                    }),
                
                'registration_place': fields.many2one('feosco.hr.health.insurance.config', 'Registration Place', readonly=True,
                                                 states={
                                                   'new': [('readonly', False)],
                                                   'active': [('readonly', False)]
                                                    }),
                
                'code': fields.char('Code', size=64, help='Code', readonly=True,
                                                 states={
                                                   'new': [('readonly', False)],
                                                   'active': [('readonly', False)]
                                                    }),
                
                'from_date': fields.date('From Date', readonly=True,
                                                 states={
                                                   'new': [('readonly', False)],
                                                   'active': [('readonly', False)]
                                                    }),
                
                'to_date': fields.date('To Date', readonly=True,
                                                 states={
                                                   'new': [('readonly', False)],
                                                   'active': [('readonly', False)]
                                                    }),
                
                'note': fields.text('Note'),
                
                'state': fields.selection([('new',u'New'),
                                           ('active',u'Active'),
                                           ('expired',u'Expired')],
                                            'Status', readonly=True),
                'health_insurance_tracking_ids' : fields.one2many('feosco.hr.health.insurance.tracking', 'health_insurance_id', 'Examination history'),
    }

    _constraints = [
                    (_check_date,'input to date again,please !', ['to_date']),
    ]
    
    _defaults = {
                 'state': 'new'
                }
    
    def create(self, cr, uid, vals, context=None):
        
        vals.update({'state': 'active'})
        
        return super(feosco_hr_health_insurance, self).create(cr, uid, vals, context=context)
    
    def set_expired(self, cr ,uid, ids, context=None):
        
        return self.write(cr, uid, ids, {'state': 'expired'}, context=context)
    

feosco_hr_health_insurance()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
