# -*- coding: utf-8 -*-
from openerp.osv import fields, osv
import logging
from datetime import datetime, date, timedelta

class feosco_hr_insurance_cronjob(osv.osv):
    _name = "feosco.hr.insurance.cronjob"
    __logger = logging.getLogger(_name)
    _auto=False
    
    def feosco_auto_stop_health_insurance(self, cr, uid, automatic=False, use_new_cursor=False, context=None):
        self.__logger.info('START: feosco_auto_stop_health_insurance')
        
        try:
            if context == None: context={}
            
            health_insurance_pool = self.pool.get('feosco.hr.health.insurance')
            
            agrs = [
                ( 'to_date','<',datetime.now().strftime('%Y-%m-%d') ),
                ('state','=','active')
                ]
        
            health_insurance_ids = health_insurance_pool.search(cr,uid, agrs, context=context)
            
            if health_insurance_ids:
                
                health_insurance_pool.set_expired(cr, uid, health_insurance_ids, context=context)
                self.__logger.info('Insurance ids are expired %s' %( str(health_insurance_ids) ))
        
        except Exception as exc:
            self.__logger.info('Can NOT set expired Insurance %s' %(exc))
            
        self.__logger.info('END: feosco_auto_stop_health_insurance')
        return True

feosco_hr_insurance_cronjob()