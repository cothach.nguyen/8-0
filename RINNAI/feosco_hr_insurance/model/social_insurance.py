# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import fields, osv
import time
from datetime import datetime

class feosco_hr_social_insurance(osv.osv):
    _name = "feosco.hr.social.insurance"
    _description = "Employee's social insurance Management"
    
    def get_month_total(self, cr, uid, ids, field_name, field_value, arg, context=None):
        insurance_obj= self.browse(cr, uid, ids)
        result={}
        for insurance in insurance_obj:
            if insurance.from_date:
                from_date_so=datetime.strptime(insurance.from_date,'%Y-%m-%d')         
                to_date_so=datetime.strptime(insurance.to_date,'%Y-%m-%d') if insurance.to_date else datetime.today()
                social_date=(to_date_so-from_date_so) 
                int_days=(social_date.days)
                result[insurance.id]={'years_total':int(int_days/365),
                              'month_total':(float(int_days))*12/365-(int(int_days/365))*12
                              }
            else:
                result[insurance.id]={'years_total':0,
                              'month_total':0
                              }
        return result
    
    def _check_date(self, cr, uid, ids, context=None):
        insurance_obj = self.browse(cr, uid, ids, context=context)
        for insurance in insurance_obj:
            if insurance.from_date and insurance.to_date and insurance.from_date > insurance.to_date:
                return False
        return True
    
    _columns = {
                'name': fields.char('Insurance Number', size=64, help='Insurance Number'),
                'employee_id': fields.many2one('hr.employee', 'Employee'),
                'code': fields.char('Code', size=64, help='Code'), # So thẻ  BHXH
                'from_date': fields.date('From Date'),
                'to_date': fields.date('To Date'),
                #'years_total': fields.float('Total years', digits=(16,1)),
                'years_total': fields.function(get_month_total,string='Years total',type='integer',multi='total'),
                'month_total': fields.function(get_month_total,string='Months total',type='float',multi='total'),
                'note': fields.text('Note'),
    }
    
    
    _constraints = [
                    (_check_date,'input to date again,please !', ['to_date']),
    ]
    
    _defaults = {
                }

feosco_hr_social_insurance()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
