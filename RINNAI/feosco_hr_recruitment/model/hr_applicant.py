#-*- coding:utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv

AVAILABLE_PRIORITIES = [
    ('5', 'Not Good'),
    ('4', 'On Average'),
    ('3', 'Good'),
    ('2', 'Very Good'),
    ('1', 'Excellent')
]

class hr_applicant(osv.Model):
    _inherit = 'hr.applicant'
    
    _columns = {
                'priority': fields.selection(AVAILABLE_PRIORITIES, 'Appreciation'),
                }
    
    def _check_job_by_department(self, cr, uid, ids, context=None):
        for applicant in self.browse(cr, uid, ids, context=context):
            if applicant.job_id and applicant.department_id and applicant.job_id.department_id.id != applicant.department_id.id:
                return False

        return True
    
    _constraints = [(_check_job_by_department, "Job is not belong to department", ['job_id','department_id'])]
    
hr_applicant()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
