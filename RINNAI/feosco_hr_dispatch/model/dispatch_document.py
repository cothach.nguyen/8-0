# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import addons
import logging
from openerp.osv import fields, osv
from datetime import datetime
import openerp


class feosco_hr_dispatch_document(osv.osv):
    _name = "feosco.hr.dispatch.document"
    _description = "Dispatch Document  Management"
    _inherits = {'ir.attachment': 'attachment_id'}
    
    def _compute_version(self, cr, uid, ids, field, arg, context=None):
        res = dict.fromkeys(ids, False)
        for current_attachment_id in ids:
            # Đọc toàn bộ object hiện tại
            dispatch = self.read(cr, uid, current_attachment_id, ['dispatch_id','version'], context=context)
            dispatch_id = dispatch.get('dispatch_id')[0]
            version = dispatch.get('version')
            if not version:
                # get all document of dispatch id  {'version': 2, 'attachment_id': 27, 'dispatch_id': (13, u'Test'), 'id': 19}
                attachment_ids = self.search(cr, uid, [('dispatch_id','=', dispatch_id)], context=context)
                versions = [attachment.version for attachment in self.browse(cr, uid, attachment_ids, context=context)]
                res[current_attachment_id] = max(versions) + 1
            else:
                res[current_attachment_id] = version
                
        return res
    
    _columns = {
                    'state': fields.selection([('draft','Draft'),
                                               ('verify','Verified'),
                                              ('approve','Approved'),
                                              ('cancel','Cancelled'),], 'Status'),
                    'version': fields.function(_compute_version, store=True, type='integer', string='Version'),
                    
                    'dispatch_id': fields.many2one('feosco.hr.dispatch', 'Dispatch', ondelete='cascade'),
               }
    _constraints = [
    ]
    
    _defaults = {
                'state': 'draft'
       }
    
    def set_verify(self, cr ,uid, ids, context=None):
        
        return self.write(cr, uid, ids, {'state': 'verify'}, context=context)
    
    def set_approve(self, cr ,uid, ids, context=None):
        
        return self.write(cr, uid, ids, {'state': 'approve'}, context=context)
    
    def set_cancel(self, cr ,uid, ids, context=None):
        
        return self.write(cr, uid, ids, {'state': 'cancel'}, context=context)

feosco_hr_dispatch_document()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
