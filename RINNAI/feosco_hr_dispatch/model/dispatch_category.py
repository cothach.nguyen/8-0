# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import addons
import logging
from openerp.osv import fields, osv
from datetime import datetime
import openerp


class feosco_hr_dispatch_category(osv.osv):
    _name = "feosco.hr.dispatch.category"
    _description = "Dispatch Category"
    
    _columns = {
                    'code': fields.char('Code', size=64, help='Code'),
                    'name': fields.char('Name', size=64, help='Name'),
                    'description':fields.text('Description', size=256),
               }
    _constraints = [
    ]
    
    _defaults = {
                
       }

feosco_hr_dispatch_category()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
