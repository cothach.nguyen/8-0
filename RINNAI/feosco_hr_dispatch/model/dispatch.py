# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import addons
import logging
from openerp.osv import fields, osv
from datetime import datetime
import openerp


class feosco_hr_dispatch(osv.osv):
    _name = "feosco.hr.dispatch"
    _description = "Dispatch Management"
    
    _columns = {
                    'code': fields.char('Code', size=64),
                    'name': fields.char('Name', size=64),
                    'category_id': fields.many2one('feosco.hr.dispatch.category', 'Category'),
                    'type': fields.selection([('in','Incoming'),
                                              ('out','Out Coming'),
                                              ('internal','Internal'),], 'Type'),
                    'priority': fields.selection([('low','Low'),
                                              ('medium','Medium'),
                                              ('high','High'),], 'Priority'),
                    'state': fields.selection([('draft','New'),
                                              ('verify','Verified'),
                                              ('approve','Approved'),
                                              ('cancel','Cancelled'),], 'Status'),
                    'journal_number': fields.integer('Journal Number'),
                    'department_id': fields.many2one('hr.department', 'Assigned to department'),
                    'user_id': fields.many2one('res.users', 'Assigned to user'),
                    'send_department': fields.many2one('hr.department', 'From Department'),
                    
                    'from': fields.many2one('res.partner', 'From'),
                    'to': fields.many2one('res.partner', 'To'),
                    'date': fields.date('Date'),
                    
                    'attachment_ids' : fields.one2many('feosco.hr.dispatch.document','dispatch_id','Dispatch Attachment'),
                    'summary' : fields.text('Summary Content'),
                    'note' : fields.text('Note'),
               }
    _constraints = [
    ]
    
    _sql_constraints = [
        ('journal_number_uniq', 'unique(journal_number)', 'Journal Number must be unique!'),
    ]
    
    _defaults = {
                'state': 'draft'
       }
    
    def set_draft(self, cr ,uid, ids, context=None):
        
        return self.write(cr, uid, ids, {'state': 'draft'}, context=context)
    
    def set_verify(self, cr ,uid, ids, context=None):
        
        return self.write(cr, uid, ids, {'state': 'verify'}, context=context)
    
    def set_approve(self, cr ,uid, ids, context=None):
        
        return self.write(cr, uid, ids, {'state': 'approve'}, context=context)
    
    def set_cancel(self, cr ,uid, ids, context=None):
        
        return self.write(cr, uid, ids, {'state': 'cancel'}, context=context)

feosco_hr_dispatch()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
