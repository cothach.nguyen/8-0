��    C      4  Y   L      �     �     �     �     �     �          &     .     7     =     B     H     T  	   [     e     n     �  	   �  
   �     �     �     �     �     �     �     �     �  &   �           &     3     A     P  
   j     u     |     �     �     �     �     �     �  	   �     �     �     �     �     �     �               %     6     C     P     `  
   l     w     �     �  
   �     �     �     �     �  [   �  �  8	     �
     �
     �
       $   )  $   N     s     �     �     �     �     �  
   �     �     �     �          #  
   4  
   ?     J     S     c     x     �     �     �  :   �     �     �          "  "   9     \     j     w     �     �  
   �     �     �     �     �  
                   #     )     5     F     \     y     �     �     �     �     �  $   �          #     8  
   D     O  $   j     �  u   �            2   9   !   >   %                 =   0       
                   *   #                   3      "          ,              -   /   (   :       7   &                    	   ;   $   B       4      '             C   1                   ?       <       )         5          8       .                @   A      6      +    Active Allocation Mode Allocation Request Allocation Requests Allocation Requests to Approve Apply Double Validation Approve Approved Black Blue Brown By Employee Cancel Cancelled Category Category of Employee Color in Report Confirmed Department Department(s) Description Details Employee Employee(s) End Date From Group By... HR Leaves Summary Report By Department Leave Leave Detail Leave Request Leave Requests Leave Requests to Approve Leave Type Leaves Leaves Management Leaves by Department Light Green Magenta Manager Meeting Misc My Leaves Number of Days Parent Print Red Refuse Refused Remaining Days Remaining Leaves Remaining leaves Request Type Search Leave Second Approval Sick Leaves Start Date Total holidays by type Type User Validation Violet Waiting Approval Waiting Second Approval Warning! You cannot modify a leave request that has been approved. Contact a human resource manager. Project-Id-Version: openobject-addons
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2013-06-07 19:36+0000
PO-Revision-Date: 2013-12-05 18:25+0700
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Vietnamese <vi@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2013-11-21 06:12+0000
X-Generator: Poedit 1.5.4
 Hoạt động Chế độ phân bổ Phân bổ ngày phép Cấu hình ngày phép Phân bổ ngày phép cần duyệt Thực hiện Xét duyệt Hai lần Chấp thuận Được chấp thuận Đen Xanh dương Nâu Theo Người lao động Hủy bỏ Bị hủy bỏ Phân loại Nhóm Người lao động Màu trong Báo cáo Đã xác nhận Phòng ban Phòng ban Mô tả Các chi tiết Người lao động Người lao động Ngày kết thúc Từ Nhóm theo... Thống kê ngày nghỉ của nhân viên theo phòng ban Ngày phép Chi tiết ngày phép Yêu cầu Nghỉ Các yêu cầu Nghỉ Ngày phép cần được duyệt Loại nghỉ Ngày nghỉ Quản lý Ngày nghỉ Ngày nghỉ theo Phòng ban Xanh lá nhạt Đỏ tía Người quản lý Cuộc họp Các thứ khác Ngày nghỉ của tôi Số ngày Cha In Đỏ Từ chối Đã từ chối Số ngày còn lại Số ngày nghỉ còn lại Số ngày nghỉ còn lại Loại Yêu cầu Tìm kiếm Ngày nghỉ Chấp thuận Lần 2 Nghỉ ốm Ngày bắt đầu Tổng số ngày nghỉ theo loại Loại Người sử dụng Xác nhận Tím sẫm Đang chờ Chấp thuận Đang chờ Chấp thuận Thứ hai Cảnh báo! Bạn không thể sửa ngày nghỉ đã duyệt. Liên lạc với quản lý nhân sự để được hỗ trợ 