# -*- coding: utf-8 -*-
from openerp.tools.translate import _
from openerp.osv import osv, fields
import logging
import openerp.pooler

class old_allocation_request_option_memory(osv.osv_memory):
    _name = "old.allocation.request.option.memory"

    _columns = {
                'option': fields.selection([('add','Add'),('remove','Remove')], 'Option'),
                'number_of_days': fields.float('Number of days', help="Số ngày phép cũ được chuyển sang năm mới, 0 là chuyển tất cả"),
                
    }
    
    
    
    def create_allocation_request(self, cr, uid, ids, context=None):
        
        holiday_pool = self.pool.get('hr.holidays')
        
        holiday_status_pool = self.pool.get('hr.holidays.status')
        
        holiday_status_ids = context.get('active_ids', None)
        
        this = self.browse(cr, uid, ids[0], context=context)
        res = holiday_pool.create_allocation_request_for_employee(cr, uid, holiday_status_ids=holiday_status_ids, 
                                                                    old_allocation_opt = this.option, 
                                                                    allow_allocation = this.number_of_days, 
                                                                    context=context)
        if res:
            holiday_status_pool.write(cr, uid, holiday_status_ids, { 'feosco_created_allocation' : True }, context=context)
            
        
        
        result = {
            'type': 'ir.actions.act_window_close',
        }

        return result
        
old_allocation_request_option_memory()