# -*- coding: utf-8 -*-
##################################################################################
#
# Copyright (c) 2005-2006 Axelor SARL. (http://www.axelor.com)
# and 2004-2010 Tiny SPRL (<http://tiny.be>).
#
# $Id: hr.py 4656 2006-11-24 09:58:42Z Cyp $
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as
#     published by the Free Software Foundation, either version 3 of the
#     License, or (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
#
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import time
from datetime import datetime
from datetime import timedelta
from datetime import date

from openerp import tools
from openerp.osv import fields, osv
from openerp.tools.translate import _


class hr_holidays(osv.osv):
    _inherit = "hr.holidays"

    def _check_is_manager(self, cr, uid, ids, field, arg, context=None):
        res = dict.fromkeys(ids, False)
        for allocation in self.browse(cr, uid, ids, context=context):
            user_pool = self.pool.get('res.users')
            # Neu nhan vien thuoc group HR manager, co the approve cho chinh minh
            if user_pool.has_group(cr, uid, 'base.group_hr_manager'):
                continue
            # Neu user thuoc group HR user va Dept Head => Co the approve cho nhan vien, khong the approve cho minh
            if allocation.employee_id.user_id.id == uid and \
                    (user_pool.has_group(cr, uid, 'base.group_hr_dept_head')
                     or user_pool.has_group(cr, uid, 'base.group_hr_user')):
                res[allocation.id] = True
        return res

    def _check_invisible_cancel(self, cr, uid, ids, field, arg, context=None):
        res = dict.fromkeys(ids, False)
        for holiday in self.browse(cr, uid, ids, context=context):
            current = time.strftime("%Y-%m-%d %H:%M:%S")
            if holiday.state == 'validate' and holiday.date_from <= current:
                res[holiday.id] = True
        return res

    _columns = {
        'employee_is_manager': fields.function(_check_is_manager, type='boolean', string='Employee is manager'),
        'invisible_cancel': fields.function(_check_invisible_cancel, type='boolean', string='Invisible Cancel'),
        'state': fields.selection([('draft', 'To Submit'), ('confirm', 'To Approve'), ('validate1', 'Second Approval'),
                                   ('validate', 'Approved'), ('refuse', 'Refused'), ('cancel', 'Cancelled')],
                                  'Status', readonly=True, track_visibility='onchange',
                                  help='The status is set to \'To Submit\', when a holiday request is created.\
                        \nThe status is \'To Approve\', when holiday request is confirmed by user.\
                        \nThe status is \'Refused\', when holiday request is refused by manager.\
                        \nThe status is \'Approved\', when holiday request is approved by manager.'),
    }

    def _compute_allocation_for_new_emp(self, cr, uid, start_date, total_allocation_date=12, remain_allocation=0,
                                        allow_allocation=0, context=None):
        '''
        Input: 
            start_date: the date that allocation date will be start to apply
            total_allocation_date: total allocation date that is used to computed actual allocation date of employee
        Output: actual allocation date of employee
        Desc: This function used to compute computed actual allocation date of employee in year
        '''

        deduct_allocation = remain_allocation - allow_allocation if remain_allocation > allow_allocation else 0

        res = 0
        if start_date:
            start_date = datetime.strptime(start_date, '%Y-%m-%d')
            current_date = start_date.day
            join_month = start_date.month

            if 1 <= current_date <= 10:  # Tổng số ngày phép = total_allocation_date - tháng hiện tại + 1
                res = (total_allocation_date - join_month + 1) - deduct_allocation
            elif 11 <= current_date <= 20:  # Tống số ngày phép = total_allocation_date - tháng hiện tại + 0.5
                res = total_allocation_date - join_month + 0.5 - deduct_allocation
            else:
                res = total_allocation_date - join_month - deduct_allocation

        return res if res >= 0 else 0


    def create_allocation_request_for_employee(self, cr, uid, employee_ids=None,
                                               start_date=None,
                                               holiday_status_ids=None,
                                               old_allocation_opt='add',
                                               allow_allocation=0,
                                               context=None):

        if context == None:
            context = {}

        res = {}
        holiday_status_pool = self.pool.get('hr.holidays.status')
        employee_pool = self.pool.get('hr.employee')

        if holiday_status_ids == None:
            holiday_status_ids = holiday_status_pool.search(cr, uid, [('limit', '=', False),
                                                                      ('feosco_period', '=', int(date.today().year))],
                                                            context=context)
        if employee_ids == None:
            employee_ids = employee_pool.search(cr, uid, [], context=context)
        if start_date == None:
            start_date = '%s-01-01' % ( str(date.today().year) )

        if holiday_status_ids:

            for status_obj in holiday_status_pool.browse(cr, uid, holiday_status_ids, context=context):
                for employee in employee_pool.browse(cr, uid, employee_ids, context=context):

                    # Check old allocation of employee
                    if old_allocation_opt == 'add':
                        if allow_allocation <= 0:
                            remain_allocation = 0
                        else:
                            remain_allocation = employee.remaining_leaves
                    elif old_allocation_opt == 'remove':
                        remain_allocation = employee.remaining_leaves
                        allow_allocation = 0

                    # Create data
                    create_data = {
                        'name': 'allowcation request of %s for %s ' % (employee.name, str(date.today().year)),
                        'holiday_status_id': status_obj.id,
                        'employee_id': employee.id,
                        'number_of_days_temp': self._compute_allocation_for_new_emp(cr, uid, start_date,
                                                                                    status_obj.feosco_default_allocation,
                                                                                    remain_allocation,
                                                                                    allow_allocation,
                                                                                    context=context),
                        'type': 'add'
                    }
                    context.update({'default_type': 'add'})
                    holidaty_id = self.create(cr, uid, create_data, context=context)
                    # Approve this holiday
                    self.holidays_validate(cr, uid, [holidaty_id], context=context)

                    res[employee.id] = holidaty_id
        else:
            raise osv.except_osv(_('Error!'), _('There is no Leave Type created for this year'))

        return res

    def _compute_allocation_exit_emp(self, cr, uid, end_date, remain_allocation=0, total_allocation_date=12,
                                     context=None):

        res = 0

        if end_date:

            end_date = datetime.strptime(end_date, '%Y-%m-%d')
            last_date = end_date.day
            last_month = end_date.month

            if 1 <= last_date <= 10:
                res = remain_allocation - (total_allocation_date - last_month + 1)

            elif 11 <= last_date <= 20:
                res = remain_allocation - (total_allocation_date - last_month + 0.5)

            else:
                res = remain_allocation - (total_allocation_date - last_month)

        return res if res >= 0 else 0


    def update_allocation_request_for_employee(self, cr, uid, employee_ids, holiday_status_ids=None, end_date=None,
                                               context=None):

        holiday_pool = self.pool.get('hr.holidays')
        holiday_status_pool = self.pool.get('hr.holidays.status')
        employee_pool = self.pool.get('hr.employee')

        if holiday_status_ids == None:
            holiday_status_ids = holiday_status_pool.search(cr, uid, [('limit', '=', False),
                                                                      ('feosco_period', '=', int(date.today().year))],
                                                            context=context)

        if end_date == None:
            end_date = date.today().strftime('%Y-%m-%d')

        if holiday_status_ids:

            for status_obj in holiday_status_pool.browse(cr, uid, holiday_status_ids, context=context):

                for employee in employee_pool.browse(cr, uid, employee_ids, context=context):

                    search_agrs = [('employee_id', '=', employee.id), ('holiday_status_id', '=', status_obj.id),
                                   ('type', '=', 'add')]

                    holiday_ids = self.search(cr, uid, search_agrs, context=context)

                    if holiday_ids:
                        remain_allocation = employee.remaining_leaves

                        for holiday in holiday_pool.browse(cr, uid, holiday_ids, context=context):
                            total_allocation_date = holiday.number_of_days_temp

                            number_of_days = self._compute_allocation_exit_emp(cr, uid, end_date, remain_allocation,
                                                                               total_allocation_date, context=context)

                            write_data = {'number_of_days_temp': number_of_days}

                            self.write(cr, uid, holiday_ids, write_data, context=context)

        return True

    def onchange_date_from(self, cr, uid, ids, date_to, date_from):
        """
        If there are no date set for date_to, automatically set one 8 hours later than
        the date_from.
        Also update the number_of_days.

        Thach:
            Override lai ham nay:
             1. de fix vi ham standard tinh sai. Vi du:
                from date = 05/19/2014 00:00:00
                to date = 05/21/2014 00:00:00
                => Standard tinh tong so ngay nghi (number_of_days_temp) la: 3 days, dung la chi co 2 ngay

            2. Tinh ngay nghi theo working scheduler. => Trong Standard, khi tao ngay nghi vao thu 7, CN thi cung bi tru ngay nghi
                    Vi du: xin nghi tu thu 6 den thu 2 => Standard tinh la 4 ngay, dung la chi co 2 ngay (thu 6, thu 2)
        """
        # date_to has to be greater than date_from
        if (date_from and date_to) and (date_from > date_to):
            raise osv.except_osv(_('Warning!'), _('The start date must be anterior to the end date.'))

        result = {'value': {}}

        # No date_to set so far: automatically compute one 8 hours later
        if date_from and not date_to:
            date_to_with_delta = datetime.strptime(date_from, tools.DEFAULT_SERVER_DATETIME_FORMAT) + timedelta(hours=8)
            result['value']['date_to'] = str(date_to_with_delta)

        # Compute and update the number of days
        if (date_to and date_from) and (date_from <= date_to):
            diff_day = self._get_number_of_days(cr, uid, ids, date_from, date_to)
            result['value']['number_of_days_temp'] = diff_day
        else:
            result['value']['number_of_days_temp'] = 0

        return result

    def _get_number_of_days(self, cr, uid, ids, date_from, date_to):

        calendar_id = False

        diff_day = super(hr_holidays, self)._get_number_of_days(date_from, date_to)

        from_dt = datetime.strptime(date_from, "%Y-%m-%d %H:%M:%S")

        resource_obj = self.pool.get('resource.resource')
        nb_of_days = round(diff_day) + 1
        res_id = resource_obj.search(cr, uid, [('user_id', '=', uid)])
        for resource in resource_obj.browse(cr, uid, res_id):
            calendar_id = resource.calendar_id
            if not calendar_id:
                continue

        if calendar_id:
            diff_day = 0
            for day in range(0, int(nb_of_days)):
                working_hours_on_day = self.pool.get('resource.calendar').working_hours_on_day(cr, uid, calendar_id,
                                                                                               from_dt + timedelta(
                                                                                                   days=day))
                if working_hours_on_day:
                    diff_day += 1.0
        return diff_day

    def onchange_date_to(self, cr, uid, ids, date_to, date_from):
        """
        Update the number_of_days.
        Thach:
            Override lai ham nay:
             1. de fix vi ham standard tinh sai. Vi du:
                from date = 05/19/2014 00:00:00
                to date = 05/21/2014 00:00:00
                => Standard tinh tong so ngay nghi (number_of_days_temp) la: 3 days, dung la chi co 2 ngay

            2. Tinh ngay nghi theo working scheduler. => Trong Standard, khi tao ngay nghi vao thu 7, CN thi cung bi tru ngay nghi
                    Vi du: xin nghi tu thu 6 den thu 2 => Standard tinh la 4 ngay, dung la chi co 2 ngay (thu 6, thu 2)
        """

        # date_to has to be greater than date_from
        if (date_from and date_to) and (date_from > date_to):
            raise osv.except_osv(_('Warning!'), _('The start date must be anterior to the end date.'))

        result = {'value': {}}

        # Compute and update the number of days
        if (date_to and date_from) and (date_from <= date_to):
            diff_day = self._get_number_of_days(cr, uid, ids, date_from, date_to)
            result['value']['number_of_days_temp'] = diff_day
        else:
            result['value']['number_of_days_temp'] = 0

        return result

    def holidays_cancel_customize(self, cr, uid, ids, context=None):
        '''
            This function is used to cancel a holiday request. 
            It call holidays_cancel to cancel and change the state to 'cancel'
        '''
        res = self.holidays_cancel(cr, uid, ids, context=context)
        if res:
            for record in self.browse(cr, uid, ids):
                #Set state to cancelled
                obj_emp = self.pool.get('hr.employee')
                ids2 = obj_emp.search(cr, uid, [('user_id', '=', uid)])
                manager = ids2 and ids2[0] or False
                if record.state == 'validate1':
                    self.write(cr, uid, [record.id], {'state': 'cancel', 'manager_id': manager})
                else:
                    self.write(cr, uid, [record.id], {'state': 'cancel', 'manager_id2': manager})
        return res


    def _needaction_domain_get(self, cr, uid, context=None):
        dom = False
        user_pool = self.pool.get('res.users')
        emp_obj = self.pool.get('hr.employee')

        if user_pool.has_group(cr, uid, 'base.group_hr_manager') or user_pool.has_group(cr, uid, 'base.group_hr_user'):
            dom = [('state', 'in', ('confirm', 'validate1'))]

        elif user_pool.has_group(cr, uid, 'base.group_hr_dept_head'):
            employee_ids = emp_obj.search(cr, uid, [('department_id.manager_id.user_id.id', '=', uid)], context=context)
            if employee_ids:
                dom = [('state', 'in', ('confirm', 'validate1')),
                       ('employee_id', 'in', employee_ids)]

        elif user_pool.has_group(cr, uid, 'base.group_user'):
            employee_ids = emp_obj.search(cr, uid, [('user_id.id', '=', uid)], context=context)
            current_date = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
            dom = [('state', 'in', ('refuse', 'validate')),
                   ('employee_id', 'in', employee_ids),
                   ('date_from', '>=', current_date)]
        return dom


hr_holidays()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
