# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from datetime import datetime


class feosco_hr_employee_exit(osv.osv):
    _inherit = "feosco.hr.employee.exit"
    
    def set_approve(self, cr, uid, ids, context=None):
        res = super(feosco_hr_employee_exit, self).set_approve(cr, uid, ids, context=context)
        
        if res:
            
            holiday_pool = self.pool.get('hr.holidays')
            
            for exit_obj in self.browse(cr, uid, ids, context=context):
            
                employee_id = exit_obj.feosco_employee_id.id
                date_end = exit_obj.feosco_working_end_date
                
                holiday_pool.update_allocation_request_for_employee(cr, uid, [employee_id], end_date = date_end, context=context)
            
        return res
    
feosco_hr_employee_exit()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
