# -*- coding: utf-8 -*-
##################################################################################
#
# Copyright (c) 2005-2006 Axelor SARL. (http://www.axelor.com)
# and 2004-2010 Tiny SPRL (<http://tiny.be>).
#
# $Id: hr.py 4656 2006-11-24 09:58:42Z Cyp $
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as
#     published by the Free Software Foundation, either version 3 of the
#     License, or (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
#
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from datetime import date
from openerp.osv import fields, osv


class hr_holidays_status(osv.osv):
    _inherit = "hr.holidays.status"
    
    _columns = {
                    'feosco_created_allocation': fields.boolean('Created allocation request'),
                    'feosco_default_allocation': fields.float('Default Allocation', help="Default allocation date"),
                    'feosco_period': fields.integer('Period'),
                }
    
    _defaults = {
                    'feosco_default_allocation' : 12,
                    'feosco_period': int(date.today().year)
                 }
        
hr_holidays_status()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
