# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import addons
import logging
from openerp.osv import fields, osv
from datetime import datetime

_logger = logging.getLogger(__name__)



class hr_contract(osv.osv):
    _inherit = "hr.contract"
    
    def create(self, cr, uid, vals, context=None):
        contract_id = super(hr_contract, self).create(cr, uid, vals, context=context)
        
        # Auto create allocation request for employee
        if contract_id:
            holiday_pool = self.pool.get('hr.holidays')
            employee_id = vals.get('employee_id',False)
            date_start = vals.get('date_start',False)
            
            # Create allocation request for employee
            # Check that employee has allocation request
            holiday_ids = holiday_pool.search(cr, uid, [('employee_id','=',employee_id)], context=context)
            if not holiday_ids:
                holiday_pool.create_allocation_request_for_employee(cr, uid, [employee_id], date_start, context=context)
            
            # Update working day for employee in resource_resource
            calendar_id = vals.get('working_hours',False)
            if calendar_id and employee_id:
                
                resource_pool = self.pool.get('resource.resource')
                employee_pool = self.pool.get('hr.employee')
                employee_obj = employee_pool.browse(cr, uid, employee_id, context=context)
                user_id = employee_obj.user_id.id if employee_obj.user_id else False
                
                if user_id:
                    res_id = resource_pool.search(cr, uid,[('user_id', '=', user_id)], context=context)
                    
                    if res_id:
                        resource_pool.write(cr, uid, res_id, {'calendar_id': calendar_id}, context=context)
                        
                    else:
                        create_data = {
                                       'name': employee_obj.user_id.name,
                                       'user_id': user_id,
                                       'calendar_id': calendar_id
                                       }
                        resource_pool.create(cr, uid, create_data, context=context)
                
        return contract_id
    
hr_contract()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
