# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import time
from datetime import datetime
from dateutil import relativedelta

from openerp.osv import fields, osv
from openerp.tools.translate import _

class hr_payslip_employees(osv.osv_memory):

    _inherit ='hr.payslip.employees'

    
    def compute_sheet(self, cr, uid, ids, context=None):
        res = super(hr_payslip_employees, self).compute_sheet(cr, uid, ids, context=context)
        feosco_return_data = context.get('feosco_confirm_conflict',False)
        if feosco_return_data:
            # Call wizard
            compose_form_id = self.pool.get('ir.model.data').get_object_reference(cr, uid,'feosco_hr_payroll','view_confirm_allow_duplicate_payslip_form')[1]
            return {
                    'name': 'Warning',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'feosco.confirm.allow.duplicate.payslip',
                    'views': [(compose_form_id, 'form')],
                    'view_id': compose_form_id,
                    'target': 'new',
                    'context': context,
                }
        return res

hr_payslip_employees()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
