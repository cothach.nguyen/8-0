# -*- coding: utf-8 -*-
from openerp.tools.translate import _
from openerp.osv import osv, fields
import logging
import openerp.pooler


class confirm_allow_duplicate_payslip(osv.osv_memory):
    _name = "feosco.confirm.allow.duplicate.payslip"

    def create_payslip(self, cr, uid, ids, context=None):
        payslip_pool = self.pool.get('hr.payslip')
        payslip_ids = context.get('active_ids',False)
        context.update({'check_conflict': False})
        payslip_pool.compute_sheet(cr, uid, payslip_ids, context=context)
        return {'type': 'ir.actions.act_window_close'}

    def delete_payslip(self, cr, uid, ids, context=None):
        compose_form_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'feosco_hr_payroll',
                                                                                      'view_delete_duplicate_payslip_form')[1]
        return {
                    'name': 'Delete Payslip',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'feosco.delete.duplicate.payslip',
                    'views': [(compose_form_id, 'form')],
                    'view_id': compose_form_id,
                    'target': 'new',
                    'context': context,
                }


confirm_allow_duplicate_payslip()

class delete_duplicate_payslip(osv.osv_memory):
    _name = "feosco.delete.duplicate.payslip"

    def delete_conflict_payslip(self, cr, uid, ids, context=None):
        payslip_ids = context.get('active_ids',False)
        payslip_pool = self.pool.get('hr.payslip')
        payslip_pool.unlink(cr, uid, payslip_ids, context=context)
        return {'type': 'ir.actions.act_window_close'}

delete_duplicate_payslip()
