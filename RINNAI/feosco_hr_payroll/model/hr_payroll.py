#-*- coding:utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from datetime import datetime
from datetime import timedelta

from openerp.osv import fields, osv
from openerp.tools.translate import _


class hr_payslip(osv.osv):
    _inherit = 'hr.payslip'
    '''
    Pay Slip
    '''

    def _check_unique_payslip_in_month(self, cr, uid, ids, context=None):
        res = {}
        for payslip in self.browse(cr, uid, ids, context=context):
            agrs_search = [('date_from', '=', payslip.date_from),
                           ('date_to', '=', payslip.date_to),
                           ('employee_id', '=', payslip.employee_id.id),
                           ('id', '!=', payslip.id),
                           ('credit_note', '=', payslip.credit_note),
                           ('state', '!=', 'cancel')]
            payslip_ids = self.search(cr, uid, agrs_search, context=context)
            if payslip_ids:
                res.update({payslip.id: True})
            else:
                res.update({payslip.id: False})

        return res

    def compute_sheet(self, cr, uid, ids, context=None):
        # Thach: Override this function to add confirm wizard when a payslip is conflict with other
        if context is None:
            context = {}
        check_conflict = context.get('check_conflict', True)  # Adding check_conflict = False to context to bypass check
        if check_conflict: #call wizard confirm
            conflict = self._check_unique_payslip_in_month(cr, uid, ids, context=context)
            conflict_ids = [key for key, value in conflict.iteritems() if value]
            if conflict_ids:  # conflicted
                active_model = context.get('active_model',False)
                if active_model: # Call from hr_payslip_employees wizard
                    # compute payslip that not conflicted
                    context.update({'check_conflict': False})
                    new_ids = [item for item in ids if item not in conflict_ids]
                    super(hr_payslip, self).compute_sheet(cr, uid, new_ids, context=context)
                    context.update({'check_conflict': True})
                    # Update key feosco_return_data to context,
                    # The function compute_sheet in hr_payslip_employees wizard will be return the wizard instead of {'type': 'ir.actions.act_window_close'}
                    context.update({'feosco_confirm_conflict': True})
                    context.update({'active_ids': conflict_ids})
                    context.update({'active_model': 'hr.payslip'})
                    return True
                else:
                    # Call wizard
                    compose_form_id = self.pool.get('ir.model.data').get_object_reference(cr, uid,'feosco_hr_payroll',
                                                                                          'view_confirm_allow_duplicate_payslip_form')[1]
                    return {
                        'name': 'Warning',
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'feosco.confirm.allow.duplicate.payslip',
                        'views': [(compose_form_id, 'form')],
                        'view_id': compose_form_id,
                        'target': 'new',
                        'context': context,
                    }

        return super(hr_payslip, self).compute_sheet(cr, uid, ids, context=context)

    def get_worked_day_lines(self, cr, uid, contract_ids, date_from, date_to, context=None):
        """
        @param contract_ids: list of contract id
        @return: returns a list of dict containing the input that should be applied for the given contract between date_from and date_to

        @Thach comment: Override this function to fix bug of standard
            Employee taking leave for half day but it is calculating full day leave.
            ex. If i taking a leave for 20-12-2013 9.00 to 13.00 (o.5 day), but the leave calculation is Full day leave.
        """
        # Call super
        res = super(hr_payslip, self).get_worked_day_lines(cr, uid, contract_ids, date_from, date_to, context=None)

        # Recompute
        def was_on_leave(employee_id, datetime_day, context=None):
            res = {}
            day = datetime_day.strftime("%Y-%m-%d")
            holiday_ids = self.pool.get('hr.holidays').search(cr, uid, [('state', '=', 'validate'),
                                                                        ('employee_id', '=', employee_id),
                                                                        ('type', '=', 'remove'),
                                                                        ('date_from', '<=', day),
                                                                        ('date_to', '>=', day)])
            if holiday_ids:
                # Thach: return dict of leave type and number of days instead of leave type
                # res = self.pool.get('hr.holidays').browse(cr, uid, holiday_ids, context=context)[0].holiday_status_id.name
                holiday_object = self.pool.get('hr.holidays').browse(cr, uid, holiday_ids, context=context)[0]
                res['leave_type'] = holiday_object.holiday_status_id.name
                res['number_of_days'] = float(holiday_object.number_of_days)
                res['date_to'] = datetime.strptime(holiday_object.date_to, "%Y-%m-%d %H:%M:%S")
            return res

        res = []
        for contract in self.pool.get('hr.contract').browse(cr, uid, contract_ids, context=context):
            if not contract.working_hours:
                #fill only if the contract as a working schedule linked
                continue
            attendances = {
                'name': _("Normal Working Days paid at 100%"),
                'sequence': 1,
                'code': 'WORK100',
                'number_of_days': 0.0,
                'number_of_hours': 0.0,
                'contract_id': contract.id,
            }
            leaves = {}
            day_from = datetime.strptime(date_from, "%Y-%m-%d")
            day_to = datetime.strptime(date_to, "%Y-%m-%d")
            nb_of_days = (day_to - day_from).days + 1
            # Thach Fix here: Please reference with standard code
            # Fix compute allocation in payroll
            day = 0
            while day < nb_of_days:
                #for day in range(0, nb_of_days):
                day += 1
                working_hours_on_day = self.pool.get('resource.calendar').working_hours_on_day(cr, uid,
                                                                                               contract.working_hours,
                                                                                               day_from + timedelta(
                                                                                                   days=day), context)
                if working_hours_on_day:
                    #the employee had to work
                    leave_type_data = was_on_leave(contract.employee_id.id, day_from + timedelta(days=day),
                                                   context=context)
                    leave_type = leave_type_data.get('leave_type', False)
                    number_of_days = leave_type_data.get('number_of_days', False)
                    date_to = leave_type_data.get('date_to', False)
                    if leave_type:
                        #if he was on leave, fill the leaves dict
                        if leave_type in leaves:
                            leaves[leave_type]['number_of_days'] += (
                                -number_of_days) if number_of_days < 0 else number_of_days
                            leaves[leave_type]['number_of_hours'] += working_hours_on_day
                        else:
                            leaves[leave_type] = {
                                'name': leave_type,
                                'sequence': 5,
                                'code': leave_type,
                                'number_of_days': (-number_of_days) if number_of_days < 0 else number_of_days,
                                'number_of_hours': working_hours_on_day,
                                'contract_id': contract.id,
                            }
                        day = date_to.day - 1
                    #add the input vals to tmp (increment if existing)
                    attendances['number_of_days'] += 1
                    attendances['number_of_hours'] += working_hours_on_day

        leaves = [value for key, value in leaves.items()]
        res += [attendances] + leaves

        return res


hr_payslip()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
