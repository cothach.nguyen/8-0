# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import fields, osv
from datetime import datetime

class hr_employee_month_worked(osv.osv):
    _inherit = 'hr.employee'
    _description = 'this is module inherit employee but query data from hr_contrat'
    
    def _get_month_work(self, cr, uid, ids, name, args, context=None):  
        result={}
        contract_pool = self.pool.get('hr.contract')
        employee_exit_pool=self.pool.get('feosco.hr.employee.exit')
        for id in ids:
            search_employee_exit=[('feosco_employee_id','=',id)]
            employee_exit_id=employee_exit_pool.search(cr, uid, search_employee_exit)
            search_contract=[('employee_id','=',id)]
            if employee_exit_id:
                employee_exit_obj= employee_exit_pool.read(cr, uid, employee_exit_id,['feosco_working_end_date'], context=None)
                maxdate=max(item['feosco_working_end_date'] for item in employee_exit_obj) 
                search_contract.append(('date_start','>=',maxdate))             
            contract_ids= contract_pool.search(cr, uid, search_contract)
            month_total=0
            if contract_ids:
                contract_obj = contract_pool.browse(cr, uid, contract_ids, context=context) 
                for contract in contract_obj:
                    start_time=datetime.strptime(contract.date_start, '%Y-%m-%d')
                    if contract.date_end:                                             
                        end_time=datetime.strptime(contract.date_end, '%Y-%m-%d')
                        if end_time>datetime.today():end_time=datetime.today()  
                    else: end_time= datetime.today()
                    worked_days=end_time-start_time
                    int_days=(worked_days.days)
                    month_total=month_total+(float(int_days))*12/365
            result[id]=month_total
        return result     
    
    _columns = {
        
        'feosco_month_total':fields.function(_get_month_work, string='Month total', type='float'),
    } 
hr_employee_month_worked()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
