# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import fields, osv


class hr_contract(osv.osv):
    _inherit = 'hr.contract'
    _description = 'Contract'

    def get_dependents_count(self, cr, uid, ids, field_name, field_value, arg, context=None):
        contract_obj = self.browse(cr, uid, ids)
        count = 0
        result = {}
        for contract in contract_obj:
            for dependency in contract.feosco_dependency_id:
                count = count + 1
            result[contract.id] = count
        return result

    def get_contrac_id(self, cr, uid, ids, context=None):
        result = {}
        for r in self.browse(cr, uid, ids):
            result[r.id] = r.id  # result={r.id:r.id}
        return result

    def _get_allowance_id(self, cr, uid, ids, context=None):
        # ids: id list of feosco.hr.payroll.allowance object
        result = []
        sql = "select contract_id from contract_allowance_rel where allowance_id"
        if len(ids) == 1:
            sql = sql + " = %s" % str(ids[0])
        else:
            sql = sql + "in %s" % tuple(ids)
        cr.execute(sql)
        records = cr.dictfetchall()
        for record in records:
            result.append(record['contract_id'])
        return list(result)

    def _compute_allowance(self, cr, uid, ids, field_name, field_value, arg, context=None):
        result = {}
        for contract in self.browse(cr, uid, ids, context=context):
            total_taxable = 0
            total_non_taxable = 0

            # Compute total allownace
            for allowance in contract.feosco_allowance_ids:
                if allowance.taxable:
                    total_taxable = total_taxable + (int(allowance.amount) if allowance.amount else 0)
                else:
                    total_non_taxable = total_non_taxable + (int(allowance.amount) if allowance.amount else 0)

            # Compute Gross Salary (Wage)
            gross_salary = contract.feosco_insurance_salary + total_taxable + total_non_taxable

            # Add data to return
            allowance_data = {
                'feosco_allowance_amount': total_taxable,
                'feosco_allowance_amount_nontaxable': total_non_taxable,
                'wage': gross_salary
            }
            result[contract.id] = allowance_data

        return result

    _columns = {
        'feosco_insurance_salary': fields.float('Basic Salary', help="Salary for joining Insurances"),
        'feosco_dependency': fields.function(get_dependents_count, string='Dependency', type='integer',
                                             store={'hr.contract': (get_contrac_id, ['feosco_dependency_id'], 10)}),
        'feosco_dependency_id': fields.one2many('feosco.hr.payroll.dependency', 'contract_id', 'Dependents Detail'),

        'feosco_allowance_ids': fields.many2many('feosco.hr.payroll.allowance', 'contract_allowance_rel', 'contract_id',
                                                 'allowance_id'),
        'feosco_allowance_amount': fields.function(_compute_allowance, string='Amount (Taxable)', type='float',
                                                   multi='sums',
                                                   store={
                                                       'feosco.hr.payroll.allowance': (
                                                           _get_allowance_id, ['amount', 'taxable'], 10),
                                                       'hr.contract': ( get_contrac_id, ['feosco_allowance_ids'], 10)
                                                   }),

        'feosco_allowance_amount_nontaxable': fields.function(_compute_allowance, string='Amount (Non Taxable)',
                                                              type='float',
                                                              multi='sums',
                                                              store={
                                                                  'feosco.hr.payroll.allowance': (
                                                                      _get_allowance_id, ['amount', 'taxable'], 10),
                                                                  'hr.contract': (
                                                                      get_contrac_id, ['feosco_allowance_ids'], 10)
                                                              }),

        # Override field
            # Wage change from float to function field
            # This field will be computed: wage = Basic salary (feosco_insurance_salary) + Allowance
        'wage': fields.function(_compute_allowance, string='Gross Salary',
                                                              type='float',
                                                              multi='sums',
                                                              store={
                                                                  'feosco.hr.payroll.allowance': (
                                                                      _get_allowance_id, ['amount'], 10),
                                                                  'hr.contract': (
                                                                      get_contrac_id, ['feosco_allowance_ids','feosco_insurance_salary'], 10)
                                                              },
                                                              help="Auto computed from Basic Salary and Allowances"),
    }


hr_contract()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
