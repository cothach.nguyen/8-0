# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import datetime

from openerp.osv import fields, osv

class hr_dependency(osv.osv):
    _name='feosco.hr.payroll.dependency'
    _description = 'this is object description dependents detail'
    
    def _check_date(self, cr, uid, ids, context=None):
        dependency_obj = self.browse(cr, uid, ids, context=context)
        for dependency in dependency_obj:
            if dependency.birthday and datetime.strptime(dependency.birthday,'%Y-%m-%d')>datetime.today():
                return False
        return True
    
    _columns = {
        'contract_id':fields.many2one('hr.contract',"Emplpyee's dependent:"),
        'name':  fields.char('Name',size=32),
        'birthday':fields.date('Birthday'),
        'identification':  fields.char('Identification No', size=32),
        'relations': fields.selection((('parents','Parents'),
                                       ('grandparents','Grandparents'),
                                       ('children','Children'),
                                       ('brother_sister','Brother or Sister')),
                                       'Relations'),
        'description': fields.text('Description',size=256),
    }
    
    _constraints = [
                    (_check_date,'input to birthday again,please !', ['birthday']),
    ]
    
hr_dependency()