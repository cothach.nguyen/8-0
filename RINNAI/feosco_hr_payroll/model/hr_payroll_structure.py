
from openerp.osv import fields, osv


class hr_payroll_structure(osv.osv):
    _inherit = 'hr.payroll.structure'

    _columns = {
        # Add this column to constraint Payroll structure and Contract type in contract
        'feosco_contract_type_id':fields.many2one('hr.contract.type', 'Contract Type',
                                           help="This payroll is applied for contract type"),
    }
