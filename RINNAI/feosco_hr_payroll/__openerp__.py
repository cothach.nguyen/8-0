# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name': 'FEOSCO Vietnam - Payroll',
    'version': '1.0',
    'category': 'Feosco',
    'description': """
Customize payroll for Vietnam


    * Contract


You can assign several contracts per employee.
    """,
    'author': 'Feosco',
    'website': 'http://www.feosco.com',
    'images': [],
    'depends': ['base','feosco_base','hr','hr_contract','hr_payroll','feosco_hr_contract'],
    'data': [
       'view/hr_contract_view.xml',
       'view/hr_payroll_structure_view.xml',
       'data/contract_data.xml',
       'data/l10n_vi_hr_payroll_data.xml',
       'data/production_payroll_data.xml',
       'data/service_payroll_data.xml',
       'view/hr_dependency_view.xml',
       'view/hr_employee_view.xml',
       'view/hr_payroll_allowance_view.xml',
       'view/hr_contract_type_view.xml',
       'view/hr_payroll_view.xml',
       'wizard/view/confirm_allow_dupplicate_payslip.xml',
       'report/hr_payroll_report.xml',
       'security/ir.model.access.csv',
       'security/hr_security.xml'
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
