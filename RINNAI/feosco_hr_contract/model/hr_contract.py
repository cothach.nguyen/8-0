# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import addons
import logging
from openerp.osv import fields, osv
from datetime import datetime



class hr_contract(osv.osv):
    _inherit = "hr.contract"
    __logger = logging.getLogger(_inherit)

    _columns = {
        'active': fields.boolean('Active'),
        'type_id': fields.many2one('hr.contract.type', "Contract Type"),
    }

    _defaults = {
        'name': '/',
        'active': True
    }

    def _get_sequence(self, cr, uid, context=None):

        obj_seq = self.pool.get('ir.sequence')
        args_search = [('code', '=', 'hr.contract')]
        sequence_ids = obj_seq.search(cr, uid, args_search, context=context)

        return obj_seq.next_by_id(cr, uid, sequence_ids[0], context=context)

    def create(self, cr, uid, vals, context=None):

        vals['name'] = self._get_sequence(cr, uid, context)

        contract_id = super(hr_contract, self).create(cr, uid, vals, context=context)
        copy_data_seen = context.get('__copy_data_seen', False)
        if contract_id and not copy_data_seen:
            contract_obj = self.browse(cr, uid, contract_id, context=context)

            work_history = self.pool.get('feosco.hr.employee.work.history')
            work_history.create_work_history_by_employee(cr, uid, [contract_obj.employee_id.id],
                                                         contract_obj.employee_id.department_id.id,
                                                         contract_obj.job_id.id,
                                                         context=context)

        return contract_id

    #validation date end of contract
    def check_date_Duration(self, cr, uid, ids, context=None):
        contract_obj = self.browse(cr, uid, ids)
        for contract in contract_obj:
            if contract.date_end and contract.date_start > contract.date_end:
                return False
        return True

    def _check_unique_contract(self, cr, uid, ids, context=None):

        for contract in self.browse(cr, uid, ids, context=context):

            if not contract.type_id.allow_duplicate:
                # Search các hợp đồng vô thời hạn
                # Co thoi han / vo thoi han vs vo thoi han
                agrs_search = [
                    ('date_end', '=', False),
                    ('employee_id', '=', contract.employee_id.id),
                    ('type_id', '=', contract.type_id.id),
                    ('id', 'not in', ids)
                ]
                contract_ids = self.search(cr, uid, agrs_search, context=context)

                if not contract_ids:  # Nếu không có hợp đồng vô thời hạn, search hợp đồng có thời hạn
                    # Co thoi han vs co thoi han
                    if contract.date_end:
                        agrs_search = [('date_start', '<=', contract.date_end),
                                       ('date_end', '>=', contract.date_start),
                                       ('employee_id', '=', contract.employee_id.id),
                                       ('type_id', '=', contract.type_id.id),
                                       ('id', 'not in', ids)]
                    # vo thoi han vs co thoi han
                    else:
                        agrs_search = [('date_end', '>=', contract.date_start),
                                       ('employee_id', '=', contract.employee_id.id),
                                       ('type_id', '=', contract.type_id.id),
                                       ('id', 'not in', ids)]

                    contract_ids = self.search(cr, uid, agrs_search, context=context)

                if contract_ids: return False

        return True

    _constraints = [
        (check_date_Duration, 'input date end again,please !', ['date_end']),
        (_check_unique_contract, "\nContract of this employee has been exited'.",
         ['employee_id', 'date_from', 'date_to'])
    ]

    def onchange_employee_id(self, cr, uid, ids, employee_id, context=None):
        result = {'value': {}}
        employee_obj = self.pool.get('hr.employee').browse(cr, uid, employee_id, context=context)

        if employee_obj and employee_obj.job_id:
            result['value'].update({'job_id': employee_obj.job_id.id})
        else:
            result['value'].update({'job_id': False})
        return result

    def feosco_auto_inactive_contract(self, cr, uid, automatic=False, use_new_cursor=False, context=None):
        self.__logger.info('START: feosco_auto_inactive_contract')

        try:
            if context is None:
                context = {}

            contract_pool = self.pool.get('hr.contract')

            agrs = [
                ('date_end', '<', datetime.now().strftime('%Y-%m-%d')),
                ('active', '=', True)
            ]

            contract_ids = contract_pool.search(cr, uid, agrs, context=context)

            if contract_ids:
                contract_pool.write(cr, uid, contract_ids, {'active': False}, context=context)
                self.__logger.info('Contracts are set in-active %s' % (str(contract_ids)))

        except Exception as exc:
            self.__logger.info('Can NOT set inactive Contract because: %s' % (exc))

        self.__logger.info('END: feosco_auto_inactive_contract')
        return True


hr_contract()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
