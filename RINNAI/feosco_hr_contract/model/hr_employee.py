# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import fields, osv

class hr_employee(osv.osv):
    _inherit = "hr.employee"
    
    def _get_children_number(self, cr, uid, ids, context=None):
        employee_obj=self.browse(cr, uid, ids)
        for employee in employee_obj:
            if employee.children and int(employee.children)<0:
                return False
        return True
    _columns = {
                # Overrided
                'place_of_birth':fields.many2one('feosco.city',string='Place of Birth'),
    }
    _constraints = [
                    (_get_children_number,'input number of children again,please !', ['children']),
    ]
hr_employee()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
