# -*- coding: utf-8 -*-
{
    'name': 'Quản lý tháng khuyến mãi',
    'version': '0.1',
    'author': 'FEOSCO',
    'category': 'Feosco - Tháng khuyến mãi',
    'website': 'http://www.feosco.com',
    'description' : '''
        Module cho phép quản lý đăng ký / thông báo chương trình khuyến mãi của doanh nghiệp:
            Cho phép doanh nghiệp đăng ký online
            Chuyên viên duyệt chương trình trực tiếp trên hệ thống
            Hệ thống sẽ tự thông báo cho doanh nghiệp.
    ''',
    'images': [],
    'depends': ['thangkhuyenmai_base'],
    'data': [
        'security/ir.model.access.csv',
       'wizard/view/assign.xml',
       'view/hoso_view.xml',
       'view/sanpham_view.xml',
       'view/hinhthuckhuyenmai_view.xml',
       'view/hoso_duyet_view.xml',
       'view/menu.xml',
       'data/master_data.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
