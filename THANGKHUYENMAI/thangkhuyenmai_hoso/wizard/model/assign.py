# -*- coding: utf-8 -*-
from openerp.osv import fields, osv

class promotion_proile_assign(osv.osv_memory):

    _name = 'promotion.profile.assign'
    _description = 'Promotion Profile Assign'

    _columns = {
                'user_id': fields.many2one('res.users', 'User'),
    }
    _defaults = {
       
    }

    def assign_promotion_profile(self, cr, uid, ids, context=None):
        ticket_pool = self.pool.get('feosco.promotion.profile')
        ticket_ids = context.get('active_ids', None)
        
        for this in self.browse(cr, uid, ids, context=context):
            if ticket_ids:
                write_agrs = {'user_id': this.user_id.id if this.user_id else None}
                res = ticket_pool.write(cr, uid, ticket_ids, write_agrs, context=context)


        return {
            'type': 'ir.actions.act_window_close'
        }


