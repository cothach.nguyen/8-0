#-*- coding:utf-8 -*-

from openerp.osv import osv, fields

class feosco_promotion_product(osv.Model):

    _name = "feosco.promotion.product"

    _columns = {
        'name': fields.char(u'Tên sản phẩm'),
        'image': fields.binary(u'Hình',
            help="This field holds the image used as image for the product, limited to 1024x1024px."),
        'quantiy': fields.float(u'Số lượng'),
        'type': fields.selection([
                ('product', 'Sản phẩm'),
                ('gift','Quà tặng')], u'Loại sản phẩm'),
        'from_date': fields.datetime('Từ ngày'),
        'to_date': fields.datetime('Đến ngày'),
        'state': fields.selection([('new', u'Chưa duyệt'),
                                   ('cancel', u'Đã hủy'),
                                   ('approved','Đã duyệt')], u'Trạng thái'),
        'profile_id': fields.many2one('feosco.promotion.profile', u'Thuộc chương trình'),
        'company_id': fields.related('profile_id', 'company_id', type='many2one', relation='res.partner', string=u'Doanh nghiệp', readonly=True),
        'total_value': fields.float(u'Tổng giá trị'),
    }
    
    _defaults = {
                'state': 'new'
                }
    
    def action_by_profile_id(self, cr, uid, profile_ids, state='new', context=None):
        if profile_ids:
            product_ids = self.search(cr, uid, [('profile_id','in',profile_ids)], context=context)
            if product_ids:
                return self.write(cr, uid, product_ids, {'state': state},context=context)
        return False
    
    def approve(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'approved'}, context=context)
    
    def set_to_new(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'new'}, context=context)
    
    def cancel(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'cancel'}, context=context)