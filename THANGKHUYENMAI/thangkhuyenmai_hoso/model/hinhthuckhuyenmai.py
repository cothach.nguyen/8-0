#-*- coding:utf-8 -*-

from openerp.osv import osv, fields

class feosco_promotion_method(osv.Model):

    _name = "feosco.promotion.method"

    _columns = {
        'code': fields.char(u'Mã'),
        'name': fields.char(u'Hình thức khuyến mãi'),
    }
    
    