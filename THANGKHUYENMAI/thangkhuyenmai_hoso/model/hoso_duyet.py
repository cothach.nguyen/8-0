# -*- coding:utf-8 -*-

__author__ = 'thachnguyen'

from openerp.osv import osv, fields

class feosco_promotion_profile_approved_list(osv.Model):
    _name = "feosco.promotion.profile.approved.list"

    _columns = {
        'code': fields.char(u'Mã nội dung'),
        'name': fields.char(u'Nội dung duyệt'),
        'profile_id': fields.many2one('feosco.promotion.profile', u'Hồ sơ'),
        'state': fields.selection([('new', u'Chưa duyệt'),
                                   ('done', u'Đã duyệt')
                                  ], u'Trạng thái'),
        'type': fields.selection([('master_data', u'Dữ liệu mẫu'),
                                   ('normal_data', u'Dữ liệu')
                                  ], u'Loại dữ liệu'),
    }

    _defaults = {
        'state': 'new'
    }