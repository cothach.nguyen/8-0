# -*- coding:utf-8 -*-

from openerp.osv import osv, fields


class feosco_promotion_profile(osv.Model):
    _name = "feosco.promotion.profile"

    _columns = {
        'code': fields.char(u'Mã chương trình'),
        'name': fields.char(u'Tên chương trình'),
        'district_id': fields.many2many('feosco.district', 'hoso_district_rel', 'hoso_id', 'district_id',
                                        u'Quân huyện áp dụng khuyến mãi'),
        'apply_for_all_district': fields.boolean(u'Tp Hồ Chí Minh', ),
        'promotion_method_id': fields.many2many('feosco.promotion.method', 'hoso_hinhthuckhuyenmai_rel', 'hoso_id',
                                                 'hinhthuckhuyenmai_id', u'Hình thức khuyến mãi'),
        'other_description': fields.text(u'Mô tả hình thức khác'),
        'target_customer': fields.text(u'Đối tượng khách hàng'),
        'product_ids': fields.one2many('feosco.promotion.product', 'profile_id', u'Sản phẩm'),
        'all_product': fields.boolean(u'Tất cả sản phẩm', ),
        'company_id': fields.many2one('res.partner', u'Doanh nghiệp'),
        'company_tax_code': fields.related('company_id', 'feosco_account_num', type='char', string=u"Mã số thuế", readonly=True),
        'from_date': fields.datetime('Từ ngày'),
        'to_date': fields.datetime('Đến ngày'),
        'type': fields.selection([
                                     ('register', 'Đăng ký khuyến mãi'),
                                     ('notify', 'Thông báo khuyến mãi')], u'Loại hồ sơ'),
        'state': fields.selection([('new', u'Chưa duyệt'),
                                   ('added', u'Đã bổ sung hồ sơ'),
                                   ('cancel', u'Đã hủy'),
                                   ('additional', u'Đang bổ sung hồ sơ'),
                                   ('approved', u'Chuyên viên đã duyệt'),
                                   ('done', u'Đã duyệt'),

                                  ], u'Trạng thái'),

        'user_id': fields.many2one('res.users', u'Chuyên viên phụ trách', required=False),
        'create_date': fields.datetime('Create Date', readonly=True),
        'approved_date': fields.datetime('Ngày chuyên viên duyệt', readonly=True),
        'dept_head': fields.many2one('res.users', u'Trưởng phòng duyệt', required=False),
        'dept_approve_date': fields.datetime(u'Ngày trưởng phòng duyệt', readonly=True),
        'director': fields.many2one('res.users', u'Giám đốc duyệt', required=False),
        'director_approve_date': fields.datetime(u'Ngày giám đốc duyệt', readonly=True),
        'content': fields.text(u'Nội dung chương trình'),
        'approved_list_ids': fields.one2many('feosco.promotion.profile.approved.list', 'profile_id', u'Danh sách duyệt'),

        # Approved check field
        'name_approved': fields.boolean(u'Duyệt tên chương trình'),
        'district_approved': fields.boolean(u'Duyệt địa bàn khuyến mãi'),
        'date_approved': fields.boolean(u'Duyệt thời gian khuyến mãi'),
        'method_approved': fields.boolean(u'Duyệt hình thức khuyến mãi'),
        'target_customer_approved': fields.boolean(u'Duyệt đối tượng khách hàng'),
        'product_approved': fields.boolean(u'Duyệt sản phẩm'),
        'content_approved': fields.boolean(u'Duyệt nội dung chương trình'),
    }

    _defaults = {
        'state': 'new'
    }

    def chuyen_vien_duyet(self, cr, uid, ids, context=None):
        # Duyet tat ca sp cua chuong trinh
        product_pool = self.pool.get('feosco.promotion.product')
        product_pool.action_by_profile_id(cr, uid, ids, 'approved', context=context)
        write_data = {
            'name_approved': True,
            'district_approved': True,
            'date_approved': True,
            'method_approved': True,
            'target_customer_approved': True,
            'product_approved': True,
            'content_approved': True,
            'state': 'approved',
        }
        return self.write(cr, uid, ids, write_data, context=context)

    def truong_phong_duyet(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'done', 'dept_head': uid}, context=context)

    def pho_giam_doc_duyet(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'done', 'director': uid}, context=context)

    def cancel(self, cr, uid, ids, context=None):
        # Huy tat ca sp cua chuong trinh
        product_pool = self.pool.get('feosco.promotion.product')
        product_pool.action_by_profile_id(cr, uid, ids, 'cancel', context=context)

        write_data = {
            'name_approved': False,
            'district_approved': False,
            'date_approved': False,
            'method_approved': False,
            'target_customer_approved': False,
            'product_approved': False,
            'content_approved': False,
            'state': 'cancel',
        }
        return self.write(cr, uid, ids, write_data, context=context)

    def bo_sung_ho_so(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'additional'}, context=context)

    def da_bo_sung_ho_so(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'added'}, context=context)

    def re_open(self, cr, uid, ids, context=None):
        # Mo lai tat ca sp cua chuong trinh
        product_pool = self.pool.get('feosco.promotion.product')
        product_pool.action_by_profile_id(cr, uid, ids, 'new', context=context)

        return self.write(cr, uid, ids, {'state': 'new'}, context=context)

    def duyet_truong(self, cr, uid, ids, context=None):
        feosco_approve_fields = context.get('feosco_approve_fields',False)
        if feosco_approve_fields == 'name':
            return self.write(cr, uid, ids, {'name_approved': True}, context=context)
        elif feosco_approve_fields == 'content':
            return self.write(cr, uid, ids, {'content_approved': True}, context=context)
        elif feosco_approve_fields == 'date':
            return self.write(cr, uid, ids, {'date_approved': True}, context=context)
        elif feosco_approve_fields == 'target_customer':
            return self.write(cr, uid, ids, {'target_customer_approved': True}, context=context)
        elif feosco_approve_fields == 'district':
            return self.write(cr, uid, ids, {'district_approved': True}, context=context)
        elif feosco_approve_fields == 'method':
            return self.write(cr, uid, ids, {'method_approved': True}, context=context)
        elif feosco_approve_fields == 'product':
             # Duyet tat ca sp cua chuong trinh
            product_pool = self.pool.get('feosco.promotion.product')
            product_pool.action_by_profile_id(cr, uid, ids, 'approved', context=context)
            return self.write(cr, uid, ids, {'product_approved': True}, context=context)
        else:
            return False

    def create(self, cr, uid, vals, context=None):

        profile_id = super(feosco_promotion_profile, self).create(cr, uid, vals, context=context)
        # Create approved list
        self.create_approved_list(cr, uid, [profile_id], context=context)

        return profile_id

    def create_approved_list(self, cr, uid, profile_ids, context=None):
        # Create approved list for this profile
        approved_list_pool = self.pool.get('feosco.promotion.profile.approved.list')
        approved_list_ids = approved_list_pool.search(cr, uid, [('type','=','master_data')], context=context)
        if approved_list_ids:
            for profile_id in profile_ids:
                new_approved_list_ids = []
                tmp_default = {
                    'state': 'new',
                    'type': 'normal_data'
                }
                for approved_list_id in approved_list_ids:
                    new_approved_list_id = approved_list_pool.copy(cr, uid, approved_list_id, default=tmp_default, context=context)
                    new_approved_list_ids.append(new_approved_list_id)
                if new_approved_list_ids:
                    approved_list_pool.write(cr, uid, new_approved_list_ids, {'profile_id': profile_id}, context=context)
            return True

        return False