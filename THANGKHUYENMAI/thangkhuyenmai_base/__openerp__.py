# -*- coding: utf-8 -*-
{
    'name': 'FEOSCO Digital CORE Module',
    'version': '0.1',
    'author': 'FEOSCO',
    'category': 'FEOSCO',
    'description': 'Thang Khuyen Mai base module',
    'website': 'http://www.feosco.com',
    'images': [],
    'depends': [
        'feosco_base',
        'feosco_web_adblock'
    ],
    'data': [
            'security/res_groups.xml'
    ],
    'installable': True,
    'auto_install': False,
}
