openerp.pos_restaurant_tables = function(instance){

    var module = instance.point_of_sale;
    pos_restaurant_tables_screens(instance,module);
    pos_restaurant_tables_widgets(instance,module);

};