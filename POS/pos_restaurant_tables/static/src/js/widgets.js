function pos_restaurant_tables_widgets(instance, module) {

    var qweb = instance.web.qweb;

    module.PosWidget = module.PosWidget.extend({

		start: function() {

            var self = this;
            var parentReturn = this._super();

            return parentReturn;
        },

        showAllTableObj: function() {

            this.tableObj_screen = new module.TableObjScreenWidget(this, {});
            this.tableObj_screen.appendTo($('.screens'));
            this.pos_widget.screen_selector.screen_set['tableObj'] = this.tableObj_screen
            this.pos_widget.screen_selector.set_current_screen('tableObj');

        },

        renderElement: function() {
            var self = this;
            this._super()
            var element = self.$('.showAllTableObj')
            element.click(function(){
                self.showAllTableObj();
            });


        }
 	});

    module.TableObjListWidget = module.PosBaseWidget.extend({

        template:'TableObjListWidget',

        init: function(parent, options) {
            var self = this;
            this._super(parent,options);
            this.model = options.model;
            this.tableObj_cache = {};
            this.tableObj_list = options.tableObj_list || []


        },

        replace: function($target){
            this.renderElement();
            var target = $target[0];
            if (target) {
                target.parentNode.replaceChild(this.el,target);
            }

        },

        get_image_url: function(tableObj){
            return window.location.origin + '/web/binary/image?model=pos.tables&field=image&id='+ tableObj.table.id;
        },


        render_tableObj: function(tableObj){

            if(!this.tableObj_cache[tableObj.sequence_number]){
                var tableObj_html = qweb.render('TableObj',{
                        widget:  this,
                        tableObj: tableObj
                    }),
                    tableObj_note = document.createElement('div');

                tableObj_note.innerHTML = tableObj_html;
                tableObj_note = tableObj_note.childNodes[1];
                this.tableObj_cache[tableObj.sequence_number] = tableObj_note;
            }
            var tableObj_cache = this.tableObj_cache[tableObj.sequence_number];
            return tableObj_cache
        },

        renderElement: function() {

            var self = this,
            list_container,
            i, len, line_node,
            el_str  = qweb.render(this.template, {widget: this}),
            el_node = document.createElement('div');
            el_node.innerHTML = el_str;
            el_node = el_node.childNodes[1];
            if(this.el && this.el.parentNode){
                this.el.parentNode.replaceChild(el_node,this.el);
            }
            this.el = el_node;
            list_container = el_node.querySelector('.tableobj-list');
            for(i = 0, len = this.tableObj_list.length; i < len; i++){
                tableObj_cache = this.render_tableObj(this.tableObj_list[i]);
                list_container.appendChild(tableObj_cache);
            }
        }
    });




}