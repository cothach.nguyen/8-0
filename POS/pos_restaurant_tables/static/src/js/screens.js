function pos_restaurant_tables_screens(instance, module) {

    var QWeb = instance.web.qweb;
	var _t = instance.web._t;

    module.TableObjScreenWidget = module.ScreenWidget.extend({
        template:     'TableObjScreenWidget',
        show_leftpane:   false,

        show: function(){
            this._super();
            var self = this;
            this.add_action_button({
                label: _t('Back'),
                icon: '/point_of_sale/static/src/img/icons/png48/go-previous.png',
                click: function(){
                    self.back();
                }
            });

        },

        back: function () {
            this.pos_widget.screen_selector.set_current_screen('products');
        },


        start: function(){

            var self = this;
            this.TableObjListWidget = new module.TableObjListWidget(this,{
                tableObj_list: this.get_all_currentOrders()
            });
            this.TableObjListWidget.replace($('.placeholder-TableObjListWidget'));
        },

        get_all_currentOrders: function() {
            var all_orders = []
            var orders = this.pos.get('orders').models;
            _.each(orders, function(order) {
                all_orders.push(order)
            })

            return all_orders
        }

    });
}