# -*- coding: utf-8 -*-
{
    'name': 'Point of Sale: Show all Table current Order',
    'version': '1.0',
    'category': 'Point of Sale',
    'sequence': 106,
    'summary': 'Point of Sale: Show all Table current Order',
    'description': """
    Module Show all table have Order \n
    Author: trungthanh.nguyen@feosco.com \n
    Website: www.feosco.com \n
    Skype: thanhchatvn \n
    Mobile: (+84) 902 403 918 \n
    """,
    'author': 'FEOSCO',
    'depends': ['pos_restaurant_cafeshop'],
    'website': 'www.feosco.com',
    'data': [
        'view/template.xml',
    ],
    'qweb':[
        'static/src/xml/tables.xml'
    ],
    'installable': True,
    'auto_install': False,
}
