function pos_bill_widget(instance, module) {
    var qweb = instance.web.qweb;
    var _t = instance.web._t;

    module.LineListWidget = module.PosBaseWidget.extend({
        template:'LineListWidget',

        init: function(parent, options) {
            var self = this;
            this._super(parent,options);
            this.model = options.model;
            this.next_screen = options.next_screen || false;
            this.line_cache = {};
            this.line_list = options.line_list

        },

        replace: function($target){
            this.renderElement();
            var target = $target[0];
            if (target) {
                target.parentNode.replaceChild(this.el,target);
            }

        },


        render_line: function(line){

            if(!this.line_cache[line.id]){
                var line_html = qweb.render('Line',{
                        widget:  this,
                        line: line
                    }),
                    line_note = document.createElement('div');

                line_note.innerHTML = line_html;
                line_note = line_note.childNodes[1];
                this.line_cache[line.id] = line_note;
            }
            var line_cache = this.line_cache[line.id];
            return line_cache
        },

        renderElement: function() {
            var self = this,
            list_container,
            i, len, line_node,
            el_str  = qweb.render(this.template, {widget: this}),
            el_node = document.createElement('div');
            el_node.innerHTML = el_str;
            el_node = el_node.childNodes[1];
            if(this.el && this.el.parentNode){
                this.el.parentNode.replaceChild(el_node,this.el);
            }
            this.el = el_node;
            list_container = el_node.querySelector('.line-list');
            for(i = 0, len = this.line_list.length; i < len; i++){
                line_node = this.render_line(this.line_list[i]);
                list_container.appendChild(line_node);
            }
        }
    });

    module.OrderWidget = module.OrderWidget.extend({

        next_print: function() {
            var self = this;
            this.line_screen = new module.LineScreenWidget(this, {});
            this.line_screen.appendTo($('.screens'));
            this.pos_widget.screen_selector.screen_set['lines'] = this.line_screen
            this.pos_widget.screen_selector.set_current_screen('lines');

        },

        renderElement: function(scrollbottom){

            var self = this;
            this._super(scrollbottom);
            $('.printKitchen').click(function() {
                self.next_print();

            })

        }
    });

    module.PosWidget = module.PosWidget.extend({
		build_widgets: function() {
            var self = this;
			this._super();
		}
	});


}