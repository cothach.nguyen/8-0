function pos_bill_screens(instance, module) {
    var qweb = instance.web.qweb;
    var _t = instance.web._t;

    module.LineScreenWidget = module.ScreenWidget.extend({
        template:     'LineScreenWidget',

        next_screen:  'lines',
        back_screen: 'tables',

        show: function(){
            this._super();
            var self = this;

            this.add_action_button({
                    label: _t('Print'),
                    icon: '/point_of_sale/static/src/img/icons/png48/printer.png',
                    click: function(){ self.print(); }
                });
            this.add_action_button({
                    label: _t('Back'),
                    icon: '/point_of_sale/static/src/img/icons/png48/go-previous.png',
                    click: function(){
                        self.back();
                    }
                });

            this.add_action_button({
                    label: _t('Done Bill'),
                    icon: '/pos_restaurant_printsreen/static/src/img/donebill.png',
                    click: function(){
                        self.done();
                    }
                });



        },

        back: function () {
            this.pos_widget.screen_selector.set_current_screen('products');
        },

        done: function () {
            alert('RD ...to be continue');
        },

        print: function() {
            window.print();
        },

        close: function(){
            this._super();
        },

        start: function(){

            var self = this;
            this.line_list_widget = new module.LineListWidget(this,{
                line_list: this.get_all_kitchen()
            });
            this.line_list_widget.replace($('.placeholder-LineListWidget'));
        },

        get_all_kitchen: function(table) {
            var kitchenAllOrder = []
            var orders = this.pos.get('orders').models;
            _.each(orders, function(order) {
                var table = order.table.name
                _.each(order.sendKitchen, function(sendToPrint) {
                    sendToPrint['table_name'] = table;
                    kitchenAllOrder.push(sendToPrint)
                })
            })
            return kitchenAllOrder
        }

    });
}