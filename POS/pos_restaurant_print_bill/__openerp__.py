# -*- coding: utf-8 -*-
{
    'name': 'Point of Sale Printing Screen to Kitchen',
    'version': '1.0',
    'category': 'Point of Sale',
    'sequence': 105,
    'summary': 'Point of Sale for Restaurant & Cafe-Shop',
    'description': """
    Module Print Screen all order to Kitchen \n

    Author: trungthanh.nguyen@feosco.com \n
    Website: www.feosco.com \n
    Skype: thanhchatvn \n
    Mobile: (+84) 902 403 918 \n
    """,
    'author': 'FEOSCO',
    'depends': ['pos_restaurant_cafeshop'],
    'website': 'www.feosco.com',
    'data': [
        'view/templates.xml',
    ],
    'qweb':[
        'static/src/xml/printscreen.xml',
    ],
    'installable': True,
    'auto_install': False,
}
