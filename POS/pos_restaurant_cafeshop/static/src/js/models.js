function models(instance, module) {

    var QWeb = instance.web.qweb;
	var _t = instance.web._t;

    var ModuleOrderSuper = module.Order;
    module.Order = module.Order.extend({


        initialize: function(attributes){
            Backbone.Model.prototype.initialize.apply(this, arguments);
            this.pos = attributes.pos;
            this.sequence_number = this.pos.pos_session.sequence_number++;
            this.uid =     this.generateUniqueId();
            this.set({
                creationDate:   new Date(),
                orderLines:     new module.OrderlineCollection(),
                paymentLines:   new module.PaymentlineCollection(),
                name:           _t("Order ") + this.uid,
                client:         null,
            });
            this.selected_orderline   = undefined;
            this.selected_paymentline = undefined;
            this.screen_data = {};  // see ScreenSelector
            this.receipt_type = 'receipt';  // 'receipt' || 'invoice'
            this.temporary = attributes.temporary || false;
            this.sendKitchen = [];
            this.changeTable = false;
            this.table = false;
            return this;
        },

        render_button_restaurant: function() {
            listButtonRender = ['.printKitchen', '.kitchen', '.clear', '.change', '.discountAll', '.showAllTableObj']
            this.pos.db.showElement(listButtonRender)

        },

        addProduct: function(product, options) {
            options = options || {};
            var attr = JSON.parse(JSON.stringify(product));
            attr.pos = this.pos;
            attr.order = this;
            var line = new module.Orderline({}, {pos: this.pos, order: this, product: product});

            if (options.quantity !== undefined) {
                line.set_quantity(options.quantity);
            }
            if (options.price !== undefined) {
                line.set_unit_price(options.price);
            }
            if (options.discount !== undefined) {
                line.set_discount(options.discount);
            }

            var last_orderline = this.getLastOrderline();
            if (last_orderline && last_orderline.can_be_merged_with(line) && options.merge !== false) {
                last_orderline.merge(line);
            } else {
                this.get('orderLines').add(line);
            }
            var table = this.get('table');


            this.selectLine(this.getLastOrderline());
            var sendKitchen = this.sendKitchen;
            if (sendKitchen.length >= 1) {
                var tem = false;
                _.each(this.sendKitchen, function(productOld) {
                    if (product.product_tmpl_id == productOld.product_tmpl_id) {
                        tem = true;
                        productOld['table_id'] = table.id
                        productOld['qty'] += 1
                    }
                })
                if (tem == false) {
                    product['table_id'] = table.id
                    product['qty'] = 1;
                    sendKitchen.push(product);
                }
            }
            else {
                product['table_id'] = table.id
                product['qty'] = 1;
                sendKitchen.push(product);
            }
            this.render_button_restaurant();
        },




        export_for_printing: function(){
            var dict = this._super(),
                table = this.get('table');
            if (typeof table !== 'undefined') {dict.table = table.name;}
            return dict;
        },

        export_as_JSON: function() {
            var res = ModuleOrderSuper.prototype.export_as_JSON.call(this);
            table = this.get('table');
            if (table) {
                res.table_id = table.id
            }
            return res;
        },

        setTable: function(table){
            this.set({'table': table});
        },

        removeTable: function(table){
            var self = this;
            var elements = $('#' + table.id);
            console.log('elements:');
            console.log(elements);
            elements.removeClass('oe_hidden');
        },

        get_table: function(){
            return this.get('table');
        },

        get_image: function() {
            var table = this.get('table');
            if (table) {
                return window.location.origin + '/web/binary/image?model=pos.tables&field=image&id=' + this.get('table').id;
            }
            else {
                return ''
            }

        },

        get_table_name: function(){
            var table = this.get('table');
            return table ? table.name : "";
        }
    });

    module.PosModel = module.PosModel.extend({

        // Extend the create_order function for add table information

        delete_current_order: function(){
            console.log('===> Extend PosModel delete_current_order()');

            /**
                selectedOrder : là 1 đối tượng object của module.Order
                khi khai báo 1 đối tượng ta thừa hưởng tất cả: thuộc tính cũng như phương thức của nó.
                vd goi function get_table : order.get_table()
             */

            var order = this.get('selectedOrder');
            var table = order.get_table();
            this.get('selectedOrder').destroy({'reason':'abandon'}); //delete this object use function destroy
//            if (table) {
//                order.removeTable(table);
//            }
        },


        push_order: function(order) {
            console.log('---> PosModel push_order()');
            var self = this;
            console.log(order);
            if(order){
                this.proxy.log('push_order',order.export_as_JSON());
                this.db.add_order(order.export_as_JSON());
            }

            var pushed = new $.Deferred();

            this.flush_mutex.exec(function(){
                var flushed = self._flush_orders(self.db.get_orders());

                flushed.always(function(ids){
                    pushed.resolve();
                });
            });
            var selectOrder = this.get('selectedOrder');
            var table = selectOrder.get_table();
//            console.log(table);
//            if (table) {
//                order.removeTable(table);
//            }
            return pushed;
        },



        create_order: function(order){
            var self = this,
                model = null;
            this._super(order);
            model = self.get('selectedOrder');
            self.fetch('pos.order', ['table_id'], [['id', '=', model.get('order_id')]])
                .then(function(table_data) {
                    var table = self.db.get_table_by_id(table_data[0].table_id[0]);
                    model.setTable(table);
                    //Set current order screen to products
                    model.set_screen_data('cashier_screen','products');
                });
        },
        /*
        ==============================================
        loading data from backend and store to cache
        ==============================================
         */
        models: [
        {
            model:  'res.users',
            fields: ['name','company_id'],
            domain: function(self){ return [['id','=',self.session.uid]]; },
            loaded: function(self,users){ self.user = users[0]; },
        },{
            model:  'res.company',
            fields: [ 'currency_id', 'email', 'website', 'company_registry', 'vat', 'name', 'phone', 'partner_id' , 'country_id'],
            domain: function(self){ return [['id','=',self.user.company_id[0]]]; },
            loaded: function(self,companies){ self.company = companies[0]; },
        },{
            model:  'product.uom',
            fields: [],
            domain: null,
            loaded: function(self,units){
                self.units = units;
                var units_by_id = {};
                for(var i = 0, len = units.length; i < len; i++){
                    units_by_id[units[i].id] = units[i];
                    units[i].groupable = ( units[i].category_id[0] === 1 );
                    units[i].is_unit   = ( units[i].id === 1 );
                }
                self.units_by_id = units_by_id;
            }
        },{
            model:  'res.users',
            fields: ['name','ean13'],
            domain: null,
            loaded: function(self,users){ self.users = users; },
        },{
            model:  'res.partner',
            fields: ['name','street','city','state_id','country_id','vat','phone','zip','mobile','email','ean13','write_date'],
            domain: null,
            loaded: function(self,partners){
                self.partners = partners;
                self.db.add_partners(partners);
            }
        },{
            model:  'res.country',
            fields: ['name'],
            loaded: function(self,countries){
                self.countries = countries;
                self.company.country = null;
                for (var i = 0; i < countries.length; i++) {
                    if (countries[i].id === self.company.country_id[0]){
                        self.company.country = countries[i];
                    }
                }
            }
        },{
            model:  'account.tax',
            fields: ['name','amount', 'price_include', 'type'],
            domain: null,
            loaded: function(self,taxes){ self.taxes = taxes; },
        },{
            model:  'pos.session',
            fields: ['id', 'journal_ids','name','user_id','config_id','start_at','stop_at','sequence_number','login_number'],
            domain: function(self){ return [['state','=','opened'],['user_id','=',self.session.uid]]; },
            loaded: function(self,pos_sessions){
                self.pos_session = pos_sessions[0];

                var orders = self.db.get_orders();
                for (var i = 0; i < orders.length; i++) {
                    self.pos_session.sequence_number = Math.max(self.pos_session.sequence_number, orders[i].data.sequence_number+1);
                }
            }
        },{
            model: 'pos.config',
            fields: [],
            domain: function(self){ return [['id','=', self.pos_session.config_id[0]]]; },
            loaded: function(self,configs){
                self.config = configs[0];
                self.config.use_proxy = self.config.iface_payment_terminal ||
                                        self.config.iface_electronic_scale ||
                                        self.config.iface_print_via_proxy  ||
                                        self.config.iface_scan_via_proxy   ||
                                        self.config.iface_cashdrawer;

                self.barcode_reader.add_barcode_patterns({
                    'product':  self.config.barcode_product,
                    'cashier':  self.config.barcode_cashier,
                    'client':   self.config.barcode_customer,
                    'weight':   self.config.barcode_weight,
                    'discount': self.config.barcode_discount,
                    'price':    self.config.barcode_price,
                });

                if (self.config.company_id[0] !== self.user.company_id[0]) {
                    throw new Error(_t("Error: The Point of Sale User must belong to the same company as the Point of Sale. You are probably trying to load the point of sale as an administrator in a multi-company setup, with the administrator account set to the wrong company."));
                }
            }
        },{
            model: 'stock.location',
            fields: [],
            domain: function(self){ return [['id','=', self.config.stock_location_id[0]]]; },
            loaded: function(self, locations){ self.shop = locations[0]; },
        },{
            model:  'product.pricelist',
            fields: ['currency_id'],
            domain: function(self){ return [['id','=',self.config.pricelist_id[0]]]; },
            loaded: function(self, pricelists){ self.pricelist = pricelists[0]; },
        },{
            model: 'res.currency',
            fields: ['symbol','position','rounding','accuracy'],
            domain: function(self){ return [['id','=',self.pricelist.currency_id[0]]]; },
            loaded: function(self, currencies){
                self.currency = currencies[0];
            }
        },{
            model: 'product.packaging',
            fields: ['ean','product_tmpl_id'],
            domain: null,
            loaded: function(self, packagings){
                self.db.add_packagings(packagings);
            }
        },{
            model:  'pos.category',
            fields: ['id','name','parent_id','child_id','image'],
            domain: null,
            loaded: function(self, categories){
                self.db.add_categories(categories);
            }
        },{
            model:  'product.product',
            fields: ['display_name', 'list_price','price','pos_categ_id', 'taxes_id', 'ean13', 'default_code',
                     'to_weight', 'uom_id', 'uos_id', 'uos_coeff', 'mes_type', 'description_sale', 'description',
                     'product_tmpl_id'],
            domain:  function(self){ return [['sale_ok','=',true],['available_in_pos','=',true]]; },
            context: function(self){ return { pricelist: self.pricelist.id, display_default_code: false }; },
            loaded: function(self, products){

                self.db.add_products(products);
            }
        },{
                //ading pos table to db
                //
            model:  'pos.tables',
            fields: ['name', 'capacity', 'image'],
            domain:  null,
            loaded: function(self, tables) {

                self.db.add_tables(tables);
            }

        },    // ending load
        {
            model:  'account.bank.statement',
            fields: ['account_id','currency','journal_id','state','name','user_id','pos_session_id'],
            domain: function(self){ return [['state', '=', 'open'],['pos_session_id', '=', self.pos_session.id]]; },
            loaded: function(self, bankstatements, tmp){
                self.bankstatements = bankstatements;

                tmp.journals = [];
                _.each(bankstatements,function(statement){
                    tmp.journals.push(statement.journal_id[0]);
                });
            }
        },{
            model:  'account.journal',
            fields: [],
            domain: function(self,tmp){ return [['id','in',tmp.journals]]; },
            loaded: function(self, journals){
                self.journals = journals;

                // associate the bank statements with their journals.
                var bankstatements = self.bankstatements;
                for(var i = 0, ilen = bankstatements.length; i < ilen; i++){
                    for(var j = 0, jlen = journals.length; j < jlen; j++){
                        if(bankstatements[i].journal_id[0] === journals[j].id){
                            bankstatements[i].journal = journals[j];
                        }
                    }
                }
                self.cashregisters = bankstatements;
            }
        },{
            label: 'fonts',
            loaded: function(self){
                var fonts_loaded = new $.Deferred();

                // Waiting for fonts to be loaded to prevent receipt printing
                // from printing empty receipt while loading Inconsolata
                // ( The font used for the receipt )
                waitForWebfonts(['Lato','Inconsolata'], function(){
                    fonts_loaded.resolve();
                });

                // The JS used to detect font loading is not 100% robust, so
                // do not wait more than 5sec
                setTimeout(function(){
                    fonts_loaded.resolve();
                },5000);

                return fonts_loaded;
            }
        },{
            label: 'pictures',
            loaded: function(self){
                self.company_logo = new Image();
                var  logo_loaded = new $.Deferred();
                self.company_logo.onload = function(){
                    var img = self.company_logo;
                    var ratio = 1;
                    var targetwidth = 300;
                    var maxheight = 150;
                    if( img.width !== targetwidth ){
                        ratio = targetwidth / img.width;
                    }
                    if( img.height * ratio > maxheight ){
                        ratio = maxheight / img.height;
                    }
                    var width  = Math.floor(img.width * ratio);
                    var height = Math.floor(img.height * ratio);
                    var c = document.createElement('canvas');
                        c.width  = width;
                        c.height = height
                    var ctx = c.getContext('2d');
                        ctx.drawImage(self.company_logo,0,0, width, height);

                    self.company_logo_base64 = c.toDataURL();
                    logo_loaded.resolve();
                };
                self.company_logo.onerror = function(){
                    logo_loaded.reject();
                };
                self.company_logo.src = '/web/binary/company_logo' +'?_'+Math.random();

                return logo_loaded;
            }
        }
        ],


        // removes the current order
        // return table in screen view
        delete_current_order: function(){
            var selectedOrder = this.get('selectedOrder');
            var table = selectedOrder.table
            if (table) {
                $('#' + table.id).removeClass('oe_hidden');
            }
            selectedOrder.destroy({'reason':'abandon'});
        },

        // TODO: Find a way to extend this function properly
        load_server_data: function(){
            var self = this;
            var loaded = new $.Deferred();
            var progress = 0;
            var progress_step = 1.0 / self.models.length;
            var tmp = {}; // this is used to share a temporary state between models loaders

            function load_model(index){
                if(index >= self.models.length){
                    loaded.resolve();
                }else{
                    var model = self.models[index];
                    self.pos_widget.loading_message(_t('Loading')+' '+(model.label || model.model || ''), progress);
                    var fields =  typeof model.fields === 'function'  ? model.fields(self,tmp)  : model.fields;
                    var domain =  typeof model.domain === 'function'  ? model.domain(self,tmp)  : model.domain;
                    var context = typeof model.context === 'function' ? model.context(self,tmp) : model.context;
                    progress += progress_step;

                    if( model.model ){
                        new instance.web.Model(model.model).query(fields).filter(domain).context(context).all()
                            .then(function(result){
                                try{    // catching exceptions in model.loaded(...)
                                    $.when(model.loaded(self,result,tmp))
                                        .then(function(){ load_model(index + 1); },
                                              function(err){ loaded.reject(err); });
                                }catch(err){
                                    loaded.reject(err);
                                }
                            },function(err){
                                loaded.reject(err);
                            });
                    }else if( model.loaded ){
                        try{    // catching exceptions in model.loaded(...)
                            $.when(model.loaded(self,tmp))
                                .then(  function(){ load_model(index +1); },
                                        function(err){ loaded.reject(err); });
                        }catch(err){
                            loaded.reject(err);
                        }
                    }else{
                        load_model(index + 1);
                    }
                }
            }

            try{
                load_model(0);
            }catch(err){
                loaded.reject(err);
            }

            return loaded;
        }

    });




}