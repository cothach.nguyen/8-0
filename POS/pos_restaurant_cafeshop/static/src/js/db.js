function db(instance, module) {

    module.PosDB = module.PosDB.extend({

        // element list: Array List class
        // changeColor: if have it, change this color

        showElement: function(elementList, changeColor) {
            _.each(elementList, function(element) {
                $(element).removeClass('oe_hidden');
            })
        },

        hideElement: function(elementList) {
            _.each(elementList, function(element) {
                $(element).addClass('oe_hidden');
            })
        },

        //  hide table, parameter: table_list: array list id table
        hideAllTable: function(table_list) {
            _.each(table_list, function(id) {
                var elements = $('#' + id);
                elements.addClass('oe_hidden');
            });
        },

        showAllTable: function(table_list) {
            _.each(table_list, function(id) {
                var elements = $('#' + id);
                elements.removeClass('oe_hidden');
            });

        },


        get_table_list: function(){
            var list = [],
                stored_tables = this.load('tables',{}),
                i;
            for (i in stored_tables) {
                list.push(stored_tables[i]);
            }
            return list;

        },


        get_table_by_id: function(id){
            return this.load('tables',{})[id];
        },


        add_tables: function(tables){

            var stored_tables = this.load('tables',{}),
                i, t, len;

            if(!tables instanceof Array){
                tables = [tables];
            }
            for(i = 0, len = tables.length; i < len; i++){
                t = tables[i];
                stored_tables[t.id] = t;
            }
            this.save('tables',stored_tables);
        }
    });
}