function screens(instance, module) {

    var QWeb = instance.web.qweb;
	var _t = instance.web._t;

    module.PaymentScreenWidget = module.PaymentScreenWidget.extend({

        validate_order: function(options) {
            var self = this;
            this._super(options);
            var currentOrder = this.pos.get('selectedOrder');
            var table = currentOrder.table
            if (table) {
                $('#' + table.id).removeClass('oe_hidden');
            }
        }
    })

    module.ReceiptScreenWidget = module.ReceiptScreenWidget.extend({

        refresh: function() {
            var order = this.pos.get('selectedOrder');
            $('.pos-receipt-container', this.$el).html(QWeb.render('PosTicket',{
                    widget:this,
                    order: order,
                    table: order.get_table().name,
                    orderlines: order.get('orderLines').models,
                    paymentlines: order.get('paymentLines').models
                }));
        }

    });

    module.TableScreenWidget = module.ScreenWidget.extend({

        template:     'TableScreenWidget',
        show_numpad:     false,
        show_leftpane:   true,

        show: function(){
            this._super();
            var self = this;

            self.add_action_button({
                label: _t('Products'),
                icon: '/point_of_sale/static/src/img/icons/png48/go-previous.png',
                click: function(){
                    self.back();
                }
            });
            this.refresh();
        },

        back: function () {
            var currentOrder = this.pos.get('selectedOrder')
            if (!currentOrder.table) {
                this.pos.pos_widget.screen_selector.show_popup('error-traceback',{
                        message: 'Please choice table before start order'
                    });
            }
            else {
                this.pos_widget.screen_selector.set_current_screen('products');
            }

        },

        done: function () {
            alert('RD ...to be continue');
        },

        print: function() {
            window.print();
        },

        close: function(){
            this._super();
        },

        move_lines: function(orderFrom, table) {
            var lineMoves = orderFrom.lineMoves
            var allOrders = this.pos.get('orders').models; // get all order inside session
            var orderTo;
            for (var i = 0; i < allOrders.length; i++) {
                var orderLoop = allOrders[i]
                if (orderLoop.table == table) {
                    orderTo = orderLoop;
                }
            }
            if (orderTo) {
                orderTo['table'] = table;
                orderTo.setTable(table);
                for (var j = 0; j < lineMoves.length; j++) {
                    var lineCurrent = lineMoves[j]
                    lineMoves[j].check = null;
                    lineMoves[j].order.removeOrderline(lineMoves[j]);
                    orderTo.get('orderLines').add(lineCurrent);
                }
            }
            else {
                orderTo = new module.Order({pos: this.pos});
                this.pos.get('orders').add(orderTo);
                this.pos.set('selectedOrder', orderTo);
                orderTo['table'] = table;
                orderTo.setTable(table);
                for (var j = 0; j < lineMoves.length; j++) {
                    var lineCurrent = lineMoves[j]
                    lineMoves[j].check = null;
                    lineMoves[j].order.removeOrderline(lineMoves[j]);
                    orderTo.get('orderLines').add(lineCurrent);
                }
            }
            return this.pos_widget.screen_selector.set_current_screen('products');
        },

        merge_data: function(table) {

            var selectedOrder = this.pos.get('selectedOrder')
            var allLinesCurrentOrder = selectedOrder.get('orderLines').models;
            var allOrders = this.pos.get('orders').models; // get all order inside session
            var kitchenMove = selectedOrder.sendKitchen
            var orderMoveTo;
            // step 1: push line order old to order new
            var tableSet = false;
            _.each(allOrders, function(order) {
                if (order.table == table) {
                    orderMoveTo = order;
                    tableSet = true;
                    _.each(allLinesCurrentOrder, function(line) {
                        line['order'] = ''; //reset line order before push to new order
                        order.addOrderline(line);
                    });
                }
            })

            // step 2: push kitchen order old to order new
            if (orderMoveTo) {
                _.each(kitchenMove, function(kitchen) {
                    kitchen.table_id = table.id
                    orderMoveTo.sendKitchen.push(kitchen)

                })
                orderMoveTo['table'] = table;
                orderMoveTo.setTable(table);
                selectedOrder.destroy();
            }
            else {
                selectedOrder['table'] = table;
                selectedOrder.setTable(table);
            }
            //after merging return new screen tables
            // conditon:
            // 1. if table next change have order--> return screen tables
            // 2. if table next change have order--> return screen products

            this.showHideTable();
            if (tableSet == true) {
                return this.pos_widget.screen_selector.set_current_screen('tables');
            }
            else {
                return this.pos_widget.screen_selector.set_current_screen('products');
            }

        },

        showHideTable: function() {

            var table_hide = [];
            var table_show = [];
            var allOrders = this.pos.get('orders').models; // get all order inside session
            _.each(allOrders, function(order) {
                table_hide.push(order.table.id)
            })
            if (table_hide) {
                this.pos.db.hideAllTable(table_hide)
            }
            var tables = this.pos.db.get_table_list()
            _.each(tables, function(table) {
                var index = table_hide.indexOf(table.id)
                if (index == -1) {
                    table_show.push(table.id)
                }
            })
            if (table_show) {
                this.pos.db.showAllTable(table_show)
            }
        },

        checkTableHaveInOrders: function(table) {
            console.log('checkTableHaveInOrders')
            var self = this;
            var allOrders = this.pos.get('orders').models; // get all order inside session
            var listOrder_check = []
            _.each(allOrders, function(order) {
                if (order.table == table) {
                    listOrder_check.push(order)
                }

            })
            console.log(listOrder_check)
            if (listOrder_check) {
                this.pos_widget.screen_selector.show_popup('confirm',{
                    message: _t('Table have in Ordered'),
                    comment: _t('If you confirm this, this page re-load and please choice table again'),
                    confirm: function(){
                        $('#' + table.id).addClass('oe_hidden');
                        self.pos_widget.screen_selector.set_current_screen('tables');
                    }
                });
            }

        },

        refresh: function() {
            var order = this.pos.get('selectedOrder');
            var self = this;

            this.table_list_widget = new module.TableListWidget(this,{

                // action click choice table
                // step 1: if choice new
                // step 2: if merge data table

                click_table_action: function(table){
                    // begin user click choice table
                    var selectedOrder = self.pos.get('selectedOrder');
                    var tableCurrentOrder = selectedOrder.table
                    var lineMoves = selectedOrder.lineMoves
                    if (lineMoves == undefined) {
                        if (!tableCurrentOrder) {
                            // check table haved order current page
                            self.checkTableHaveInOrders(table);
                            selectedOrder['table'] = table;
                            selectedOrder.setTable(table);
                            self.showHideTable();
                            self.pos_widget.screen_selector.set_current_screen('products');
                        }
                        else { // if table selected
                            self.merge_data(table);
                        }
                    }
                    else {
                        self.move_lines(selectedOrder, table);
                        selectedOrder.lineMoves = undefined;
                    }

                },
                table_list: this.pos.db.get_table_list()
            });
            this.table_list_widget.replace($('.placeholder-TableListWidget'));
            $('.control-buttons').removeClass('oe_hidden');
        }
    });
    
}