function widgets(instance, module) {

    var QWeb = instance.web.qweb;
	var _t = instance.web._t;
    var qweb = instance.web.qweb;


    module.NumpadWidget = module.NumpadWidget.extend({

        clickDeleteLastChar: function() {
            var deleteLastChar = this.state.deleteLastChar();
            var self = this;
            var mode = self.state.get('mode');
            var order = self.pos.get('selectedOrder')
            var sendKitchenArrays = order.sendKitchen
            var line = order.getSelectedLine()
            if (line) {
                var productId = line.product.id
                var oldQty = this.state.get('buffer');

                if (mode && mode == "quantity") {
                    console.log('===> NumpadWidget ==> clickDeleteLastChar(), ' + oldQty);
                    _.each(sendKitchenArrays, function (kitchenLine) {
                        if (kitchenLine.id == productId && oldQty != "") {
                            kitchenLine['qty'] = parseFloat(oldQty);
                        }
                        if (kitchenLine.id == productId && oldQty == "") {
                            delete sendKitchenArrays[kitchenLine];
                        }
                    })
                }
                var sendKitchenArraysNew = order.sendKitchen;
            }
            return deleteLastChar
        },

        clickAppendNewChar: function(event) {
            // core
            var newChar;
            newChar = event.currentTarget.innerText || event.currentTarget.textContent;
            var appendNew = this.state.appendNewChar(newChar);

            //implement
            if (mode && mode == "quantity") {
                var mode = this.state.get('mode');
                var order = this.pos.get('selectedOrder');
                var sendKitchenArrays = order.sendKitchen;
                var line = order.getSelectedLine();
                var qtyLine = line.quantity; // get old quantity
                var newQty;
                var oldBufferStr = this.state.get('buffer');
                if (oldBufferStr == "0") {
                    newQty = parseFloat(newChar) - qtyLine;
                }
                else {
                    newQty = parseFloat(oldBufferStr + newChar);
                }
                if (line && line.product) {
                    var productId = line.product.id
                    if (sendKitchenArrays.length >= 1) {
                        _.each(sendKitchenArrays, function (kitchenLine) {
                        if (kitchenLine.id == productId) {
                            kitchenLine['qty'] = newQty;
                            }
                        })
                    }
                    else {
                        line.product['qty'] = newQty;
                        order.sendKitchen.push(line.product);
                    }


                }
            }


            return appendNew;
        }

    });

    module.OrderWidget = module.OrderWidget.extend({
        init: function(parent, options) {
            var self = this;
            this._super(parent, options);
        },

        _save_to_server: function (values) {
            var self = this;
            var posKitchen = new instance.web.Model('pos.kitchen');

            return  pushtoBackend = posKitchen.call('create_from_ui',
                [_.map(values, function (value) {
                    return value;
                })],
                undefined,
                {}
            ).fail(function (error, event){
                console.log(error);
                if(error.code === 200 ){    // Business Logic Error, not a connection problem
                    self.pos_widget.screen_selector.show_popup('error-traceback',{
                        message: error.data.message,
                        comment: error.data.debug
                    });
                }
                if (error.code == -32098){
                    self.pos_widget.screen_selector.show_popup('error-traceback',{
                        message: 'Không thể kết nối tới máy chủ, vui lòng kiểm tra lại đường truyền',
                        comment: error.data.debug
                    });
                }

            }).done(function (returnBE) {
                    self.pos.get('selectedOrder').sendKitchen = [];
                });

        },

        clearOrder: function() {
            var selectedOrder = this.pos.get('selectedOrder');
            selectedOrder.destroy();
            var table = selectedOrder.table
            if (table) {
                this.pos.db.showAllTable([table.id])
            }
            this.pos_widget.screen_selector.set_current_screen('tables');
        },



        changeTableButton: function() {
            // set next screen
            var self = this;

            // bussiness workflow: change to new table, not old table current order
            var selfTable = this.pos.get('selectedOrder').table;
            var allOrders = this.pos.get('orders').models; // get all order inside session
            var table_list_hide = [];
            var table_list_show = [];
            if (selfTable) {
                table_list_hide.push(selfTable.id)
                this.pos.db.hideAllTable(table_list_hide)
            }
            var tables = this.pos.db.get_table_list()

            if (tables) {
                _.each(tables, function(table) {
                    if (table != selfTable) {
                        table_list_show.push(table.id);
                    }
                })
                if (table_list_show) {
                    this.pos.db.showAllTable(table_list_show);
                }
            }
            var listButtonRender = ['.cancel']
            this.pos.db.showElement(listButtonRender, true)
            this.pos_widget.screen_selector.set_current_screen('tables');
        },

        doNotChangeTableButton: function() {
            this.pos_widget.screen_selector.set_current_screen('products');
        },

        moveTable: function() {
            this.pos_widget.screen_selector.set_current_screen('products');
        },

        moveTable: function() {
            var self = this;
            this.pos_widget.screen_selector.set_current_screen('products');
        },

        render_orderline: function(orderline) {
            var self = this;
            var el_node = this._super(orderline);
            el_node.querySelector('input').addEventListener('click', function () {
                var currentOrder = self.pos.get('selectedOrder');
                var selectedLine = currentOrder.selected_orderline
                if (selectedLine != undefined) {
                    var check = selectedLine.check
                    if ( !check) {
                        selectedLine.check = true;
                    }
                    else {
                        selectedLine.check = null;
                    }
                }
            });
            return el_node
        },

        renderElement: function(scrollbottom){

            var self = this;
            this._super(scrollbottom);
            var elementMove = $('.moveline')

            elementMove.click(function() {
                var selectedOrder = self.pos.get('selectedOrder')
                var tables = self.pos.db.get_table_list()
                var table_show = []
                for ( var i =0; i < tables.length; i++) {
                    var table = tables[i];
                    table_show.push(table.id)
                }
                if (table_show.length > 0) {
                    self.pos.db.showAllTable(table_show)
                }
                var orderLines = selectedOrder.get('orderLines').models;
                lineMoves = []
                for (var i = 0 ; i < orderLines.length; i ++) {
                    var line = orderLines[i]
                    var check = line.check
                    if (check  != undefined && check == true) {
                        lineMoves.push(orderLines[i])
                    }
                }
                if (lineMoves.length > 0) {
                    selectedOrder.lineMoves = lineMoves;
                    // show all table use moving, do not show table current
                    self.pos_widget.screen_selector.set_current_screen('tables');
                }
                else {
                    self.pos_widget.screen_selector.show_popup('error',{
                        message: _t('Please choice line move table')
                    });
                    return;
                }
            });

            var buttonKitchen = $('.kitchen')
            buttonKitchen.click(function() {
                var currentOrder = self.pos.get('selectedOrder');
                var orderLines = currentOrder.get('orderLines');
                var sendKitchen = currentOrder.sendKitchen
                if (sendKitchen.length >= 1) {
                    self._save_to_server(sendKitchen);
                    currentOrder['sendKitchen'] = [];
                }
                else {
                    self.pos_widget.screen_selector.show_popup('error',{
                        message: _t('Have not line send to Kitchen')
                    });
                    return;
                }
            });

            var clearOrder = $('.clear')
            clearOrder.click(function() {
                self.clearOrder();

            });

            var changeTableButton = $('.change')
            changeTableButton.click(function() {
                var selectedOrder = self.pos.get('selectedOrder')
                self.changeTableButton();
            });

            var doNotChangeTableButton = $('.cancel')
            doNotChangeTableButton.click(function() {
                var selectedOrder = self.pos.get('selectedOrder')
                selectedOrder.changeTable = false;
                self.doNotChangeTableButton();

            });
        }
    });

    module.PosWidget = module.PosWidget.extend({
		build_widgets: function() {

			this._super();

            // -------- Load Screens ---------

            this.table_screen = new module.TableScreenWidget(this,{});
            this.table_screen.appendTo($('.rightpane'));

            // -------- Screen Selector ---------
            this.screen_selector.screen_set.tables = this.table_screen;
            this.screen_selector.default_screen = 'tables';
		}
	});

    module.TableListWidget = module.PosBaseWidget.extend({
        template:'TableListWidget',
        show_leftpane:   false,


        init: function(parent, options) {
            var self = this;
            this._super(parent,options);
            this.model = options.model;
            this.next_screen = options.next_screen || false;
            this.click_table_action = options.click_table_action;
            this.click_table_handler = function(event){
                var table = self.pos.db.get_table_by_id(this.dataset.tableId);
                options.click_table_action(table);
            };
            this.table_list = options.table_list || [];
            this.table_cache = {};
        },

        replace: function($target){
            this.renderElement();
            var target = $target[0];
            if (target) {
                console.log(target);
                target.parentNode.replaceChild(this.el,target);
            }

        },

        get_image_url: function(table){
            return window.location.origin + '/web/binary/image?model=pos.tables&field=image&id='+table.id;
        },

        render_table: function(table){
            if(!this.table_cache[table.id]){
                var table_html = qweb.render('Table',{
                        widget:  this,
                        table: table,
                        image: this.get_image_url(table)
                    }),
                    table_node = document.createElement('div');

                table_node.innerHTML = table_html;
                table_node = table_node.childNodes[1];
                this.table_cache[table.id] = table_node;
            }
            return this.table_cache[table.id];
        },

        renderElement: function() {

            var self = this,
                // Mixed vars used on this method
                list_container,
                i, len, table_node,

                el_str  = qweb.render(this.template, {widget: this}),

                el_node = document.createElement('div');
                el_node.innerHTML = el_str;
                el_node = el_node.childNodes[1];

            if(this.el && this.el.parentNode){
                this.el.parentNode.replaceChild(el_node,this.el);
            }
            this.el = el_node;

            list_container = el_node.querySelector('.table-list');
            console.log('list_container');
            console.log(list_container);

            for(i = 0, len = this.table_list.length; i < len; i++){
                table_node = this.render_table(this.table_list[i]);
                table_node.addEventListener('click',this.click_table_handler);
                list_container.appendChild(table_node);
            }
        }
    });



}