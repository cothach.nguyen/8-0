openerp.pos_restaurant_cafeshop = function(instance){
    var module = instance.point_of_sale;
    models(instance,module);
    screens(instance,module);
    widgets(instance,module);
    db(instance,module);
};