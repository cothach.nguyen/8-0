#-*- coding:utf-8 -*-

from openerp.osv import osv, fields
import logging

_logger = logging.getLogger(__name__)

class pos_kitchen(osv.Model):

    _name = "pos.kitchen"

    _columns = {
        'name': fields.char('Name'),
        'product_id': fields.many2one('product.product', 'Product', required=True),
        'state': fields.selection([('order', 'Customer Order'), ('processing', 'Processing'), ('done', 'Done')], 'State'),
        'table_id': fields.many2one('pos.tables', 'Table', required=True),
        'qty': fields.float('Quantiry', readonly=True),
        'image': fields.related('product_id', 'image_small', type='binary', string='Image'),
    }

    _defaults = {
        'state': 'order',
    }

    def create_from_ui(self, cr, uid, orderLine, context={}):
        _logger.info('===> BEGIN create_from_gui()')
        _logger.info(orderLine)
        _logger.info('total ship: %s' % len(orderLine))
        for lines in orderLine:
            _logger.info('qty: %s' % lines.get('qty'))
            _logger.info(lines.get('display_name'))
            vals = {
                'product_id': lines.get('id'),
                'table_id': lines.get('table_id'),
                'name': lines.get('display_name'),
                'qty': lines.get('qty'),
            }
            self.create(cr, uid, vals)
        _logger.info('-' * 50)
        _logger.info('===> END create_from_gui()')
        return True


