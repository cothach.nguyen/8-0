# -*- coding: utf-8 -*-
{
    'name': 'Point of Sale for Restaurant & Cafe-Shop',
    'version': '1.0',
    'category': 'Point of Sale',
    'sequence': 103,
    'summary': 'Point of Sale for Restaurant & Cafe-Shop',
    'description': """

    Point of Sale for Restaurant & Cafe-Shop \n
    1. Multi Table for restaurant-cafeshop configuration \n
    2. Loading Table Order for POS font-end \n
    3. Order discount total & clear this \n
    ...to be continue
    Author: trungthanh.nguyen@feosco.com \n
    Website: www.feosco.com \n
    Skype: thanhchatvn \n
    Mobile: (+84) 902 403 918 \n
    """,
    'author': 'FEOSCO',
    'depends': ['point_of_sale'],
    'website': 'www.feosco.com',
    'data': [
        'security/ir.model.access.csv',
        'security/ir_rule.xml',
        'datas/pos_tables.xml',
        'views/templates.xml',
        'views/pos_view.xml',
        'views/pos_kitchen_view.xml',
        'datas/pos_tables.xml'
    ],
    'qweb':[
        'static/src/xml/pos_table.xml',
    ],
    'installable': True,
    'auto_install': False,
}
