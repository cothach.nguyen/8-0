# -*- coding: utf-8 -*-
{
    'name': 'Point of Sale: Promotion for POS',
    'version': '1.0',
    'category': 'Point of Sale',
    'sequence': 102,
    'summary': 'Promotion for POS',
    'description': """
    Promotion for POS \n
    Example: \n
    + By 1000 $ discount 10% total \n
    + By 1000 $ discount 100 $ \n
    + By 4 Product A free 1 product B \n
    + By 10 Product A free 2 product C \n
    .....etc... \n
    \n

    Author: trungthanh.nguyen@feosco.com \n
    Website: www.feosco.com \n
    Skype: thanhchatvn \n
    Mobile: (+84) 902 403 918 \n
    """,
    'author': 'FEOSCO',
    'depends': ['point_of_sale'],
    'website': 'www.feosco.com',
    'data': [
        'data/demo.xml',
        'security/ir.model.access.csv',
        'view/pos_promotion_view.xml',
        'view/template.xml',
        'view/pos_config_view.xml',
    ],
    'qweb':[
        'static/src/xml/pos_promotion.xml'
    ],
    'installable': True,
    'auto_install': False,
}
