openerp.pos_promotion = function(instance){
    var module = instance.point_of_sale;
    pos_promotion_models(instance,module);
    pos_promotion_widget(instance,module);
    pos_promotion_screens(instance,module);
};
