function pos_promotion_screens(instance, module) {

    var SuperPaymentScreenWidget = module.PaymentScreenWidget;
    module.PaymentScreenWidget = module.PaymentScreenWidget.extend({

        update_payment_summary: function() {
            SuperPaymentScreenWidget.prototype.update_payment_summary.call(this);
            var currentOrder = this.pos.get('selectedOrder');
            if (!currentOrder.promotion_total) {
                currentOrder.promotion_total = 0;
            }
            if (!currentOrder.promotion_categ_total) {
                currentOrder.promotion_categ_total = 0;
            }
            if (currentOrder.promotion_total) {
                this.$('.promotion_by_total_order').html(this.format_currency(currentOrder.promotion_total));
            }
            if (currentOrder.promotion_categ_total) {
                this.$('.promotion_by_total_category').html(this.format_currency(currentOrder.promotion_categ_total));
            }
            currentOrder.promotion_all = currentOrder.promotion_total + currentOrder.promotion_categ_total
            this.$('.promotion_total_order').html(this.format_currency(currentOrder.promotion_total + currentOrder.promotion_categ_total));

        }

    })

}