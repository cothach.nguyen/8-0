function pos_promotion_models(instance, module) {
    var round_di = instance.web.round_decimals;
    var round_pr = instance.web.round_precision

    module.PosModel.prototype.models.push({
        model: 'pos.promotion',
        fields: ['min_total_order', 'multi_discount_thesame_product', 'name', 'sum' ,'type', 'method', 'product_ids', 'categ_ids', 'dt_from', 'dt_to', 'promotion_line_ids'],
        domain: [['state', '=', 'approved']],
        context:{ 'pos': true},
        loaded: function(self, promotions){
            self.promotions = promotions;
        }
    });

    var ModuleOrderSuper = module.Order;
    module.Order = module.Order.extend({

        check_product_detail: function(promotions, order, currentOrderLines) {
//            debugger;
            // before adding promotion to order, clear all line have product gift free
            _.each(currentOrderLines, function(line) {
                var promotion_product_free = line.product.promotion_product_free;
                // how to remove looping from function addProduct()
                if (promotion_product_free == true) {
                        order.get('orderLines').remove(line);
                    }
                })
                for ( var i = 0; i < promotions.length; i ++){
                    var promotion = promotions[i];
                    var product_promotion_list = []
                _.each(promotion.promotion_line_ids, function(promotion_line) {
                    product_promotion_list.push(promotion_line);
                })
                //TODO: looping and remove the same product discount
                var dict_product = []
                if (product_promotion_list) {

                    // check multi product the same, but free product not same, check from sequence
                    if (promotion.multi_discount_thesame_product == true) {
                        _.each(product_promotion_list, function(product_promotion, index, product_promotion_list) {
                            var product_from_id =  product_promotion.product_from_id[0];
                            var sequence = product_promotion.sequence

                            _.each(product_promotion_list, function(product_promotion_list_child) {
                                if (product_from_id == product_promotion_list_child.product_from_id[0] && sequence < product_promotion_list_child.sequence) {
                                    delete product_promotion_list[index] // del the same
                                }
                            })
                        })
                    }

                    _.each(product_promotion_list, function(product_promotion) {
                        var min_qty =  product_promotion.min_qty;
                        var product_from = product_promotion.product_from_id
                        var product_free = product_promotion.product_to_id
                        var total_qty_current_order = 0;
                        var freeFromProduct;

                        _.each(currentOrderLines, function (orderLine) {
                            if (orderLine.product && orderLine.product.id == product_from.id) {
                                total_qty_current_order += orderLine.quantity;
                                freeFromProduct = orderLine.product.display_name;
                            }
                        });

                        if (total_qty_current_order >= min_qty) {
                            product_free['gift_qty'] = product_promotion.gift_qty
                            if (freeFromProduct) {
                                product_free['freeFromProduct'] = freeFromProduct;
                            }
                            dict_product.push(product_free)
                        }
                    })


                }
//                debugger;
                // how to remove looping from function addProduct()
                if (dict_product.length > 0) {
                    _.each(dict_product, function(product) {
                        product['promotion_product_free'] = true;
                        order.addProductFree(product, {
                            price: 0,
                            quantity: promotion.gift_qty,
                            freeFromProduct: product.freeFromProduct
                            })
                    })

                }


            }


        },

        check_product_category: function(promotions, currentOrder, orderLines) {
            // before loop and apply discount product category, clear all discount
            currentOrder.promotion_categ_total = 0;
            var promotion_categ_total = 0;

            // ok, after reset discount all line, begin loop line to line and change discount from promotions
            for (var i = 0; i < promotions.length; i++) {
                var promotion = promotions[i]
                var sum = promotion.sum
                var method = promotion.method
                if (orderLines) {
                    for ( var j = 0 ; j < orderLines.length; j ++){
                        var line = orderLines[j]
                        if (line.product && line.product.pos_categ_id) {
                            var promotion_categ_ids = promotion.categ_ids;
                            var pos_categ_id = line.product.pos_categ_id;

                            var index = 0;
                            for (var k = 0; k < promotion_categ_ids.length; k++) {
                                if (pos_categ_id.indexOf(promotion_categ_ids[k]) != -1) {
                                    index += 1
                                    console.log('index:' + index)
                                    if (method == 'percent') {
                                        var addSum =  (line.product.price * line.quantity / 100) * sum
                                        promotion_categ_total += addSum;
                                    }
                                    if (method == 'down_money') {
                                        var addSum = line.quantity * sum
                                        promotion_categ_total += addSum
                                    }
                                }
                            }
                        }
                    }
                }
            }
            currentOrder.promotion_categ_total = promotion_categ_total;
            if (promotion_categ_total != undefined) {
                this.pos.pos_widget.order_widget.update_summary();
            }
        },

        check_total_order: function(promotionTotalOrders, currentOrder, orderLines) {
            var getTotalTaxIncluded = currentOrder.getTotalTaxIncluded();
            currentOrder.promotion_total = 0;
            var promotion_total = currentOrder.promotion_total;
            for ( var i =0 ; i < promotionTotalOrders.length ; i ++ ) {
                var promotion = promotionTotalOrders[i]
                var min_total_order = promotion.min_total_order
                if (getTotalTaxIncluded >= min_total_order) {
                    if (promotion.method == "down_money") {
                        promotion_total += promotion.sum
                    }
                    else if (promotion.method == "percent") {
                        promotion_total += (getTotalTaxIncluded / promotion.sum)
                    }
                }
            }
            currentOrder.promotion_total = promotion_total;
            if (promotion_total != undefined) {
                this.pos.pos_widget.order_widget.update_summary();
            }
        },

        check_promotion_backend: function(payment) {
            var currentOrder = this.pos.get('selectedOrder')
            var orderLines = currentOrder.get('orderLines').models
            var allPromotions = this.pos.promotions
            var promotionActives = this.pos.config.promotion_ids; // data store promotion active for user
            var promotionReviews = []
            for (var i = 0 ; i < allPromotions.length; i++) {
                if (promotionActives && promotionActives.indexOf(allPromotions[i].id) != -1) {
                    promotionReviews.push(allPromotions[i])
                }
            }
            var promotionTotalOrders = [];
            var promotionProductDetails = [];
            var promotionProductCategories = [];
            for (var i = 0  ; i < promotionReviews.length; i++) {
                if (promotionReviews[i].type == 'total_order') { // call function check total order
                    currentOrder.promotion_total = 0; // set to 0, default
                    promotionTotalOrders.push(promotionReviews[i])
                }
                if (promotionReviews[i].type == 'product_detail') { // call function check product detail
                    promotionProductDetails.push(promotionReviews[i])
                }
                if (promotionReviews[i].type == 'product_category') { // call function check product category
                    promotionProductCategories.push(promotionReviews[i])
                }
            }

            if (promotionTotalOrders.length > 0) {
                this.check_total_order(promotionTotalOrders, currentOrder, orderLines)
            }
            if (promotionProductCategories.length > 0) {
                this.check_product_category(promotionProductCategories, currentOrder, orderLines)
            }

            if (promotionProductDetails.length > 0 && payment != undefined) {
                this.check_product_detail(promotionProductDetails, currentOrder, orderLines)
            }
        },

        addOrderline: function(line){
            ModuleOrderSuper.prototype.addOrderline.call(this, line)
            this.check_promotion_backend()
        },

        addProductFree: function(product, options){
            options = options || {};
            var attr = JSON.parse(JSON.stringify(product));
            attr.pos = this.pos;
            attr.order = this;
            var line = new module.Orderline({}, {pos: this.pos, order: this, product: product});

            if(options.quantity !== undefined){
                line.quantity    = options.quantity;
                line.quantityStr = '' + options.quantity;
            }
            if(options.price !== undefined){
                line.price = round_di(parseFloat(options.price) || 0, 2);
            }
            if( options.freeFromProduct) {
                line.promotion_product_free_name = product.freeFromProduct;
            }
            this.get('orderLines').add(line);
            line.promotion_product_free = true;

        },

        addProduct: function(product, options){
            ModuleOrderSuper.prototype.addProduct.call(this, product, options)
            this.check_promotion_backend()

        },

        removeOrderline: function( line ){
            ModuleOrderSuper.prototype.removeOrderline.call(this, line )
            this.check_promotion_backend()
        },

        getTotalTaxIncluded: function() {
            var total = ModuleOrderSuper.prototype.getTotalTaxIncluded.call(this);
            var promotion_total = this.promotion_total;
            if (promotion_total != undefined) {
                var total = total - promotion_total
                return total
            }
            else {
                return total
            }
        },

        getTotalTaxExcluded: function() {
            var getTotalTaxExcluded = ModuleOrderSuper.prototype.getTotalTaxExcluded.call(this);
            var promotion_total = this.promotion_total;
            if (promotion_total != undefined) {
                var getTotalTaxExcluded = getTotalTaxExcluded - promotion_total
                return getTotalTaxExcluded
            }
            else {
                return getTotalTaxExcluded
            }
        }

    })

    OrderlineSuper = module.Orderline
    module.Orderline = module.Orderline.extend({

        set_quantity: function(qty) {
            OrderlineSuper.prototype.set_quantity.call(this, qty)
            return this.order.check_promotion_backend()

        },

        set_discount: function(discount) {
            OrderlineSuper.prototype.set_discount.call(this, discount)
            this.order.check_promotion_backend()
        }

    })
}