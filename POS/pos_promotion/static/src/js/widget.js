function pos_promotion_widget(instance, module) {

    var QWeb = instance.web.qweb;
	var _t = instance.web._t;


    module.OrderWidget = module.OrderWidget.extend({

        update_summary: function(){
            this._super();
            var promotion_total = this.pos.get('selectedOrder').promotion_total
            var promotion_categ_total = this.pos.get('selectedOrder').promotion_categ_total
            console.log('update_summary Order promotion_total:' + promotion_total);
            console.log('update_summary Order promotion_categ_total:' + promotion_categ_total);
            if (promotion_total != undefined) {
                this.el.querySelector('.promotion_total .value').textContent = this.format_currency(promotion_total);
            }
            if (promotion_categ_total != undefined) {
                this.el.querySelector('.promotion_category .value').textContent = this.format_currency(promotion_categ_total);
            }
        }
    });

    var SuperPosWidget = module.PosWidget;
    module.PosWidget = module.PosWidget.extend({
        build_widgets: function() {
            SuperPosWidget.prototype.build_widgets.call(this)
            var self = this;
            var promotion = $(QWeb.render('Promotion'));
            promotion.click(function () {
                if (self.pos.get('selectedOrder').get('orderLines').models.length > 0) {
                    self.pos.get('selectedOrder').check_promotion_backend(true)
                }
            });
            var element = $('.cafeshop')
            promotion.appendTo(element);
        }
    })


    module.PaypadButtonWidget.include({

        renderElement: function() {
            var self = this;
            this._super();

            this.$el.click(function(){

                if (self.pos.get('selectedOrder').get('screen') === 'receipt'){  //TODO Why ?
                    console.warn('TODO should not get there...?');
                    return;
                }
                self.pos.get('selectedOrder').addPaymentline(self.cashregister);
                self.pos.get('selectedOrder').check_promotion_backend(true)
                self.pos_widget.screen_selector.set_current_screen('payment');
            });
        }
    });
}