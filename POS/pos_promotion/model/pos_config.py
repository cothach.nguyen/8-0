#-*- coding:utf-8 -*-
from openerp.osv import osv, fields

class pos_config(osv.osv):

    _inherit = "pos.config"

    _columns = {
        'promotion_ids': fields.many2many('pos.promotion', 'pos_promotion_rel', 'pos_config_id', 'promotion_id', 'Promotions', domain=[('state', '=', 'approved')]),
    }

pos_config()