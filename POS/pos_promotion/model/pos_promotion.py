#-*- coding:utf-8 -*-
from openerp.osv import osv, fields

class pos_promotion(osv.osv):

    _name = "pos.promotion"
    _columns = {
        'sequence': fields.integer('Seq', required=True),
        'name': fields.char('Name', required=True),
        'type': fields.selection([
            ('total_order', 'Total Order'),
            ('product_detail', 'Product by Product'),
            ('product_category', 'Product Category'),
        ], 'Type', required=True),
        'method': fields.selection([
            ('percent', 'Percent (%)'),
            ('down_money', 'Down Money')
        ], 'Method'),
        'product_ids': fields.many2many('product.product', 'product_promotion_rel', 'promotion_id', 'product_id', 'Products', domain=[('available_in_pos', '=', True)]),
        'categ_ids': fields.many2many('pos.category', 'pos_category_promotion_rel', 'categ_id', 'promotion_id', 'Categories'),
        'dt_from': fields.datetime('From', required=True),
        'dt_to': fields.datetime('To', required=True),
        'promotion_line_ids': fields.one2many('pos.promotion.line', 'promotion_id', 'Rule Line Products'),
        'sum': fields.float('Discount $/%'),
        'min_total_order': fields.float('Min Order'),
        'state': fields.selection([
            ('draft', 'Draft'),
            ('pending', 'Pending'),
            ('approved', 'Approved'),
            ('reject', 'Reject'),
        ], 'State'),
        'multi_discount_thesame_product': fields.boolean('Multi Discount the same Product', help='Set true if you want set multi product the same have discount, example: Product A sale quantity 10 free Product B (1), quantity 30 free product C (2), sequence (2) > sequence (1), auto get discount free Product C'),
    }

    def read(self, cr, uid, ids, fields=None, context=None, load='_classic_read'):
        res = super(pos_promotion, self).read(cr, uid, ids, fields=fields, context=context, load=load)
        line_pool = self.pool.get('pos.promotion.line')
        product_pool = self.pool.get('product.product')
        for re in res:
            if context and context.has_key('pos'):
                if re.has_key('promotion_line_ids'):
                    vals = line_pool.read(cr, uid, re.get('promotion_line_ids'), [
                        'sequence', 'product_from_id', 'product_to_id', 'min_qty', 'gift_qty'
                    ])
                    for val in vals:
                        val['product_from_id'] = product_pool.read(cr, uid, val['product_from_id'][0], [])
                        val['product_to_id'] = product_pool.read(cr, uid, val['product_to_id'][0], [])
                    re['promotion_line_ids'] = vals
        return res

    _defaults = {
        'state': 'draft',
        'multi_discount_thesame_product': False,
    }

pos_promotion()

class pos_promotion_line(osv.osv):

    _name = "pos.promotion.line"
    _columns = {
        'sequence': fields.integer('Sequence'),
        'product_from_id': fields.many2one('product.product', 'Product', domain=[('available_in_pos', '=', True)], required=True),
        'product_to_id': fields.many2one('product.product', 'Gift', domain=[('available_in_pos', '=', True)], required=True),
        'min_qty': fields.integer('Min Qty', required=True),
        'gift_qty': fields.integer('Gift Qty', required=True),
        'promotion_id': fields.many2one('pos.promotion', 'Promotion'),
    }

    _sql_constraints = [
        ('product_sequence', 'unique (product_from_id,sequence)', 'Can not the same Product and Sequence! Please try again')
    ]

pos_promotion_line()