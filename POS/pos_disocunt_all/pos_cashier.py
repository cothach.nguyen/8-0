 # -*- coding: utf-8 -*-
##############################################################################
#    
# Module : pos_cashier
# Author: thanhchatvn@gmail.com
#
# Module: Discount All POS
#
##############################################################################
from openerp.osv import fields, osv
import logging

_logger = logging.getLogger(__name__)

class inherit_pos_order_for_cashiers(osv.osv):
    _name='pos.order'
    _inherit='pos.order'
                   
    def create_from_ui(self, cr, uid, orders, context=None):
        _logger.info("===> begin create_from_ui()")
        _logger.info(orders)
        order_ids = []
        res = super(inherit_pos_order_for_cashiers, self).create_from_ui(cr, uid, orders, context=context)
        _logger.info("===> end create_from_ui()")
        return res

    _columns = {
        'cashier_name': fields.char('Cashier', size=128),
    }


inherit_pos_order_for_cashiers()