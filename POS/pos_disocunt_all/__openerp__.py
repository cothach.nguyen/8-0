# -*- coding: utf-8 -*-
{
    'name': 'POS Discount All Oder for Cafeshop & Restaurant',
    'version': '1.0',
    'category': 'Point Of Sale',
    'sequence': 100,
    'author': 'FEOSCO',
    'summary': 'Function Discount Total Order',
    'description': """
    Author: trungthanh.nguyen@feosco.com \n
    Website: www.feosco.com \n
    Skype: thanhchatvn \n
    Mobile: (+84) 902 403 918 \n
    """,
    'depends': ["pos_restaurant_cafeshop"],
    'data': [
        'views/templates.xml',
    ],
    'qweb': [
        'static/src/xml/pos_disocunt_all.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}