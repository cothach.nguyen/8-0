openerp.pos_disocunt_all = function(instance){
    var module   = instance.point_of_sale;
    var round_pr = instance.web.round_precision
    var QWeb = instance.web.qweb;

    module.OrderWidget.include({
        init: function(parent, options) {
            var self = this;
            this._super(parent,options);
            this.summary_selected = false;
            this.clear_all = false;

            var line_click_handler = this.line_click_handler;
            this.line_click_handler = function(event){
                self.deselect_summary();
                line_click_handler.call(this, event)
            }
        },
        select_summary:function(){
            if (this.summary_selected)
                return;
            this.deselect_summary();
            this.summary_selected = true;
            $('.summary').addClass('selected')
            this.pos_widget.numpad.state.reset();
            this.pos_widget.numpad.state.changeMode('discount');
        },
        deselect_summary:function(){
            this.summary_selected = false;
            $('.summary').removeClass('selected')
        },
        set_value: function(val){
            if (!this.summary_selected)
                return this._super(val);
            var mode = this.numpad_state.get('mode');
            if (mode=='discount'){
                var order = this.pos.get('selectedOrder');
                var lines = []
                $.each(order.get('orderLines').models, function (k, line){
                    lines.push(line)
                    line.set_discount(val)
                })
                var element = $('.clearDiscount')
                element.removeClass('oe_hidden');
            }
        },

        enable_numpad: function(){
            if (!this.clear_all) {
                return this._super();
            }
            else {
                console.log(this.pos);
                var order = this.pos.get('selectedOrder');
                $.each(order.get('orderLines').models, function (k, line ) {
                    line.set_discount(0);
                })
            }

        },

        renderElement:function(scrollbottom){
            var self = this;
            this._super(scrollbottom);

            $('.discountAll').click(function(){
                self.pos.get('selectedOrder').deselectLine(this.orderline);
                self.pos_widget.numpad.state.reset();
                self.pos_widget.numpad.state.changeMode('discount');
                self.select_summary()


            })
            $('.clearDiscount').click(function(){
                self.pos.get('selectedOrder').deselectLine(this.orderline);
                self.clear_all = true;
                self.enable_numpad();

            })
        }
    });

};

