# -*- coding: utf-8 -*-
{
    'name': 'Point of Sale: Loyalty Management for POS',
    'version': '1.0',
    'category': 'Point of Sale',
    'sequence': 101,
    'summary': 'Loyalty Management for POS',
    'description': """
    Loyalty Management for POS \n
    Author: trungthanh.nguyen@feosco.com \n
    Website: www.feosco.com \n
    Skype: thanhchatvn \n
    Mobile: (+84) 902 403 918 \n
    """,
    'author': 'FEOSCO',
    'depends': ['point_of_sale'],
    'website': 'www.feosco.com',
    'data': [
        'security/ir.model.access.csv',
        'data/datas.xml',
        'view/pos_loyalty_view.xml',
        'view/pos_promotion_point_view.xml',
        'view/template.xml',
    ],
    'qweb':[
        'static/src/xml/pos_loyalty.xml'
    ],
    'installable': True,
    'auto_install': False,
}
