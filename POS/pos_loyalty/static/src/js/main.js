openerp.pos_loyalty = function(instance){
    var module = instance.point_of_sale;
    pos_loyalty_models(instance,module);
    pos_loyalty_widget(instance,module);
    pos_loyalty_screens(instance,module);
};
