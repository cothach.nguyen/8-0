function pos_loyalty_models(instance, module) {

    module.PosModel.prototype.models.push({
        model:  'res.partner',
        fields: [
            'name',
            'street',
            'city',
            'state_id',
            'country_id',
            'vat',
            'phone',
            'zip',
            'mobile',
            'email',
            'ean13',
            'write_date',
            'pos_member',
            'pos_startdate',
            'pos_total_point',
            'pos_loyalty_category_id',
            'pos_last_shopingdate',
            'pos_redeemed_point'
        ],
        domain: null,
        loaded: function(self, partners){
            self.partners = partners;
            self.db.add_partners(partners);
        }

    });

    module.PosModel.prototype.models.push({
        model:  'pos.loyalty.category',
        fields: ['name','code','active','purchase_limit','point_to_price','price_to_point'],
        domain: null,
        loaded: function(self,loyaltys){
            self.loyaltys = loyaltys;
        }

    });

    module.PosModel.prototype.models.push({
        model:  'pos.promotion.point',
        fields: ['name','code','price_to_point','apply','min_order','product_ids'],
        domain: null,
        context: {'point_of_sale': true},
        loaded: function(self, promotionPoints){
            self.promotionPoints = promotionPoints;
        }

    });

//    var PaymentlineSuper = module.Paymentline;
//    module.Paymentline = module.Paymentline.extend({
//
//        set_amount: function(value){
//            var self = this;
//            var res = PaymentlineSuper.prototype.set_amount.call(this);
//            debugger;
//            return res
//        }
//    });

    var ModuleOrderSuper = module.Order;
    module.Order = module.Order.extend({

        export_as_JSON: function() {
            var res = ModuleOrderSuper.prototype.export_as_JSON.call(this);
            var currentOrder = this.pos.get('selectedOrder');
            var use_point = currentOrder.use_point;
            res['use_point'] = use_point;
            var totalGiftPoint = currentOrder.totalGiftPoint
            if (totalGiftPoint) {
                res['total_gift_point'] = totalGiftPoint
            }
            return res;
        },

        set_new_client: function(partner) {
            var loyaltys = this.pos.loyaltys
            var pos_loyalty_category_id = partner.pos_loyalty_category_id[0]
            if (pos_loyalty_category_id && loyaltys.length > 0) {
                _.each(loyaltys, function(loyalty) {
                    if (loyalty.id == pos_loyalty_category_id) {
                        partner.loyalty = loyalty;
                    }
                })
            }
            this.set('client', partner);

        },


        //move to overide function
        set_client: function(client){
            var self = this;
            ModuleOrderSuper.prototype.export_as_JSON.call(this);
            var partners = this.pos.partners
            _.each(partners, function(partner) {
                if (partner.id == client.id) {
                    self.set_new_client(partner);
                }
            })
        },

        get_client: function(){
            var res = ModuleOrderSuper.prototype.get_client.call(this);
            return res
        },

        getTotalTaxIncluded: function() {
            var dueTotal = ModuleOrderSuper.prototype.getTotalTaxIncluded.call(this);
//            console.log('dueTotal before: ' + dueTotal)
            var currentOrder = this.pos.get('selectedOrder');
            var covertPriceDown = currentOrder.covertPriceDown
            var promotion_total = this.promotion_total;
            if (promotion_total != undefined) {
                dueTotal += promotion_total
            }
            if (covertPriceDown != undefined) {
                dueTotal -= covertPriceDown;
            }
//            console.log('dueTotal after: ' + dueTotal)
            return dueTotal

        },

        getPaidTotal: function() {
            var currentOrder = this.pos.get('selectedOrder');
            var covertPriceDown = currentOrder.covertPriceDown
            var getPaidTotal = ModuleOrderSuper.prototype.getPaidTotal.call(this);
//            console.log('getPaidTotal after: ' + getPaidTotal)
            if (covertPriceDown != undefined) {
                getPaidTotal -= covertPriceDown;
            }
//            console.log('getPaidTotal after: ' + getPaidTotal)
            return getPaidTotal
        }

    })
}