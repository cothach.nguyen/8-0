function pos_loyalty_widget(instance, module) {

    module.OrderWidget.include({

        get_total_gift_point: function() {
            var self = this;

            return totalGiftPoint
        },

        update_summary: function(){
            var self = this;
            this._super();
            var currentOrder = this.pos.get('selectedOrder')
            var partner = currentOrder.get_client();
            var totalGiftPoint = 0;
            var ProductLineIds = [];
            var orderLines = currentOrder.get('orderLines').models
            _.each(orderLines, function(line) {
                if (line && line.product) {
                    ProductLineIds.push(line.product.id)
                }
            })

            var promotionPoints = this.pos.promotionPoints
            if (promotionPoints) {
                _.each(promotionPoints, function(promotionPoint) {
                    if (promotionPoint.product_ids) {
                        _.each(promotionPoint.product_ids, function(productId) {
                            if (ProductLineIds.indexOf(productId) != -1 ) {
                                _.each(orderLines, function(line) {

                                    if (line && line.product && line.product.id == productId) {
                                        var subtotal = line.price * line.quantity
                                        if (line.discount != 0) {
                                            var amount_total =  subtotal - subtotal / line.discount
                                            var price_to_point  = promotionPoint.price_to_point
                                            totalGiftPoint += amount_total * price_to_point
                                        }
                                        if (line.discount == 0) {
                                            var amount_total = subtotal
                                            var price_to_point  = promotionPoint.price_to_point
                                            totalGiftPoint += amount_total * price_to_point
                                        }
                                    }
                                })
                            }
                        })
                    }
                })
            }
            currentOrder.totalGiftPoint = totalGiftPoint
             // update realtime from fontend click add new product
            //
            if (partner != undefined) {
                var pos_total_point = partner['pos_total_point']
                this.el.querySelector('.summary .loyalty_point_customer .value').textContent = this.format_currency(pos_total_point);
                this.el.querySelector('.summary .loyalty_point_gift .value').textContent = this.format_currency(totalGiftPoint);

            }
            else {
                this.el.querySelector('.summary .loyalty_point_customer .value').textContent = this.format_currency(0);
                this.el.querySelector('.summary .loyalty_point_gift .value').textContent = this.format_currency(totalGiftPoint);
            }
        }
    })
}