function pos_loyalty_screens(instance, module) {

    module.PaymentScreenWidget.include({
        defaults: {
            buffer: "0",
            mode: "quantity"
        },

        init: function(parent, options) {
            var self = this;
            this._super(parent, options);
            this.last
            this.line_change_handler = function(event){
                var currentOrder = self.pos.get('selectedOrder')
                var node = this;
                while(node && !node.classList.contains('paymentline')){
                    node = node.parentNode;
                }
                if(node){
                    var covertPriceDown = currentOrder.covertPriceDown
                    if (covertPriceDown != undefined) {
                        var value = parseFloat(this.value)
                        node.line.set_amount(value + covertPriceDown);
                    }
                    else {
                        node.line.set_amount(this.value);
                    }

                }
            };

        },

        appendNewChar: function(newChar) {
            var oldBuffer;
            oldBuffer = this.get('buffer');
            if (newChar != oldBuffer) {
                this.set({
                    buffer:  newChar
                });
                return true
            }
            else { return false}
        },

        renderElement: function(){
            this._super();
            var self = this;
            this.el.querySelector('input').addEventListener('keyup', function(event) {
                var value = this.value;

                // start check and stoping event change value input
                // call appendNewChar and return value compare before input && after input
                // if the same, reject and nothing to do

                var appendNewChar = self.appendNewChar(value);
                var currentOrder = self.pos.get('selectedOrder')
                var selected_line = currentOrder.selected_paymentline;
                var paidTotal = currentOrder.getTotalTaxIncluded();
                if (appendNewChar == true) {

                    var partner = currentOrder.get_client();
                    if (partner && partner.loyalty) {
                        currentOrder.loyalty_point = value;
                        var point_to_price = partner.loyalty.point_to_price
                        var pos_total_point = partner.pos_total_point
                        var valueFloat = parseFloat(value);
                        if (value=="") {
                            value = 0;
                        };

                        if ( valueFloat <= pos_total_point){
                            var covertPriceDown = valueFloat / point_to_price;
                            if (covertPriceDown >= 0 && covertPriceDown <= paidTotal) {
                                currentOrder.covertPriceDown = covertPriceDown;
                                currentOrder.use_point = value;
                                currentOrder.pos_total_point = pos_total_point;
                                currentOrder.remain_pos_point = pos_total_point - value;

                                selected_line.set_amount(paidTotal - covertPriceDown);
                                var toFixed = selected_line.amount.toFixed(2)
                                selected_line.node.querySelector('input').value = toFixed;
                            }
                            if (covertPriceDown > paidTotal) {
                                this.value = "";
                                currentOrder.covertPriceDown = 0;
                                currentOrder.use_point = 0;
                                currentOrder.pos_total_point = pos_total_point;
                                currentOrder.remain_pos_point = pos_total_point;
                                selected_line.set_amount(paidTotal);
                                var toFixed = selected_line.amount.toFixed(2)
                                selected_line.node.querySelector('input').value = toFixed;

                                self.pos_widget.screen_selector.show_popup('error-traceback',{
                                    message: 'Can not paid with point covert money > total order current !!!'
                                });
                            }

                        }
                        else {
                            this.value = "";
                            currentOrder.covertPriceDown = 0;
                            currentOrder.use_point = 0;
                            currentOrder.pos_total_point = pos_total_point;
                            currentOrder.remain_pos_point = pos_total_point;
                            selected_line.set_amount(paidTotal);
                            var toFixed = selected_line.amount.toFixed(2)
                            selected_line.node.querySelector('input').value = toFixed;
                            self.pos_widget.screen_selector.show_popup('error-traceback',{
                                message: 'Point not enough !!!'
                            });
                        }
                    }
                    else {
                        this.value = "";
                        self.pos_widget.screen_selector.show_popup('error-traceback',{
                            message: 'Please add Customer before use point paid !!!'
                        });
                    }
                }


            });
            this.update_payment_summary();
        },

        update_payment_summary: function() {
            var self = this;
            this._super();
            var currentOrder = this.pos.get('selectedOrder');
            var paidTotal = currentOrder.getPaidTotal();
            var dueTotal = currentOrder.getTotalTaxIncluded();
            var remaining = dueTotal > paidTotal ? dueTotal - paidTotal : 0;
            var change = paidTotal > dueTotal ? paidTotal - dueTotal : 0;

            this.$('.payment-due-total').html(this.format_currency(dueTotal));
            this.$('.payment-paid-total').html(this.format_currency(paidTotal));
            this.$('.payment-remaining').html(this.format_currency(remaining));
            this.$('.payment-change').html(this.format_currency(change));
            if(currentOrder.selected_orderline === undefined){
                remaining = 1;  // What is this ?
            }

            if(this.pos_widget.action_bar){
                this.pos_widget.action_bar.set_button_disabled('validation', !this.is_paid());
                this.pos_widget.action_bar.set_button_disabled('invoice', !this.is_paid());
            }
            if (currentOrder.covertPriceDown) {

                this.$('.payment-paid-point').html(this.format_currency(currentOrder.covertPriceDown));
            }
            else {
                this.$('.payment-paid-point').html(this.format_currency(0));
            }
        }


    })
}