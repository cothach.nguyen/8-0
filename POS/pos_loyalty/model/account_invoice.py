#-*- coding:utf-8 -*-

from openerp import api
from openerp.osv import fields, osv
import logging
_logger = logging.getLogger(__name__)

class account_invoice(osv.osv):

    _inherit = "account.invoice"

    @api.one
    @api.depends('invoice_line.price_subtotal', 'tax_line.amount', 'pos_point_paid')
    def _compute_amount(self):
        if self.pos_point_paid:
            self.amount_untaxed = sum(line.price_subtotal for line in self.invoice_line)
            self.amount_tax = sum(line.amount for line in self.tax_line)
            self.amount_total = self.amount_untaxed + self.amount_tax  - self.pos_point_paid
        else:
            self.amount_untaxed = sum(line.price_subtotal for line in self.invoice_line)
            self.amount_tax = sum(line.amount for line in self.tax_line)
            self.amount_total = self.amount_untaxed + self.amount_tax


    @api.one
    @api.depends(
        'state', 'currency_id', 'invoice_line.price_subtotal',
        'move_id.line_id.account_id.type',
        'move_id.line_id.amount_residual',
        'move_id.line_id.amount_residual_currency',
        'move_id.line_id.currency_id',
        'move_id.line_id.reconcile_partial_id.line_partial_ids.invoice.type',
        'pos_point_paid'
    )
    def _compute_residual(self):
        _logger.info('begin _compute_residual')
        action_dict = super(account_invoice, self)._compute_residual()
        if self.pos_point_paid:
            self.residual = self.residual - self.pos_point_paid
        _logger.info('self.residual: %s' % self.residual)
        _logger.info('end _compute_residual')
        return action_dict


    _columns = {
        'pos_point_paid': fields.float('Point Paid', readonly=True),
    }

account_invoice()
