#-*- coding:utf-8 -*-

from openerp.osv import osv, fields

class pos_loyalty_category(osv.osv):

    _name = "pos.loyalty.category"

    _columns = {
        'name': fields.char('Name', required=True, size=128),
        'code': fields.char('Code', required=True, size=128),
        'active': fields.boolean('Active'),
        'purchase_limit': fields.float('Purchase Limit'),
        'point_to_price': fields.float('Covert point to price', required=True),
        'price_to_point': fields.float('Covert price to point', required=True),
        'default': fields.boolean('Default Type Loyalty for Customer'),
    }
pos_loyalty_category()


class res_partner(osv.osv):

    _inherit = "res.partner"
    _columns = {
        'pos_member': fields.boolean('Member POS'),
        'pos_startdate': fields.datetime('Join in date'),
        'pos_total_point': fields.float('Total Point'),
        'pos_loyalty_category_id': fields.many2one('pos.loyalty.category', 'Loyalty Category'),
        'pos_last_shopingdate': fields.datetime('Last Shopping date'),
        'pos_redeemed_point': fields.float('Redeemed Point'),
        'res_partner_pos_ids': fields.one2many('res.partner.pos', 'partner_id', 'Point Transaction', readonly=True),
    }

res_partner()

class res_partner_pos(osv.osv):

    _name = "res.partner.pos"
    _columns = {
        'partner_id': fields.many2one('res.partner', 'Partner'),
        'pos_order_id': fields.many2one('pos.order', 'POS Order'),
        'create_date': fields.datetime('Shopping date', readonly=True),
        'total_amount': fields.float('Total Amount'),
        'total_point': fields.float('Total Point / Order'),
    }


