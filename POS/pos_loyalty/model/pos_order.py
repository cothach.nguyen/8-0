#-*- coding:utf-8 -*-
from openerp.osv import osv, fields
import datetime
import logging
import openerp.addons.decimal_precision as dp
from openerp.tools.translate import _
_logger = logging.getLogger(__name__)

class PosOrder(osv.Model):

    _inherit = "pos.order"

    def action_invoice(self, cr, uid, ids, context=None):
        """
        add pos_point_paid to invoice from POS
        """
        inv_ref = self.pool.get('account.invoice')
        inv_line_ref = self.pool.get('account.invoice.line')
        product_obj = self.pool.get('product.product')
        inv_ids = []

        for order in self.pool.get('pos.order').browse(cr, uid, ids, context=context):
            if order.invoice_id:
                inv_ids.append(order.invoice_id.id)
                continue

            if not order.partner_id:
                raise osv.except_osv(_('Error!'), _('Please provide a partner for the sale.'))

            acc = order.partner_id.property_account_receivable.id
            inv = {
                'name': order.name,
                'pos_point_paid': order.point_paid,
                'origin': order.name,
                'account_id': acc,
                'journal_id': order.sale_journal.id or None,
                'type': 'out_invoice',
                'reference': order.name,
                'partner_id': order.partner_id.id,
                'comment': order.note or '',
                'currency_id': order.pricelist_id.currency_id.id, # considering partner's sale pricelist's currency
            }
            inv.update(inv_ref.onchange_partner_id(cr, uid, [], 'out_invoice', order.partner_id.id)['value'])
            if not inv.get('account_id', None):
                inv['account_id'] = acc
            inv_id = inv_ref.create(cr, uid, inv, context=context)

            self.write(cr, uid, [order.id], {'invoice_id': inv_id, 'state': 'invoiced'}, context=context)
            inv_ids.append(inv_id)
            for line in order.lines:
                inv_line = {
                    'invoice_id': inv_id,
                    'product_id': line.product_id.id,
                    'quantity': line.qty,
                }
                inv_name = product_obj.name_get(cr, uid, [line.product_id.id], context=context)[0][1]
                inv_line.update(inv_line_ref.product_id_change(cr, uid, [],
                                                               line.product_id.id,
                                                               line.product_id.uom_id.id,
                                                               line.qty, partner_id = order.partner_id.id,
                                                               fposition_id=order.partner_id.property_account_position.id)['value'])
                inv_line['price_unit'] = line.price_unit
                inv_line['discount'] = line.discount
                inv_line['name'] = inv_name
                inv_line['invoice_line_tax_id'] = [(6, 0, [x.id for x in line.product_id.taxes_id] )]
                inv_line_ref.create(cr, uid, inv_line, context=context)
            inv_ref.button_reset_taxes(cr, uid, [inv_id], context=context)
            self.signal_workflow(cr, uid, [order.id], 'invoice')
            inv_ref.signal_workflow(cr, uid, [inv_id], 'validate')

        if not inv_ids: return {}

        mod_obj = self.pool.get('ir.model.data')
        res = mod_obj.get_object_reference(cr, uid, 'account', 'invoice_form')
        res_id = res and res[1] or False
        return {
            'name': _('Customer Invoice'),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': [res_id],
            'res_model': 'account.invoice',
            'context': "{'type':'out_invoice'}",
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'current',
            'res_id': inv_ids and inv_ids[0] or False,
        }

    def _amount_all(self, cr, uid, ids, name, args, context=None):
        _logger.info('begin _amount_all')
        res = super(PosOrder, self)._amount_all(cr, uid, ids, name, args, context=context)
        for key, value in res.iteritems():
            order = self.browse(cr, uid, key)
            res[key]['point_paid'] = 0
            if order.use_point and order.pos_loyalty_category_id:
                res[key]['amount_total'] = res[key]['amount_total'] - order.use_point / order.pos_loyalty_category_id.point_to_price
                res[key]['amount_paid'] = res[key]['amount_paid'] - order.use_point / order.pos_loyalty_category_id.point_to_price
                res[key]['point_paid'] = order.use_point / order.pos_loyalty_category_id.point_to_price
        _logger.info(res)
        _logger.info('end _amount_all')
        return res

    _columns = {
        'use_point': fields.float('Use Point', readonly=True),
        'total_gift_point': fields.float('Gift Point Order', readonly=True),
        'amount_tax': fields.function(_amount_all, string='Taxes', digits_compute=dp.get_precision('Account'), multi='all'),
        'amount_total': fields.function(_amount_all, string='Total', multi='all'),
        'amount_paid': fields.function(_amount_all, string='Paid', states={'draft': [('readonly', False)]}, readonly=True, digits_compute=dp.get_precision('Account'), multi='all'),
        'amount_return': fields.function(_amount_all, 'Returned', digits_compute=dp.get_precision('Account'), multi='all'),
        'point_paid': fields.function(_amount_all, string='Point Paid', store={
            'pos.order': (lambda self, cr, uid, ids, c={}: ids, ['state'], 10),
                },
            multi='all', help="Point of Customer use paid order POS"),
        'pos_loyalty_category_id': fields.many2one('pos.loyalty.category', 'Loyalty Category'),
    }



    def _order_fields(self, cr, uid, ui_order, context=None):
        res = super(PosOrder, self)._order_fields(cr, uid, ui_order, context=context)
        if ui_order.has_key('use_point'):
            res['use_point'] = float(ui_order['use_point'])
        if ui_order.has_key('total_gift_point'):
            res['total_gift_point'] = float(ui_order['total_gift_point'])
        partner_pool = self.pool.get('res.partner')
        if res and res['partner_id']:
            partner = partner_pool.browse(cr, uid, res['partner_id'])
            pos_loyalty_category_id = partner.pos_loyalty_category_id.id if partner.pos_loyalty_category_id else None
            if pos_loyalty_category_id:
                res['pos_loyalty_category_id'] = pos_loyalty_category_id
        return res


    def action_paid(self, cr, uid, ids, context={}):
        res = super(PosOrder, self).action_paid(cr, uid, ids)
        loyalty_categ_ids = self.pool.get('pos.loyalty.category').search(cr, uid, [('default', '=', True)])
        pos_pool = self.pool.get('res.partner.pos')
        partner_pool = self.pool.get('res.partner')
        if loyalty_categ_ids:
            loyalty = self.pool.get('pos.loyalty.category').browse(cr, uid, loyalty_categ_ids[0])
            for this in self.browse(cr, uid, ids):
                partner_id = this.partner_id.id
                total_amount = this.amount_total
                total_gift_point = this.total_gift_point - this.use_point
                val = {
                    'partner_id': partner_id,
                    'pos_order_id': this.id,
                    'total_amount': total_amount,
                    'total_point': total_gift_point,
                }
                pos_pool.create(cr, uid, val)
                val_update = {
                    'pos_total_point': this.partner_id.pos_total_point + total_gift_point,
                    'pos_member': True,
                    'pos_loyalty_category_id': loyalty_categ_ids[0],
                    'pos_last_shopingdate': datetime.datetime.today()
                }
                if not this.partner_id.pos_startdate:
                    val_update['pos_startdate'] = datetime.datetime.today()
                partner_pool.write(cr, uid, partner_id, val_update)
        return res

PosOrder()
