#-*- coding:utf-8 -*-
from openerp.osv import osv, fields
import logging
from openerp.tools.translate import _
_logger = logging.getLogger(__name__)

class PosPromotionPoint(osv.Model):

    _name = "pos.promotion.point"

    _columns = {
        'name': fields.char('Name', size=128, required=True),
        'code': fields.char('Code', size=128, required=True),
        'price_to_point': fields.float('Price covert Point', required=True),
        'apply': fields.selection([
            ('choice', 'Apply for Products your chocie'),
            ('total_order', 'Apply for Total Order POS'),
        ], 'Method Apply', required=True),
        'min_order': fields.float('Min Order'), # use for apply method : total_order
        'product_ids': fields.many2many('product.product', 'pos_promotion_point_product_rel', 'pos_promotion_point_id', 'product_id', string='Products'), # use for apply method: choice
    }
    def create(self, cr, uid, vals, context={}):
        if vals and vals.get('price_to_point'):
            if vals.get('price_to_point') <= 0:
                raise osv.except_osv(_('Error'), _(' Price cover to Point can not <= 0'))
        res = super(PosPromotionPoint, self).create(cr, uid, vals, context=context)
        return res

    _default = {
        'type': 'choice',
    }

    # method sync with pos font-end
    def read(self, cr, uid, ids, fields=None, context=None, load='_classic_read'):
        _logger.info('begin read')
        res = super(PosPromotionPoint, self).read(cr, uid, ids, fields=fields, context=context, load=load)
        if context and context.has_key('point_of_sale'):
            _logger.info(res)
        _logger.info('end read')
        return res

PosPromotionPoint()

class Product(osv.Model):

    _inherit = "product.product"

    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        res = super(Product, self).search(cr, uid, args, offset=offset, limit=limit, order=order, context=context, count=count)
        if context and context.has_key('promotion_point'):
            cr.execute( "SELECT product_id FROM pos_promotion_point_product_rel")
            product_ids = [x[0] for x in cr.fetchall()]
            if product_ids:
                for p_id in product_ids:
                    if p_id in res:
                        res.remove(p_id)
        return res
Product()

