# -*- coding: utf-8 -*-
{
    'name': 'Point of Sale: Install All module POS Restaurant & CafeShop Depends',
    'version': '1.0',
    'category': 'Point of Sale',
    'sequence': 0,
    'summary': 'Install All module POS Restaurant & CafeShop Depends',
    'description': """
    Install All module POS Restaurant & CafeShop Depends \n
    Author: trungthanh.nguyen@feosco.com \n
    Skype: thanhchatvn \n
    Mobile: (+84) 902 403 918 \n
    """,
    'author': 'FEOSCO',
    'depends': [
        'pos_restaurant_tables',
        'pos_restaurant_print_bill',
        'pos_restaurant_cafeshop',
        'pos_promotion',
        'pos_loyalty',
    ],
    'website': 'www.feosco.com',
    'installable': True,
    'auto_install': False,
}
