# -*- coding: utf-8 -*-
{
    'name': 'Point of Sale: Left Bar multi button',
    'version': '1.0',
    'category': 'Point of Sale',
    'summary': 'Left Bar multi button',
    'description': """
    Left Bar multi button \n
    Author: trungthanh.nguyen@feosco.com \n
    Website: www.feosco.com \n
    Skype: thanhchatvn \n
    Mobile: (+84) 902 403 918 \n
    """,
    'author': 'FEOSCO',
    'depends': ['point_of_sale'],
    'website': 'www.feosco.com',
    'data': [
        'view/template.xml'
    ],
    'qweb':[
        'static/src/xml/button.xml'
    ],
    'installable': True,
    'auto_install': False,
}
