function screens(instance, module) {

    var QWeb = instance.web.qweb;
	var _t = instance.web._t;

    module.MultiButtonScreen = module.ScreenWidget.extend({

        template:     'MultiButtonScreen',
        show_numpad:     false,
        show_leftpane:   false,

        show: function(){

            this._super();
            var self = this;

            self.add_action_button({
                label: _t('Methods'),
                icon: '/point_of_sale/static/src/img/icons/png48/go-previous.png',
                click: function(){
                    self.back();
                }
            });
            this.refresh();
        },



        refresh: function() {
            debugger;
            this.MultiButtonWidget = new module.MultiButtonWidget(this,{});
            this.MultiButtonWidget.replace($('.placeholder-MultiButtonWidget'));
        }
    });

}