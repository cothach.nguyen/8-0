{
    'name': 'Feosco Website',
    'category': 'Hidden',
    'version': '1.0',
    'depends': ['website', 'feosco_web'],
    'auto_install': False,
    'data': [
        'views/include.xml',
    ],
}