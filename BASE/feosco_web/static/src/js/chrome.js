openerp.feosco_web = function(instance) {
    instance.web.WebClient.include({
        set_title: function(title) {
            title = _.str.clean(title);
            var sep = _.isEmpty(title) ? '' : ' - ';
            document.title = title + sep;
            // document.title = title + sep + 'FEOS'; //Khong dung code nay vi no fix cung FEOSCO tren title
        }
    });


    instance.web.WebClient.include({
        init: function (parent, client_options) {
            this._super(parent, client_options);
            // this.set('title_part', {"zopenerp": "FEOS"});
            this.set('title_part', {"zopenerp": " "});
        }
    });


    instance.web.UserMenu.include({

        start: function() {
            var self = this;
            this._super.apply(this, arguments);
            $('.dropdown-menu li:eq(0)').hide();
            $('.dropdown-menu li:eq(1)').hide();
            $('.dropdown-menu li:eq(2)').hide();
            $('.dropdown-menu li:eq(3)').hide();
            $('.dropdown-menu li:eq(4)').hide();
            // $('.oe_logo > img').attr("src", "/feosco_web/static/src/img/logo.png"); //Khong xai doan code nay vi no fix cung logo cong ty (feosco)

        },

        on_menu_help: function() {
            window.open('http://www.feosco.com', '_blank');
        }
    });

};
