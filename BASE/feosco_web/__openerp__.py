{
    'name': 'Feosco Web',
    'author': 'Feosco',
    'category': 'Feosco',
    'version': '1.0',
    'description':
        """
Feosco Web Customize module.\n
This module provides the core of the OpenERP Web Client.
        """,
    'depends': ['web','im_odoo_support', 'feosco_web_adblock'],
    'auto_install': True,
    'data': [
        'views/webclient_templates.xml',
        'views/base_export_language.xml',
    ],
    'qweb' : [
        "static/src/xml/*.xml",
    ],
}