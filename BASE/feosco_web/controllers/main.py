# # -*- coding: utf-8 -*-


import openerp
from openerp.addons.web import http

class feosco_web(openerp.addons.web.controllers.main.Home):

    @http.route()
    def web_login(self, redirect=None, **kw):
        # Ham nay dung de redirect ve home sau khi login
        # Mac dinh sau khi login se redirect ve /web
        if redirect is None:
            redirect = '/'
        return super(feosco_web, self).web_login(redirect, **kw)