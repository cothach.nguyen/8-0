from flask import Flask, render_template, request, jsonify
import xmlrpclib, json
# Initialize the Flask application
app = Flask(__name__)


def call(method, val):
    username = 'admin'
    pwd = 'P@ssw0rd'
    dbname = 'register'

    sock_common = xmlrpclib.ServerProxy ('http://123.30.109.227:7069/xmlrpc/common')
    uid = sock_common.login(dbname, username, pwd)
    sock = xmlrpclib.ServerProxy('http://123.30.109.227:7069/xmlrpc/object')
    print 'restful_get_version'
    results = sock.execute(dbname, uid, pwd, 'feosco.key', '%s' % method, json.dumps(val))
    return results

@app.route('/api/restful_re_register', methods = ['POST'])
def restful_re_register():
    # Get the parsed contents of the form data
    val_json = request.json
    print 'restful_re_register'
    print val_json

    return  call('restful_re_register', val_json)

@app.route('/api/restful_get_version', methods = ['POST'])
def restful_get_version():
    # Get the parsed contents of the form data
    val_json = request.json
    print 'restful_get_version'
    print val_json

    return  call('restful_get_version', val_json)

@app.route('/api/restful_register_key', methods = ['POST'])
def restful_register_key():
    # Get the parsed contents of the form data
    val_json = request.json
    print 'restful_get_version'

    return  call('restful_register_key', val_json)


# Run
if __name__ == '__main__':
    app.run(
        host = "0.0.0.0",
        port = 6000
    )
