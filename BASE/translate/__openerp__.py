# -*- coding: utf-8 -*-
{
    'name': 'FEOSCO: Translate to Vietnam all module core odoo',
    'version': '1.1',
    'author': 'Feosco',
    'category': 'Feosco',
    'website': 'http://www.feosco.com',
    'summary': 'Translate all module',
    'author': 'Feosco',
    'images': [
    ],
    'depends': [
        'base',
        'feosco_web_adblock',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
    'css': [],
}

