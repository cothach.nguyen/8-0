{
    'name': 'FEOSCO Demo Data',
    'version': '1.1',
    'author': 'Feosco',
    'category': 'Feosco',
    'sequence': 21,
    'website': 'http://www.feosco.com',
    'description': """
        Feosco demo data
    """,
    'author': 'Feosco',
    'depends': ['point_of_sale'],
    'data': [
        'datas/product_demo.xml',
        'datas/hr_demo.xml',
        'datas/res_partner_demo.xml',
    ],
}
