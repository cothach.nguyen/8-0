# -*- coding: utf-8 -*-
{
    'name': 'FEOSCO Block Pop-Up Register from Odoo ',
    'version': '1.0',
    'author': 'Feosco',
    'category': 'Feosco',
    'website': 'http://www.feosco.com',
    'summary': 'Block Pop-Up Register from Odoo',
    'description': """
    Block Pop-Up Register from Odoo
    """,
    'author': 'Feosco',
    'website': 'http://www.feosco.com',
    'images': [
    ],
    'depends': ['web', 'mail'],
    'data': [
        'views/web_adblock.xml',
        'sql/update_mail_group.sql',
    ],
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

