# -*- encoding: utf-8 -*-
{
    "name": "FEOS: Assets Management Public Trial Key",
    "version": "1.0",
    "depends": [
        "feosco_account_asset",
        ],
    "author": "Feosco",
    "description": """
    Check Key register from www.feosco.com
    """,
    "website": "http://www.feosco.com",
    "category": "Feosco",

    "update_xml": [
    ],
    "auto_install": False,
    "installable": True,
    "application": False,
}


