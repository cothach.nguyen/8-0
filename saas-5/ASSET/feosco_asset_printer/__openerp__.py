# -*- encoding: utf-8 -*-
{
    "name": "FEOS: Assets Management",
    "version": "1.0",
    "depends": ["feosco_account_asset"],
    "author": "Feosco",
    "description": """Asset management.
    This Module manages the assets owned by a company or an individual. It will keep track of depreciation's occurred on
    those assets. And it allows to create Move's of the depreciation lines.
    """,
    "website": "http://www.feosco.com",
    "category": "Feosco",
    "init_xml": [
    ],
    "demo_xml": [

    ],
    'test': [

    ],
    "update_xml": [
        "views/feosco_template_view.xml"
    ],
    "auto_install": False,
    "installable": True,
    "application": False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

