#-*- coding:utf-8 -*-
from openerp.osv import osv, fields

class feosco_template_printer(osv.osv):

    _name = "feosco.template.printer"

    _columns = {
        'name': fields.char('File Name', size=128, required=True),
        'active': fields.boolean('Active'),
        'field_ids': fields.many2many('ir.model.fields', 'model_fields_template_printer_rel', 'fields_id', 'template_id', 'Label Printer', domain="[('model', '=', 'account.asset.asset')]")
    }

    _defaults = {
        'active': True,
    }

feosco_template_printer()