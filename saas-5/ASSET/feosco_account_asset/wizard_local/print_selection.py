#-*- coding:utf-8 -*-
from openerp.osv import osv, fields

class feosco_print_selection(osv.osv_memory):
    _name = "feosco.print.selection"

    _columns = {
        'asset_ids': fields.many2many('account.asset.asset', 'fesco_print_rel', 'mem_id', 'asset_id', u'Lựa chọn tài sản in', required=True),
        'state': fields.selection([('draft', u'Chưa in'), ('done', u'Đã xuất qua máy in')], u'Tình trạng kết nối máy in'),
        'print_type': fields.selection([('chopped', u'In cắt đoạn'), ('ligature', u'In liền đoạn')], u'Kiểu in', required=True)
    }
    _defaults = {
                 'state': 'draft',
                 'print_type':'ligature'
    }

    def act_print(self, cr, uid, ids, context={}):
        for this in self.browse(cr, uid, ids):
            print_ids = []
            asset_ids = this.asset_ids
            if asset_ids:
                for ass in asset_ids:
                    print_ids.append(ass.id)
                if print_ids:
                    self.pool.get('feosco.print.asset').record_asset_to_print(cr, uid, print_ids, this.print_type, context=context)
                    return self.write(cr, uid, [this.id], {'state': 'done'})
            else:
                return self.write(cr, uid, [this.id], {'state': 'done'})

feosco_print_selection()