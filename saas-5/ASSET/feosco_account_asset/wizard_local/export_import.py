#coding:utf-8
from openerp.osv import osv, fields
import logging
import base64
import os
from openerp.tools.translate import _
import datetime


class feosco_export_import(osv.osv_memory):

    _name = "feosco.export.import"

    __logger = logging.getLogger(_name)

    _columns = {
        'file': fields.binary(u'Dữ liệu excel'),
        'state': fields.selection([
            ('default', u'Chưa thực thi'),
            ('succed', u'Hoàn tất')],
                                  u'Trạng thái', readonly=True, states={'default': [('readonly', False)]}),
        'action': fields.selection([('import', u'Đưa dữ liệu vào'), ('export', u'Xuất dữ liệu ra')], u'Hành động', required=True, readonly=True, states={'default': [('readonly', False)]}),
        'excel_format': fields.char(u'Mẫu file nhập dữ liệu', readonly=True),
        'path_image': fields.char( u'Đường đẫn thư mục hình ảnh', size = 256),
        'output_type': fields.selection([
            ('all', u'Tất cả'),
            ('scan', u'Đã kiểm kê'),
            ('not_scan', u'Chưa kiểm kê'),
        ], u'Lựa chọn kiểu xuất dữ liệu'),
        'scan_time_id': fields.many2one('feosco.scan.time', u'Định kỳ'),
    }

    _defaults = {
        'output_type': 'all',
        'state': 'default',
        'action': 'import',
        'excel_format':'https://mega.co.nz/#!1xM3ADTL!gqoIWDn44kCt15yJTesWK2aDCfJEdWQCcRTjaANrG48'
    }

    def action_change(self, cr, uid, ids, action, context={}):
        if action:
            if action == 'import':
                return {
                    'value': {'name': 'Import.xls'}
                }
            else:
                return {
                    'value': {'name': 'Export.xls'}
                }

    def export_excel(self, cr, uid, ids, context={}):
        tool = self.pool.get('feosco.excel')
        asset_pool = self.pool.get('account.asset.asset')
        for wiz in self.browse(cr, uid, ids):
            label = [u'Tên TS', u'Danh mục', u'Giá nhập', u'Ngày nhập', u'Nhập từ', u'Tham chiếu', u'Người sử dụng', u'Phòng ban', u'Trạng thái sủ dụng']
            condition = []
            if wiz.output_type in ['scan', 'not_scan']:
                condition = [('feosco_scan_time_id', '=', wiz.scan_time_id.id)]
            if wiz.output_type == 'all':
                condition = []
            asset_ids = asset_pool.search(cr, uid, condition)
            if not asset_ids:
                raise osv.except_osv(_(u'Xuất dữ liệu thất bại'), _(u'không tìm thấy tài sản nào ứng với điều kiện đưa vào.'))
            list_tuple_data = []
            for asset in asset_pool.browse(cr, uid, asset_ids):
                tup = (asset.name, )

                if asset.category_id:
                    tup += (asset.category_id.name, )
                else:
                    tup += ('', )
                if asset.purchase_value:
                    tup += (asset.purchase_value, )
                else:
                    tup += ('',)
                if asset.purchase_date:
                    tup += (asset.purchase_date, )
                else:
                    tup += ('', )
                if asset.partner_id:
                    tup += (asset.partner_id.name, )
                else:
                    tup += ('',)
                if asset.feosco_code:
                    tup += (asset.feosco_code, )
                else:
                    tup += ('',)
                if asset.feosco_user_id:
                    tup += (asset.feosco_user_id.name, )
                else:
                    tup += ('',)
                if asset.feosco_asset_department_id:
                    tup += (asset.feosco_asset_department_id.name, )
                else:
                    tup += ('',)
                if asset.state == 'draft':
                    tup += (u'Chưa sử dụng', )
                if asset.state == 'open':
                    tup += (u'Đang sử dụng', )
                if asset.state == 'close':
                    tup += (u'Hư hỏng', )
                list_tuple_data.append(tup)

            attach_id = self.pool.get('ir.attachment').create(cr, uid,{
                'name': 'feosco_asset_management.xls',
                'datas': tool.export_data(label, list_tuple_data),
                'datas_fname': 'feosco_asset_management.xls',
                },
            context=context)
            self.__logger.info("===> END: export_data()")
            return {
                'type': 'ir.actions.act_window',
                'res_model': 'ir.attachment',
                'view_mode': 'form',
                'view_type': 'form',
                'res_id': attach_id,
                'views': [(False, 'form')],
                'target': 'new',
            }


    def _get_data(self, cr, uid, value):

        feosco_user_ids = self.pool.get('res.users').search(cr, uid, [('name', '=', value.get('feosco_user_id'))])
        categ_ids = self.pool.get('account.asset.category').search(cr, uid, [('name', '=', value.get('category_id'))])
        feosco_barcode_types = self.pool.get('feosco.barcode.type').search(cr, uid, [('name', '=', value.get('feosco_barcode_type'))])

        return {
            'category_id': categ_ids[0] if categ_ids else None,
            'feosco_barcode_type': feosco_barcode_types[0] if feosco_barcode_types else None,
            'feosco_user_id': feosco_user_ids[0] if feosco_user_ids else None,

            }



    def import_excel(self, cr, uid, ids, context={}):
        """
        ===================================================
        val[0]: Mã barcode
        val[1]: Tên
        val[2]: Serial Tham chiếu
        val[3]: Phòng ban (*)
        val[4]: Ngày mua về
        val[5]: Giá mua về
        val[6]: Kiểu định dạng barcode (*)
        val[7]: Người sử dụng (*)
        val[8]: Hình ảnh cần import (*)
        val[9]: Danh mục TS (*)
        ===================================================
        Code: barcode ex: 123456789
        name: Tên ex: Máy In Barcode
        category: danh mục TS ex: Hàng mua về
        Purchase value: giá mua vào ex: 100000000
        Purchase Date (YYYY-MM-DD): ngày nhập về 12-12-2014
        method_time: thời gian khấu hao ex: number
        method_number: số lần khấu hao ex: 10
        method_period: 3
        method: linear
        prorata:
        feosco_barcode_type: loại barcode ex: QR
        feosco_user_id: người sử dụng
        code: số seri ( tham chiếu )
        image: tên hình ảnh
        =================================================
        1. Mã barcode
        2. Tên TS
        3. Tham chiếu (code)
        4. Phòng ban.
        5. Ngày mua về
        6. Giá mua về.
        7. Định dạng barcode
        8. Trạng thái sử dụng
        9. Số lượng phân bổ.

        """
        self.__logger.info('===> begin import_excel')

        orm_excel = self.pool.get('feosco.excel')
        orm_asset = self.pool.get('account.asset.asset')
        orm_categ = self.pool.get('account.asset.category')
        orm_barcode = self.pool.get('feosco.barcode.type')
        orm_department = self.pool.get('feosco.asset.department')
        orm_user = self.pool.get('res.users')

        for wiz in self.browse(cr, uid, ids):

            path = os.path.dirname(os.path.abspath(__file__)) + "/data.xls"
            f = open(path, "w")
            try:
                strDecodeToByte = base64.b64decode(wiz.file)
                f.write(strDecodeToByte)
                f.close()
                self.__logger.info('...saving to: %s' % path)
            except Exception, ex:
                self.__logger.error('error: %s' % ex)


            dataDictReturn = orm_excel.import_data(path)

            self.__logger.info('...return sheet by sheet.....')
            index = 0
            for sheet, value in dataDictReturn.iteritems():
                del value[0]

                for val in value:

                    self.__logger.info('...merge data: %s and len %s' % (val, len(val)))
                    purchase_date = ''
                    try:
                        purchase_date = datetime.datetime.strptime(val[4], "%Y-%m-%d") if val[4] else None
                    except:
                        raise osv.except_osv(_(u'Lổi file'),_(u'Định dạng cột Ngày nhập về ko đúng, vui lòng kiểm tra lại mẫu import.'))
                    valNew = {
                        'feosco_code': val[0],
                        'name': val[1],
                        'code': val[2],
                        'purchase_date': purchase_date,
                        'purchase_value': val[5],
                        'state': 'open',
                        'feosco_qty': val[9] if val[9] else 1,
                    }
                    checkAsset_ids = orm_asset.search(cr, uid, [('feosco_code', '=', val[0])])
                    if not val[0]:
                        self.__logger.error(': Ma code khong duoc dua vao')
                        continue
                    if checkAsset_ids:
                        self.__logger.error(': TS da co trong he thong tuong ung vs ma code: %s' % val[0])
                        continue
                    else:
                        department_ids = orm_department.search(cr, uid, [('name', '=', val[3])])
                        if department_ids:
                            valNew.update({'feosco_asset_department_id': department_ids[0]})
                        else:
                            valNew.update({'feosco_asset_department_id': orm_department.create(cr, uid, {'name': val[3]})})
                        barcodeType_ids = orm_barcode.search(cr, uid, [('name', '=', val[6])])
                        if not barcodeType_ids:
                            barcodeType_ids = orm_barcode.search(cr, uid, [('name', '=', 'Code128')])
                            valNew.update({'feosco_barcode_type': barcodeType_ids[0]})
                        else:
                            valNew.update({'feosco_barcode_type': barcodeType_ids[0]})
                        if val[7]:
                            user_ids = orm_user.search(cr, uid, [('name', '=', val[7])])
                            if user_ids:
                                valNew.update({'feosco_user_id': user_ids[0]})
                            else:
                                valNew.update({'feosco_user_id': orm_user.create(cr, uid, {'name': val[7], 'login': val[7]})})
                        if val[8]:
                            categ_ids = orm_categ.search(cr, uid, [('name', '=', val[8])])
                            if categ_ids:
                                valNew.update({'category_id': categ_ids[0]})
                            else:
                                valNew.update({'category_id': orm_categ.search(cr, uid, [])[0] if orm_categ.search(cr, uid, []) else None})
                        else:
                            valNew.update({'category_id': orm_categ.search(cr, uid, [])[0] if orm_categ.search(cr, uid, []) else None})
                        # ============= remove image asset on server =================
                        # if val[8]:
                        #     try:
                        #         imagePath = wiz.path_image + "%s" % val[8] + ".png"
                        #         with open(imagePath, "rb") as image_file:
                        #             imageBaseString = base64.b64encode(image_file.read())
                        #             valNew.update({
                        #                'feosco_image': imageBaseString,
                        #             })
                        #     except:
                        #         raise osv.except_osv((u'Lỗi '),(u'Không tìm thấy thư mục chứa hình hoặc ko tìm thấy hình.'))
                        #=============================================================
                        if not valNew.has_key('purchase_date') or not valNew.get('purchase_date'):
                            valNew.update({'purchase_date': datetime.datetime.today()})
                        self.__logger.info('valNew: %s' % valNew)
                        orm_asset.create(cr, uid, valNew)
                        index += 1
            self.__logger.info('...insert total: [ %s ]' % index)
            self.write(cr, uid, [wiz.id], {'state': 'succed'})
            self.__logger.info('===> end import_excel')
            return {
                'type': 'ir.actions.act_window',
                'res_model': 'feosco.export.import',
                'view_mode': 'form',
                'view_type': 'form',
                'res_id': wiz.id,
                'views': [(False, 'form')],
                'target': 'new',
                'name': u'Import Hoàn Tất'
            }


feosco_export_import()