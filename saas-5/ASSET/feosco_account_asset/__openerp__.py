# -*- encoding: utf-8 -*-
{
    "name": "FEOS: Assets Management",
    "version": "1.0",
    "depends": [
        "account_asset",
        ],
    "author": "Feosco",
    "description": """Asset management.
    This Module manages the assets owned by a company or an individual. It will keep track of depreciation's occurred on
    those assets. And it allows to create Move's of the depreciation lines.
    """,
    "website": "http://www.feosco.com",
    "category": "Feosco",
    'images': ['images/hr_department.jpeg', 'images/hr_employee.jpeg','images/hr_job_position.jpeg'],
    "sequence": 32,
    "init_xml": [
    ],
    "demo_xml": [
        # "demo/asset_demo.xml",
    ],
    'test': [
    ],
    "update_xml": [
        'security/ir.model.access.csv',
        'datas/master_data.xml',
        'datas/asset_ref.xml',
        'views/asset_main_menu.xml',
        'views/asset_public_key_view.xml',
        'views/asset_view.xml',
        'views/asset_barcode_view.xml',
        'views/asset_report_view.xml',
        'views/asset_status.xml',
        'views/asset_print.xml',
        'views/asset_scan_view.xml',
        'views/asset_department.xml',
        'views/asset_scan_time.xml',
        'wizard_local/account_asset_print_all.xml',
        'wizard_local/feosco_wizard_asset_assign_view.xml',
        'wizard_local/export_import_view.xml',
        'wizard_local/print_selection_view.xml',
        'wizard_local/move_department.xml',
    ],
    "auto_install": False,
    "installable": True,
    "application": False,
}


