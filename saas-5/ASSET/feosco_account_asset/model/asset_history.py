#coding:utf-8

from openerp.osv import osv, fields


class feosco_asset_history(osv.osv):

    _name = 'feosco.asset.history'

    _columns = {
        'asset_id': fields.many2one('account.asset.asset', 'Asset', required=True),
        'user_id': fields.many2one('res.users', 'Users'),
        'create_uid': fields.many2one('res.users', 'User', readonly=True),
        'create_date': fields.datetime('Assign Date', readonly=True),
        'action': fields.char('Action', size=128, required=True),
    }
    _order = 'create_date desc'

feosco_asset_history()
