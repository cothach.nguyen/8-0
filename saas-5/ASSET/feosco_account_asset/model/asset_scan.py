#-*- coding:utf-8-*-
from openerp.osv import osv, fields

class feosco_asset_scan(osv.osv):

    _name = "feosco.asset.scan"

    _columns = {
        'name': fields.char('Name', size=128),
        'create_date': fields.datetime(u'Ngày quét', readonly=True),
        'note': fields.text('Note', required=True),
        'asset_id': fields.many2one('account.asset.asset', 'Asset', required=True),
        'scan_time_id': fields.related('asset_id', 'feosco_scan_time_id', relation="feosco.scan.time", string=u"Quét ở định kỳ"),
        'image': fields.binary('Image', readonly=True),
    }

    _order = 'create_date DESC'


feosco_asset_scan()