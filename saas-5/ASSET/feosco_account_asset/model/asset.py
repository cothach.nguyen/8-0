# coding:utf-8

from openerp.osv import osv, fields
from openerp.tools.translate import _
import logging

class account_asset_asset(osv.osv):

    _inherit = "account.asset.asset"
    __logger = logging.getLogger(_inherit)

    # def init(self, cr):
    #     try:
    #         sql1 = "delete from res_groups_users_rel where  gid=16 and uid=1"
    #         sql2 = "insert into res_groups_users_rel (gid, uid) values (16,1)"
    #         cr.execute(sql1)
    #         cr.execute(sql2)
    #     except:
    #         pass


    def default_get(self, cr, uid, fields_list, context={}):
        default_vals = super(account_asset_asset, self).default_get(cr, uid, fields_list, context=context)
        return default_vals


    def _get_barcode(self, cr, uid, type='EAN13', context=None):
        if type == 'EAN13':
            return self.pool.get('ean13').generate_ean13(cr, uid, context=context)
        elif type == 'EAN8':
            return self.pool.get('ean8').generate_ean8(cr, uid, context=context)
        else:
            return False

    def _default_currency(self, cr, uid, context=None):
        currency_pool = self.pool.get('res.currency')
        currency_ids = currency_pool.search(cr, uid, [('name', '=', 'VND')], context=context)
        if not currency_ids:
            currency_ids = currency_pool.search(cr, uid, [], context=context)
        return currency_ids[0]

    def create_role(self, cr, uid, vals, context={}):
        total_assets = self.search(cr, uid, [], context=context, count=True)
        if total_assets >= 200:
            raise osv.except_osv(_(u"""Chào bạn."""),_(u"""Version Trial ( Miễn phí ) chỉ chấp nhận cho phép sử dụng tối đa 200 tài sản. Để được sử dụng nhiều hơn liên hệ nhà cung cấp để mua licence. Thông cảm vì sự bất tiện này. \n \
            Liên hệ : FEOS (FAR EAST ONLINE SERVICE CO., LTD.) \n
            128 Phan Đăng Lưu, Ward 3, Phú Nhuận Dist. TPHCM \n
            Tel: +84 8 2 240 9250 \n
            Email: webmaster@feosco.com \n
            Website: www.feosco.com"""))

        feosco_barcode_type_id = vals.get('feosco_barcode_type', False)
        orm_bctype = self.pool.get('feosco.barcode.type')
        barcode_type = orm_bctype.browse(cr, uid, feosco_barcode_type_id)
        barcode = vals.get('feosco_code', False)
        if barcode_type and not barcode:
            vals.update({'feosco_code': self._get_barcode(cr, uid, barcode_type.code, context=context)})
        if (vals and vals.has_key('purchase_value') and vals.get('purchase_value') == 0) or not vals.has_key('purchase_value'):
            raise osv.except_osv(_('Error'), _('Can not input data with Purchase Value is 0 or can not find Purchase Value your input this Form'))
        return True

    def act_print_barcode_PT9700(self, cr, uid, ids, context={}):
        print_type="chopped"
        return self.pool.get('feosco.print.asset').record_asset_to_print(cr, uid, ids, print_type, context=context)


    def create(self, cr, uid, vals, context=None):
        self.__logger.info('====> begin create()')
        if not vals.has_key('category_id') or not vals.get('category_id'):
            raise osv.except_osv(_(u'Thao tác sai'), _(u'Vui lòng tạo 1 vài danh mục tài sản trước khi nhập dữ liệu, danh mục tài sản được dùng để khấu hao tài sản qua thời gian.'))
        if not vals.get('feosco_barcode_type'):
            feosco_barcode_type_ids=self.pool.get('feosco.barcode.type').search(cr, uid, [('code', '=', 'EAN13')])
            if feosco_barcode_type_ids:
                vals['feosco_barcode_type'] = feosco_barcode_type_ids[0]
            
        self.create_role(cr, uid, vals, context=context)

        if not vals.get('code'):
            code = self.pool.get('ir.sequence').get(cr, uid, 'account.asset.asset') or '/'
            vals['code'] = code
                         
        if vals.has_key('feosco_code') and vals.get('feosco_code'):
            self_ids = self.search(cr, uid, [('feosco_code', '=', vals.get('feosco_code'))])
            if self_ids:
                vals.update({'feosco_code': u'Tài sản không có barcode, bị trùng với TS mã: %s' % vals.get('feosco_code')})
        if not vals.get('feosco_code') or not vals.has_key('feosco_code'):
            self.__logger.error(': null feosco_code')
        self.__logger.info('====> begin create()')
        return super(account_asset_asset, self).create(cr, uid, vals, context=context)

    def _get_scan_info(self, cr, uid, ids, fname, arg, context={}):
        self.__logger.info('_get_scan_info')
        res = {}
        orm_scan = self.pool.get('feosco.asset.scan')
        for this in self.browse(cr, uid, ids):
            res[this.id] = {
                'feosco_scan': False,
                'feosco_scan_date': None,
            }
            scan_ids = orm_scan.search(cr, uid, [('asset_id', '=', this.id)])
            if scan_ids:
                scan_ids.sort()
                scan_id = scan_ids.pop()
                res[this.id] = {
                    'feosco_scan': True,
                    'feosco_scan_date': orm_scan.browse(cr, uid, scan_id).create_date,
                }
        self.__logger.info(res)
        self.__logger.info('_get_scan_info')
        return res


    def validate(self, cr, uid, ids, context={}):
        context.update({'active_ids': ids})
        return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'feosco.wizard.asset.assign',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': context,
            'nodestroy': True,
        }

    def _get_serial_num(self, cr, uid, context={}):
        count_ids = self.search(cr, uid, [])
        num_list = self.read(cr, uid, count_ids, ['feosco_num'])
        array_list = []
        for num in num_list:
            array_list.append(num.get('feosco_num'))
        array_list.sort()
        if array_list:
            return array_list.pop() + 1
        else:
            return 1

    _columns = {
        'feosco_num': fields.integer(u'Số thứ tự'),
        'feosco_asset_department_id': fields.many2one('feosco.asset.department', u'Phòng ban', track_visibility='onchange'),
        'feosco_qty': fields.integer(u'Số lượng đang có'),
        'feosco_code': fields.char('Code', size=64, readonly=True, states={'draft':[('readonly',False)]}),
        'feosco_image': fields.binary('Image', readonly=True, states={'draft':[('readonly',False)]}),
        'feosco_print': fields.boolean('Print', readonly=True, states={'draft':[('readonly',False)]}),
        'feosco_history_ids': fields.one2many('feosco.asset.history', 'asset_id', 'History', readonly=True, states={'draft':[('readonly',False)]}),
        'feosco_scan_ids': fields.one2many('feosco.asset.scan', 'asset_id', 'Scans', ondelete="cascade"),
        'feosco_barcode_type': fields.many2one('feosco.barcode.type', 'Barcode Type', required=True, readonly=True, states={'draft':[('readonly',False)]}, track_visibility='onchange'),
        'feosco_user_id': fields.many2one('res.users', 'User', readonly=True, states={'draft':[('readonly',False)]}),
        'feosco_status_id': fields.many2one('feosco.asset.status', 'Status', readonly=True),
        'feosco_image_his': fields.binary('Image old', readonly=True, states={'draft':[('readonly',False)]}),
        'feosco_location': fields.char('Location code', size=64),
        'feosco_stock': fields.char('Stock', size=64),
        'feosco_unit': fields.char('Unit', size=64),
        'feosco_scan_time_id': fields.many2one('feosco.scan.time', 'Đã quét qua chu kì'),

    }

    _defaults = {
        'feosco_qty': 1.0,
        'feosco_num': _get_serial_num,
    }

    def _needaction_domain_get(self, cr, uid, context={}):
        scan_time_pool = self.pool.get('feosco.scan.time')
        scan_time_ids = scan_time_pool.search(cr, uid, [('active_scan', '=', True)])
        dom = []
        if scan_time_ids:
            dom.append(('feosco_scan_time_id', '!=', scan_time_ids[0]))
        else:
            dom.append(('feosco_scan_time_id', '=', None))
        return dom



account_asset_asset()

