# -*- coding:utf-8 -*-
from openerp.osv import osv, fields

class feosco_move_department(osv.osv_memory):

    _name = "feosco.move.department"

    _columns = {
        'department_id': fields.many2one('feosco.asset.department', u'Chuyển sang phòng', required=True),
    }

    def moving(self, cr, uid, ids, context={}):
        if context.has_key('active_ids'):
            orm_asset = self.pool.get('account.account.asset')
            for wiz in self.browse(cr, uid, ids):
                depart_id = wiz.department_id.id
                orm_asset.write(cr, uid, context.get('active_ids'), {'feosco_asset_department_id': depart_id})
                return {'type': 'ir.actions.act_window_close'}
