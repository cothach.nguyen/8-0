# -*- coding: utf-8 -*-
from openerp.osv import osv, fields
from openerp.tools.translate import _
import logging
import os
import re, itertools
import json
import urllib2

url = 'http://123.30.109.227:6000/api/'




class feosco_key(osv.osv):

    _name = "feosco.key"
    __logger = logging.getLogger(_name)

    _columns = {
        'sequence': fields.char(u'Số đăng ký', size=128, states={'register':[('readonly', True)]}),
        'name': fields.char(u'Tên DN đăng ký', size=128, required=True, states={'register':[('readonly', True)]}),
        'mail': fields.char('Mail',  size=128, required=True, states={'register':[('readonly', True)]}),
        'phone': fields.char(u'Số Di Động',  size=128, required=True, states={'register':[('readonly', True)]}),
        'contact': fields.char(u'Người đăng ký',  size=128, required=True, states={'register':[('readonly', True)]}),
        'key': fields.text(u'Mã đăng ký', readonly=True, states={'register':[('readonly', True)]}),
        'state': fields.selection([('not_register', u'Chưa đăng ký'), ('register', u'Đăng ký thành công')], string=u'Tình trạng đăng ký'),
        'type': fields.char('Type', size=128, states={'register':[('readonly', True)]}),
        'path_key': fields.char('Path Key', size=256, states={'register':[('readonly', True)]}),
        #The ky add parameter 04.08.2014
        'account_num': fields.char(u'Mã số thuế', size=128, required=True, states={'register':[('readonly', True)]}),
        'website': fields.char(u'Trang web', size=128, states={'register':[('readonly', True)]}),
    }

    _defaults = {
        'state': 'not_register',
    }

    def _validate_phone(self, cr, uid, phone):
        regex=re.compile("([0-9]{10})|([0-9]{3}[-]{1}[0-9]{3}[-]{1}[0-9]{4})|([0-9]{3}[.]{1}[0-9]{3}[.]{1}[0-9]{4})|([\(]{1}[0-9]{3}[\)][ ]{1}[0-9]{3}[-]{1}[0-9]{4})|([0-9]{3}[-]{1}[0-9]{4})")
        chain = itertools.chain(*regex.findall(phone))
        if phone in list(chain):
            return True
        else:
            raise osv.except_osv(_(u'Chào bạn.'),_(u'Số điện thoại không hợp lệ'))

    def _validate_email(self, cr, uid, mail, context={}):
        if re.match(r"^[a-zA-Z0-9._]+\@[a-zA-Z0-9._]+\.[a-zA-Z]{3,}$", mail)!= None:
            return True
        else:
            raise osv.except_osv(_(u'Chào bạn.'),_(u'Địa chỉ email không hợp '))

    def create(self, cr, uid, vals, context={}):
        self_ids = self.search(cr, uid, [])
        if len(self_ids) >= 1:
            raise osv.except_osv(_(u'Chào bạn.'),_(u'Hệ thống chỉ chấp nhận 1 lần đăng ký duy nhất'))
        self._validate_email(cr, uid, vals.get('mail'))
        self._validate_phone(cr, uid, vals.get('phone'))
        return super(feosco_key, self).create(cr, uid, vals, context=context)

    def get_version(self, cr, uid, context={}):
        self_ids = self.search(cr, uid, [])
        lic = self.browse(cr, uid, self_ids[0])
        if lic.type in ['normal', 'enterprise']:
            return True
        else:
            return False



    #Action check register from Enterprise

    def action_check_register(self, cr, uid):

        key_ids = self.search(cr, uid, [('state', '=', 'register')])

        if not key_ids:
            raise osv.except_osv(_(u'Chào bạn.'),_(u'Vui lòng đăng ký sử dụng phần mềm với nhà cung cấp từ Menu Đăng Ký. Cám ơn đã sử dụng phần mềm.'))
        else:
            key = self.browse(cr, uid, key_ids[0])
            try:
                key_text = open(key.path_key, 'r').read()
                if key_text == key.key:
                    return True
                else:
                    raise osv.except_osv(_(u'Chào bạn.'),_(u'Bạn đã cố tình crack hệ thống phần mềm mà chúng tôi cung cấp, vui lòng tôn trọng bản quyền từ nhà cung cấp. Xin chào.'))
            except:
                raise osv.except_osv(_(u'Chào bạn.'),_(u'Bạn đã cố tình crack hệ thống phần mềm mà chúng tôi cung cấp, vui lòng tôn trọng bản quyền từ nhà cung cấp. Xin chào.'))



    #++++++++++++++++++++ CallRestful ++++++++++++++++++++++++++++

    def call_restful(self, url, jsonVals):
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        req = urllib2.Request(url, jsonVals, headers)
        response = urllib2.urlopen(req)
        return response.read()


    def restful_re_register(self, cr, uid, ids, context={}):
        self.__logger.info('--> BEGIN restful_re_register')
        vals = {}
        for this in self.browse(cr, uid, ids):
            if not this.sequence:
                raise osv.except_osv(_(u'Chào bạn.'),_(u'Vui lòng nhập lại mã đăng ký trước đây để chúng tôi kích hoạt lại.'))


            res= {}
            try:
                res = json.loads(self.call_restful(url + 'restful_re_register', json.dumps({'sequence': this.sequence})))
            except:
                raise osv.except_osv(_(u'Chào bạn.'),_(u'Vui lòng kết nối internet'))

            if res and res.get('code') == 1:
                key = res.get('key')
                message = res.get('message')
                sequence = res.get('sequence')

                path = os.getcwd()
                location_file = path + '/' + sequence + '.txt'
                file_ = open(location_file, 'w')
                file_.write(key)
                file_.close()
                res.update({'path_key': location_file, 'state': 'register'})
                self.__logger.info('--> END restful_re_register')
                return self.write(cr, uid, ids, res)
            if res and res.get('code') == 0:
                raise osv.except_osv(_(u'Chào bạn.'),_('%s' % res.get('message')))






    def restful_get_version(self, cr, uid, ids, context={}):

        self.__logger.info('--> BEGIN restful_get_version')
        for this in self.browse(cr, uid, ids):
            data_send = ''
            try:
                key_text = open(this.path_key, 'r').read()
                data_send = {'sequence': this.sequence, 'key': key_text}
            except:
                raise osv.except_osv(_(u'Chào bạn.'),_(u'Hệ thống không tìm thấy licence mà bạn đăng ký.'))
            if data_send:

                try:
                    val = self.call_restful(url+ 'restful_get_version', json.dumps(data_send))
                    return self.write(cr, uid, [this.id], {'type': val})
                except:
                    raise osv.except_osv(_(u'Chào bạn.'),_(u'Vui lòng kết nối internet'))
                finally:
                    self.__logger.info('--> END restful_get_version')


    def restful_register_key(self, cr, uid, ids, context={}):

        self.__logger.info('--> BEGIN restful_register_key')

        vals = {}
        for this in self.browse(cr, uid, ids):
            vals.update({
                'name': this.name,
                'mail': this.mail,
                'phone': this.phone,
                'contact': this.contact,
            })
        val = {}
        try:
            val = self.call_restful(url+ 'restful_register_key', json.dumps(vals))
        except:
            raise osv.except_osv(_(u'Chào bạn.'),_(u'Vui lòng kết nối internet'))
        if val:

            res = json.loads(val)
            key = res.get('key')
            type = res.get('type')
            sequence = res.get('sequence')
            code = res.get('code')
            message = res.get('message')
            if code == 1:
                path = os.getcwd()
                location_file = path + '/' + sequence + '.txt'
                file_ = open(location_file, 'w')
                file_.write(key)
                file_.close()
                self.__logger.info('--> END action_register_key')
                return self.write(cr, uid, ids, {'key': key, 'sequence': sequence, 'type': type, 'state': 'register', 'path_key': location_file})
            else:
                raise osv.except_osv(_(u'Chào bạn.'),_(u'Email này đã được đăng ký trước đó, vui lòng đăng ký email khác.'))

feosco_key()






