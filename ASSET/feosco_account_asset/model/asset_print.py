#-*- coding:utf-8 -*-
from openerp.osv import osv, fields


class feosco_print_asset(osv.osv):
    _name = "feosco.print.asset"
    
    _columns = {
                'create_uid': fields.many2one('res.users', 'Print User'),
                'asset_id': fields.many2one('account.asset.asset', 'Asset'),
                'print_type': fields.selection([('chopped', u'In cắt đoạn'), ('ligature', u'In liền đoạn')]),
                'company_id': fields.many2one('res.company', 'Company', required=True),
                }

    _defaults = {
        'company_id': lambda self, cr, uid, context: self.pool.get('res.company')._company_default_get(cr, uid, 'account.asset.category', context=context),
    }
    
    
    def record_asset_to_print(self, cr, uid, asset_ids, print_type, context=None):
        if asset_ids:
            # Check if this asset already to run by this users, if there, remove it
            search_args = [('create_uid','=',uid),
                               ('asset_id', 'in', asset_ids)]
            res_id = self.search(cr, uid, search_args, context=context)
            if res_id:
                existed_assets = self.read(cr, uid, res_id,['asset_id'])
                # print_assets_ids = [{'asset_id': (3, u'IPHONE 4S'), 'id': 1}, {'asset_id': (3, u'IPHONE 4S'), 'id': 2}]
                existed_asset_ids = [existed_asset_id['asset_id'][0] for existed_asset_id in existed_assets]
                new_asset_ids = filter(lambda x: x not in existed_asset_ids, asset_ids)
                asset_ids = new_asset_ids
            
            # If not exits, create to print
            for asset_id in asset_ids:
                self.create(cr, uid, {'asset_id': asset_id, 'print_type':print_type})
                
        return True