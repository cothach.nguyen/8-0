#-*- coding:utf-8 -*-

from openerp.osv import  osv
from openerp.tools.translate import _

class account_asset_category(osv.osv):

    _inherit = "account.asset.category"

    def _check_register(self, cr, uid, context={}):
        self_ids = self.search(cr, uid, [])
        if len(self_ids) > 3:
            raise osv.except_osv(_(u"""Chào bạn."""),_(u"""Version Trial ( Miễn phí ) chỉ chấp nhận cho phép sử dụng tối đa 200 tài sản, 3 Danh mục Tài Sản khấu hao, 1 địa điểm ( vùng miền ), 1 toà nhà trong địa điểm đó, 1 tầng trong địa điểm đó. Để được sử dụng nhiều hơn liên hệ nhà cung cấp để mua licence. Thông cảm vì sự bất tiện này. \n \
            Liên hệ : FEOS (FAR EAST ONLINE SERVICE CO., LTD.) \n
            128 Phan Đăng Lưu, Ward 3, Phú Nhuận Dist. TPHCM \n
            Tel: +84 8 2 240 9250 \n
            Email: webmaster@feosco.com \n
            Website: www.feosco.com"""))
        else:
            return True

    def create(self, cr, uid, vals, context=None):
        self._check_register(cr, uid, context=context)
        return super(account_asset_category, self).create(cr, uid, vals, context=context)


account_asset_category()