#-*- coding:utf-8 -*-
from openerp.osv import osv, fields

class feosco_asset_department(osv.osv):

    _name = "feosco.asset.department"

    _columns = {
        'name': fields.char(u'Tên phòng ban', size=128, required=True),
        'parent_id': fields.many2one('feosco.asset.department', u'Phòng ban cấp trên'),
        'code': fields.char(u'Mã phòng', size=128),
        'company_id': fields.many2one('res.company', 'Company', required=True),
    }

    _defaults = {
        'company_id': lambda self, cr, uid, context: self.pool.get('res.company')._company_default_get(cr, uid, 'account.asset.category', context=context),
    }

feosco_asset_department()