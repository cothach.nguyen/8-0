{
    "name": "FEOS: Asset Install All Module",
    "version": "1.0",
    "depends": [
        'feosco_account_asset',
        'feosco_account_asset_licence',
        'feosco_asset_printer',
    ],
    "author": "Feosco",
    "website": "http://www.feosco.com",
    "category": "Feosco",
    "auto_install": True,
}


