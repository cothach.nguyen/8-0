# -*- coding: utf-8 -*-
{
    'name': 'XMHL: Accounting',
    'version': '0.1',
    'author': 'FEOSCO',
    'category': 'FEOSCO',
    'website': 'http://www.feosco.com',
    'images': [],
    'depends': [
        'xmhl_contract',
    ],

    'data': [
        'wizard/report.xml',
        'views/menu_accounting.xml',
    ],
    'js': [],
    'qweb': [],
    'css': [],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}
