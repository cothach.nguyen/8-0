#-*- coding:utf-8 -*-

from openerp.osv import osv, fields
import logging

class feosco_accounting_report(osv.osv_memory):

    _name = "feosco.accounting.report"
    __logger = logging.getLogger(_name)
    _columns = {
        'name': fields.char(u'Tên file'),
        'state': fields.selection([
                                      ('draft', 'Chưa xuất dữ liệu'),
                                      ('done', 'Đã xuất thành công'),
        ], string=u'Trạng thái'),
    }

    _defaults = {'state': 'draft'}



    def exporting(self, cr, uid, ids, context={}):
        product_pool= self.pool.get('product.product')
        excel_pool = self.pool.get('feosco.excel')
        sale_pool = self.pool['sale.order']
        ir_attach = self.pool['ir.attachment']
        product_ids = product_pool.search(cr, uid, [('type', '!=', 'consu')])
        for wiz in self.browse(cr, uid, ids):
            self.__logger.info("===> BEGIN: exporting()")
            label = [
                u'Nhà phân phối', u'Đơn hàng', u'Sản phẩm', u'Giá bán', u'Đơn vị', u'Tổng'
            ]
            list_data = []
            pro_dict = {}
            for product in product_pool.browse(cr, uid, product_ids):
                label.append(product.name)
                pro_dict.update({
                    product.id: product.name
                })
            #customer
            sale_ids = sale_pool.search(cr, uid, [('state', 'not in', ['cancel', 'draft', 'sent', 'waiting_date'])])
            for sale in sale_pool.browse(cr, uid, sale_ids):
                tup = ()
                tup += (sale.partner_id.name, sale.name, )
                for line in sale.order_line:
                    tup += (line.product_id.name if line.product_id else '', line.price_unit, line.product_uom_qty,line.price_unit * line.product_uom_qty, )
                    for k, v in pro_dict.iteritems():
                        print k,v
                    list_data.append(tup)



            print label
            exel = excel_pool.export_data(label, list_data)
            filename = u'Báo cáo công nợ.xls',
            self.write(cr, uid, [wiz.id], {
                'name': filename,
                'state': 'done'
            })
            values = dict(
                name=u'Báo cáo công nợ.xls',
                datas_fname=u'Báo cáo công nợ.xls',
                res_model='ir.ui.view',
                type='binary',
                datas=exel,
            )

            attach_id = ir_attach.create(cr, uid, values, context=context)
            self.__logger.info("===> END: exporting()")
            return {
                'type': 'ir.actions.act_window',
                'res_model': 'ir.attachment',
                'view_mode': 'form',
                'view_type': 'form',
                'res_id': attach_id,
                'views': [(False, 'form')],
                'target': 'new',
                'name': filename,
            }

