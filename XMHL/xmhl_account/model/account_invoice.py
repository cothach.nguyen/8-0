#-*- coding:utf-8 -*-
from openerp.osv import osv, fields

class account_invoice(osv.Model):

    _inherit = "account.invoice"

    # def invoice_validate(self, cr, uid, ids, context={}):
    #     res = super(account_invoice, self).invoice_validate(cr, uid, ids, context=context)
    #     for this in self.browse(cr, uid, ids):
    #         sum_reward = 0
    #         sum_panalties = 0
    #         if this.feosco_contract_id:
    #             for product in this.feosco_contract_id.feosco_pro_service_ids:
    #                 sum_reward += product.list_price
    #             for product in this.feosco_contract_id.feosco_pro_reward_ids:
    #                 sum_reward += product.list_price
    #             for product in this.feosco_contract_id.feosco_pro_panalties_ids:
    #                 sum_panalties += product.list_price
    #             for product in this.feosco_contract_id.feosco_pro_support_ids:
    #                 sum_reward += product.list_price
    #         self.write(cr, uid, [this.id], {
    #             'feosco_reward': sum_reward,
    #             'feosco_penalties': sum_panalties,
    #             'residual': sum_reward - sum_panalties,
    #         })
    #     return res
    #
    #
    # def penalty_late_contract(self, cr, uid, ids, context={}):
    #     model, view_id = self.pool['ir.model.data'].get_object_reference(cr, uid, 'xmhl_contract', 'penalty_late_contract_form')
    #     if ids:
    #         context.update({'inv_id': ids[0]})
    #         return {
    #             'name': _(u'Phạt thanh toán'),
    #             'type': 'ir.actions.act_window',
    #             'view_type': 'form',
    #             'view_mode': 'form',
    #             'res_model': 'feosco.penalty.late.contract',
    #             'views': [(view_id, 'form')],
    #             'view_id': view_id,
    #             'target': 'current',
    #             'context': context,
    #         }
    #
    #
    #
    # def _get_contract_info(self, cr, uid, ids, name, args, context={}):
    #     res = {}
    #     DATETIME_FORMAT = "%Y-%m-%d"
    #
    #     for this in self.browse(cr, uid, ids):
    #         res[this.id] = {
    #             'feosco_type': '',
    #             'feosco_invoice_late': 0,
    #         }
    #         if this.feosco_penatly == True:
    #             res[this.id].update({'feosco_type': u'Hoá đơn phạt'})
    #             continue
    #         if this.payment_term and this.payment_term.line_ids and this.date_invoice:
    #
    #             from_dt = datetime.datetime.strptime(this.date_invoice, DATETIME_FORMAT)
    #             days = 0
    #             for line in this.payment_term.line_ids:
    #                 days += line.days
    #             to_dt = from_dt + datetime.timedelta(days=days)
    #             timedelta = to_dt - from_dt
    #             diff_day_contract = timedelta.days + float(timedelta.seconds) / 86400
    #             today = datetime.datetime.now()
    #             timedelta = today - from_dt
    #             diff_day_today = timedelta.days + float(timedelta.seconds) / 86400
    #             res[this.id] = {
    #                 'feosco_invoice_late': diff_day_contract - diff_day_today,
    #             }
    #             if diff_day_contract < diff_day_today:
    #                 res[this.id].update({'feosco_type': u'Trễ hạn'})
    #             else:
    #                 res[this.id].update({'feosco_type': u'Chưa vượt mức'})
    #
    #     return res

    _columns = {
        'feosco_contract_id': fields.many2one('account.analytic.account', string=u'Hợp đồng'),
        'feosco_reward': fields.float(u'Thưởng'),
        'feosco_penalties': fields.float(u'Phạt'),

    }

