# -*- coding: utf-8 -*-
{
    'name': 'XMHL: Accounting Report',
    'version': '0.1',
    'author': 'FEOSCO',
    'category': 'FEOSCO',
    'website': 'http://www.feosco.com',
    'images': [],
    'depends': ['base','account_followup','xmhl_account'],
    'data': [
        'report/account_followup_report.xml'
    ],
    'js': [],
    'qweb': [],
    'css': [],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}
