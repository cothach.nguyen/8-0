#-*- coding: utf-8 -*-
from openerp.osv import osv, fields
from openerp.tools.translate import _
import logging

class sale_order(osv.Model):

    _inherit = "sale.order"
    __logger = logging.getLogger(_inherit)

    _columns = {
        'project_id': fields.many2one('account.analytic.account', 'Contract / Analytic', readonly=True, \
            states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}, \
            help="The analytic account related to a sales order.", \
            domain="[('partner_id', '=', partner_id)]"),
    }


    def on_change_contract_id(self, cr, uid, ids, partner_id, contract_id, pricelist_id,context={}):
        self.__logger.info('Begin on_change_contract_id')
        res = {}
        orm_contract = self.pool.get('account.analytic.account')
        if ids:
            for this in self.browse(cr, uid, ids):
                res[this.id] = []
                data_return = []
                contract = orm_contract.browse(cr, uid, contract_id)
                if contract.recurring_invoice_line_ids:
                    for invoice_line in contract.recurring_invoice_line_ids:
                        data_return.append((0, 0, {
                            'product_id': invoice_line.product_id.id
                        }))
                    res[this.id] = data_return
        else:
            data_return = []
            contract = orm_contract.browse(cr, uid, contract_id)
            if contract.recurring_invoice_line_ids:
                for invoice_line in contract.recurring_invoice_line_ids:
                    sale_line_val = self.pool.get('sale.order.line').product_id_change(cr, uid, ids, pricelist_id, invoice_line.product_id.id, 0,
                        False, 0, False, '', partner_id,
                        False, True, False, False, False, False, None)
                    val = sale_line_val.get('value')
                    val.update({'product_id': invoice_line.product_id.id})
                    data_return.append((0, 0, val))

            self.__logger.info('end1 on_change_contract_id')
            return {'value': {'order_line': data_return}}
        self.__logger.info('end2 on_change_contract_id')
        return {'value': res}

    def onchange_partner_id(self, cr, uid, ids, part, context=None):

        res = super(sale_order, self).onchange_partner_id(cr, uid, ids, part, context=context)
        if part:
            orm_contract = self.pool.get('account.analytic.account')
            contract_ids = orm_contract.search(cr, uid, [('partner_id', '=', part), ('state', '=', 'open')])
            if not contract_ids:
                raise osv.except_osv(_('Error, Please create contract and submit it for this Customer before create Sale Order'), _(''))
            else:
                contract_id = contract_ids[0]
                res['value']['project_id'] = contract_id
                contract_obj = orm_contract.browse(cr, uid, contract_id, context=context)
                res['value']['payment_term'] = contract_obj.feosco_payment_term.id if contract_obj.feosco_payment_term else False
                
        return res



sale_order()

class sale_order_line(osv.Model):

    _inherit = "sale.order.line"
    _columns = {
        'product_id': fields.many2one('product.product', 'Product',
                                      domain=[('sale_ok', '=', True), ('type', '=', 'consu')],
                                      change_default=True, readonly=True, states={'draft': [('readonly', False)]}, ondelete='restrict'),
    }



