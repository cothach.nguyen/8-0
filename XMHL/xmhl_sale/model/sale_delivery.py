#coding: utf-8

from openerp.osv import osv, fields

class feosco_sale_delivery_type(osv.Model):

    _name = "feosco.sale.delivery.type"

    _columns = {
        'name': fields.char('Name', required=True),
    }

class feosco_sale_delivery(osv.Model):

    _name = "feosco.sale.delivery"

    _columns = {
        'name': fields.char('Name', required=True),
        'type_id': fields.many2one('feosco.sale.delivery.type', 'Type', required=True),
    }