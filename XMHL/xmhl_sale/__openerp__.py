# -*- coding: utf-8 -*-
{
    'name': 'XMHL: Sale Management',
    'version': '0.1',
    'author': 'FEOSCO',
    'category': 'FEOSCO',
    'website': 'http://www.feosco.com',
    'images': [],
    'depends': [
        'base',
        'sale',
        'xmhl_product',
        'xmhl_contract',
    ],

    'data': [
        'security/ir.model.access.csv',
        'views/sale_order_view.xml',
        # 'report/report_sale_policy_view.xml',
        # 'report/report_sale_delivery_view.xml',
        # 'report/sale_report_view.xml'
    ],
    'js': [],
    'qweb': [],
    'css': [],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}
