#coding: utf-8

from openerp.osv import osv, fields

class report_sale_policy(osv.Model):

    _name = "feosco.policy.report"
    _auto = False
    
    _columns = {
        'customer':fields.char('Customer name'),
        'contract_name': fields.char('Contract name'),
        'product_name': fields.char('Product name'),
        'support_value_total':fields.float('Support value total'),
        'received_value_total':fields.float('Received value total'),
        'residual_value':fields.float('Residual value'),
    }
    
    def init(self, cr):
        cr.execute("""
            create or replace view feosco_policy_report as 
            (
                 SELECT p.name as customer,
                        a.name as contract_name, 
                        pd.name_template as product_name,-- new
                        sum(f.price_unit*f.quantity*-1) as support_value_total,
                        RP.received_value_total,
                        (sum(f.price_unit*f.quantity*-1)- RP.received_value_total) as residual_value,
                        row_number() OVER () as id                    
                        
                FROM account_analytic_account a 
                      join  feosco_contract_support_policy f on a.id=f.analytic_account_id
                      join product_product pd on f.product_id=pd.id
                      join (SELECT project_id, partner_id FROM sale_order GROUP BY project_id, partner_id) 
                                as s on s.project_id=a.id 
                      join res_partner p on p.id=s.partner_id 
                      join (SELECT s_r.project_id, sl_r.product_id,sum(sl_r.product_uom_qty*sl_r.price_unit*-1) 
                                        as received_value_total
                            FROM sale_order_line sl_r, sale_order s_r
                            WHERE s_r.id= sl_r.order_id and sl_r.state !='draft' and sl_r.price_unit<0
                            GROUP BY s_r.project_id,sl_r.product_id) as RP on s.project_id=RP.project_id 
                                        and f.product_id=RP.product_id
      
                    GROUP BY p.name, a.name,RP.received_value_total, s.project_id, pd.name_template

           )
           """)
        
report_sale_policy()
