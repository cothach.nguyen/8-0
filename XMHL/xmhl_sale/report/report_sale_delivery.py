#coding: utf-8

from openerp.osv import osv, fields

class report_sale_delivery(osv.Model):

    _name = "feosco.delivery.report"
    _auto = False
    
    _columns = {
        'date_delivery':fields.date('Date delivery'),
        'vehicle': fields.char('Vehicle'),
        'driver':fields.char('License plate'),
        'volume':fields.float('Volume'),
        'amount': fields.float('Amount'),
    }
    
    def init(self, cr):
        cr.execute("""
            create or replace view feosco_delivery_report as 
            ( 
                SELECT to_date(to_char(sv.date,'YYYY-MM-DD'),'YYYY-MM-DD') as date_delivery, 
                           f.name as vehicle, 
                           f.license_plate driver,
                           sum(pu.feosco_converted_to_kg * sv.product_uom_qty) as volume,
                           sum(sv.product_uom_qty*pt.list_price) as amount,
                           fu.ids as id
        
                FROM feosco_stock_vehicle f, stock_picking s, stock_move sv, product_uom pu, product_template pt,

                        (SELECT to_number((to_char(f.id, '999999999999') || to_char(sv.date, 'YYMMDD')), '99G999D9S') as ids 
                         FROM feosco_stock_vehicle f, stock_move sv, stock_picking s
                         WHERE f.id=s.carrier_id and sv.picking_id=s.id
                         GROUP BY ids) as fu

                WHERE f.id=s.carrier_id and sv.picking_id=s.id and sv.state = 'done'
                        and pu.id=sv.product_uom and pt.id=sv.product_id 
                        and fu.ids=to_number((to_char(f.id, '999999999999') || to_char(sv.date, 'YYMMDD')),'99G999D9S')
        
                GROUP BY date_delivery, f.name, f.license_plate, fu.ids
           )
           """)
        
report_sale_delivery()
