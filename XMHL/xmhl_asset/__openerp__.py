# -*- coding: utf-8 -*-
{
    'name': 'XMHL: Asset Management',
    'version': '0.1',
    'author': 'FEOSCO',
    'category': 'FEOSCO',
    'website': 'http://www.feosco.com',
    'images': [],
    'depends': ['base','account_asset', 'xmhl_base'],
    'data': [
        'views/account_asset_view.xml',
        'data/sequence.xml'
    ],
    'js': [],
    'qweb': [],
    'css': [],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}
