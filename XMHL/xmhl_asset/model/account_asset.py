#-*- coding: utf-8 -*-

from openerp.osv import fields, osv


class account_asset(osv.osv):
    _inherit = "account.asset.asset"
    _columns = {
        'feosco_delivery': fields.selection([('officer', u'Văn Phòng'), ('delivery', u'Vận Chuyển')], u'Loại'),
    }

    _defaults = {
        'feosco_delivery': 'officer',
    }

    def _get_sequence(self, cr, uid, context=None):
        obj_seq = self.pool.get('ir.sequence')
        args_search = [('code', '=', 'account.asset.asset')]
        sequence_ids = obj_seq.search(cr, uid, args_search, context=context)

        return obj_seq.next_by_id(cr, uid, sequence_ids[0], context=context)

    def create(self, cr, uid, vals, context=None):
        vals['code'] = self._get_sequence(cr, uid, context)
        return super(account_asset, self).create(cr, uid, vals, context=context)


account_asset()
