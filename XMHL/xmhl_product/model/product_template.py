# -*- coding: utf-8 -*-

from openerp.osv import fields, osv

class product_template(osv.osv):
    _inherit = "product.template"

    _columns = {
        'feosco_quality_type_id': fields.many2one('feosco.master.data', u'Package Type',
                                                  domain="[('type', '=', 'product_package_type')]"),
        'type': fields.selection([
                                     ('consu', 'Consumable'),
                                     ('service','Service'),
                                     ('reward','Reward'),
                                     ('panalties','Panalties'),
                                     ('support','Support'),
                                 ],
                                 'Product Type', required=True,
                                 help="Consumable are product where you don't manage stock, a service is a non-material product provided by a company or an individual.")
    }