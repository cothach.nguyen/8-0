__author__ = 'thanhchatvn'
# -*- coding: utf-8 -*-

from openerp.osv import fields, osv


class feosco_product(osv.osv):
    _inherit = "product.product"


    def _get_sequence(self, cr, uid, context=None):
        obj_seq = self.pool.get('ir.sequence')
        args_search = [('code', '=', 'product.product')]
        sequence_ids = obj_seq.search(cr, uid, args_search, context=context)

        return obj_seq.next_by_id(cr, uid, sequence_ids[0], context=context)

    def _get_ean13_code(self, cr, uid, context=None):
        obj_seq = self.pool.get('ean13')
        return obj_seq.generate_ean13(cr, uid, context=context)

    def create(self, cr, uid, vals, context=None):
        vals['default_code'] = self._get_sequence(cr, uid, context)
        vals['ean13'] = self._get_ean13_code(cr, uid, context=context)
        return super(feosco_product, self).create(cr, uid, vals, context=context)


feosco_product()
