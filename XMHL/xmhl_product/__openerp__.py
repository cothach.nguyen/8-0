# -*- coding: utf-8 -*-
{
    'name': 'XMHL: Product Management',
    'version': '0.1',
    'author': 'FEOSCO',
    'category': 'FEOSCO',
    'website': 'http://www.feosco.com',
    'images': [],
    'depends': [
        'base',
        'product',
        'xmhl_base',
        'account'
    ],
    'data': [
        'views/product_view.xml',
        'views/product_uom_view.xml',
        'views/product_template_view.xml',
        'data/sequence.xml',
        'data/product_data.xml',
        'data/sequence.xml'
    ],
    'js': [],
    'qweb': [],
    'css': [],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}
