# -*- coding: utf-8 -*-
{
    'name': 'XMHL: Kernel Module',
    'version': '0.1',
    'author': 'FEOSCO',
    'category': 'FEOSCO',
    'website': 'http://www.feosco.com',
    'images': [],
    'depends': ['base', 'account', 'feosco_base', 'feosco_web_adblock'],
    'data': [
        'security/ir.model.access.csv',
        'data/group_data.xml',
        'data/user_data.xml',
        'view/main_menu.xml',
        'view/company.xml',
    ],
    'installable': True,
    'auto_install': False,
}
