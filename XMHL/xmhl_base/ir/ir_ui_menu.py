# coding: utf-8

from openerp.osv import osv, fields

class ir_ui_menu(osv.osv):

    _inherit = 'ir.ui.menu'

    _columns = {
        'feosco_active': fields.boolean('Active')
    }

    _default = {
        'feosco_active': False
    }

