# coding: utf-8

from openerp.osv import osv, fields

class feosco_res_company_branch(osv.Model):

    _name = "feosco.res.company.branch"

    _columns = {
        'name': fields.char('Name', required=True),
        'address': fields.char('Address', required=True),
        'city_id': fields.many2one('feosco.city', 'City', required=True),
        'district_id': fields.many2one('feosco.district', 'District',  domain="[('city_id', '=', city_id)]"),
        'tel': fields.char('Tel'),
        'fax': fields.char('Fax'),
        'owner_id': fields.many2one('res.users', 'Owner'),
        'company_id': fields.many2one('res.company', 'Company', required=True),
    }

class res_company(osv.Model):

    _inherit = "res.company"

    _columns = {
        'city': fields.many2one('feosco.city', 'City'),
        'district_id': fields.many2one('feosco.district', 'District', domain="[('city_id', '=', city)]"),
        'feosco_branch_ids': fields.one2many('feosco.res.company.branch', 'company_id', 'Branch'),
        'feosco_account_date': fields.date('Aprroved Date'),
    }