# -*- coding: utf-8 -*-

{
    'name': 'XMHL: Install All Module',
    'version': '1.1',
    'author': 'Feosco',
    'category': 'Feosco',
    'sequence': 21,
    'website': 'http://www.feosco.com',
    'summary': 'Install all module',
    'description': """

    """,
    'author': 'Feosco',
    'website': 'http://www.feosco.com',
    'images': [
    ],
    'depends': [
        'sale_margin',
        'feosco_base',
        'feosco_web_adblock',
        'xmhl_base',
        'xmhl_contract',
        'xmhl_product',
        'xmhl_product_margin',
        'xmhl_sale',
        'translate',
    ],
    'data': [
        
        
    ],
    'demo': [],
    'test': [
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
    'css': [],
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
