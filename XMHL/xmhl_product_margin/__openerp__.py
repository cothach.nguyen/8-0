# -*- coding: utf-8 -*-
{
    'name': 'XMHL: Product Margin Management',
    'version': '0.1',
    'author': 'FEOSCO',
    'category': 'FEOSCO',
    'website': 'http://www.feosco.com',
    'images': [],
    'depends': [
        'base',
        'xmhl_base',
        'product_margin',
    ],

    'data': [
       
    ],
    'js': [],
    'qweb': [],
    'css': [],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}
