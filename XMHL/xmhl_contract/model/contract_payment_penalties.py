#coding: utf-8
from openerp.osv import osv, fields

class feosco_contract_payment_penalties(osv.Model):

    _name = "feosco.contract.payment.penalties"

    _columns = {
        'name': fields.char('Name', size=128, required=True),
        'unit': fields.integer('Unit', required=True),
        'time': fields.selection([('day', 'Days'), ('week', 'Week'), ('month', 'Month'), ('year', 'Year')], 'Time', required=True),
        'penalties': fields.float('Penalties', required=True),
        'uom_id': fields.many2one('product.uom', 'Uom', required=True),
    }