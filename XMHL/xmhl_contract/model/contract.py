# -*- coding: utf-8 -*-

from openerp.osv import fields, osv
import logging

class account_analytic_account(osv.Model):

    _inherit = "account.analytic.account"
    __logger = logging.getLogger(_inherit)


    def _invoice_count(self, cr, uid, ids, field_name, arg, context=None):
        self.__logger.info('Begin _invoice_count')
        res = dict(map(lambda x: (x, 0), ids))

        for contract in self.browse(cr, uid, ids, context):
            res[contract.id] = {
                'feosco_invoice_count': len(contract.feosco_invoice_ids),
                'feosco_sale_count': len(contract.feosco_sale_ids),
                'feosco_invoice_paid': 0.00,
                'feosco_invoice_debt': 0.00,

            }
            paid = 0
            debt = 0
            if contract.feosco_invoice_ids:
                for inv in contract.feosco_invoice_ids:

                    if inv.residual == 0:
                        paid += 0
                        debt += inv.amount_total
                    else:
                        paid += inv.amount_total - inv.residual
                        debt += inv.residual

            res[contract.id].update({
                'feosco_invoice_paid': paid,
                'feosco_invoice_debt': debt,

            })
        self.__logger.info(res)
        self.__logger.info('End _invoice_count')
        return res

    _columns = {
        'partner_id': fields.many2one('res.partner', 'Customer', required=True, domain="[('customer', '=', True)]"),
        'street': fields.related('partner_id', 'street', type='char', string='House No.'),
        'street2': fields.related('partner_id', 'street2', type='char', string='Address'),
        'mobile': fields.related('partner_id', 'mobile', type='char', string='Mobile'),
        'feosco_business_license': fields.related('partner_id', 'feosco_business_license', type='char', string='Business Lic'),
        'feosco_account_num': fields.related('partner_id', 'feosco_account_num', type='char', string='Account Num'),
        'feosco_payment_term': fields.many2one('account.payment.term', 'Payment Term', required=True),
        'feosco_minimum_mass_consumption_ids': fields.one2many('feosco.contract.mass.consumption', 'contract_id', 'Minimum Mass Consumption'),
        'feosco_debt_limitation': fields.float('Debt Limitation', help=u"Hạn mức nợ tối đa cho phép đối với NPP này", required=True),
        'feosco_support_policy_ids': fields.one2many('feosco.contract.support.policy', 'analytic_account_id', 'Support Policy'),
        'feosco_payment_penalties_ids': fields.many2many('feosco.contract.payment.penalties', 'analytic_account_payment_penalties_rel', 'contract_id', 'penalties_id'),
        'feosco_is_company': fields.boolean('Is company'),
        'feosco_invoice_count': fields.function(_invoice_count, string=u'Hoá đơn', type='integer', multi="_get_account_invoice"),
        'feosco_invoice_paid': fields.function(_invoice_count, string=u'Thanh toán', type='float', multi="_get_account_invoice"),
        'feosco_invoice_debt': fields.function(_invoice_count, string=u'Đang Nợ', type='float', multi="_get_account_invoice"),
        'feosco_invoice_ids': fields.one2many('account.invoice', 'feosco_contract_id', string=u'Hoá đơn'),
        'feosco_sale_ids': fields.one2many('sale.order', 'project_id', string=u'Đơn hàng'),
        'feosco_sale_count': fields.function(_invoice_count, string=u'Đơn hàng', type='integer', multi="_get_account_invoice"),

        'feosco_pro_consu_ids': fields.many2many('product.product', 'feosco_pro_consu_rel', 'p_id', 'c_id', string=u'Bán', domain="[('type', '=', 'consu')]"),
        'feosco_pro_service_ids': fields.many2many('product.product', 'feosco_pro_service_rel', 'p_id', 'c_id', string=u'Dịch vụ', domain="[('type', '=', 'service')]"),
        'feosco_pro_reward_ids': fields.many2many('product.product', 'feosco_pro_reward_rel', 'p_id', 'c_id', string=u'Thưởng', domain="[('type', '=', 'reward')]"),
        'feosco_pro_panalties_ids': fields.many2many('product.product', 'feosco_pro_panalties_rel', 'p_id', 'c_id', string=u'Phạt', domain="[('type', '=', 'panalties')]"),
        'feosco_pro_support_ids': fields.many2many('product.product', 'feosco_pro_support_rel', 'p_id', 'c_id', string=u'Hỗ trợ', domain="[('type', '=', 'support')]"),
    }

    def event_change_partner(self, cr, uid, ids, partner_id, context={}):
        if partner_id:
            feosco_is_company = self.pool.get('res.partner').browse(cr, uid, partner_id).is_company
            return {'value': {'feosco_is_company': feosco_is_company}}

    def act_create_sale_order(self, cr, uid, ids, context={}):

        dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'sale', 'view_order_form')
        if not context:
            context = {}
        context.update({'contract_id': ids[0]})
        return {
            'name': u'Tạo đơn hàng',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'sale.order',
            'views': [(view_id, 'form')],
            'view_id': view_id,
            'target': 'current',
            'context': context,
        }


account_analytic_account()