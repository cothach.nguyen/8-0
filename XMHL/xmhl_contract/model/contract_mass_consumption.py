# -*- coding: utf-8 -*-

from openerp.osv import fields, osv

class feosco_contract_mass_consumption(osv.osv):
    _name = "feosco.contract.mass.consumption"

    _columns = {
        'package_type_id': fields.many2one('feosco.master.data', u'Package Type', required=True, domain="[('type', '=', 'product_package_type')]"),
        'contract_id': fields.many2one('account.analytic.account', u'Contract'),
        'min_volume': fields.float('Minimum Volume'),
        'measure_id': fields.many2one('product.uom', u'Unit of Measure'),
    }

