# coding: utf-8

from openerp.osv import osv, fields

class feosco_contract_debt_limit(osv.Model):

    _name = "feosco.contract.debt.limit"

    _columns = {
        'name': fields.selection([('before', 'Before'), ('after', 'After')], 'Type', required=True),
        'rule_ids': fields.one2many('feosco.contract.debt.limit.rule', 'debt_limit_id', 'Rule', required=True),
    }

class feosco_contract_debt_limit_rule(osv.Model):

    _name = "feosco.contract.debt.limit.rule"

    _columns = {
        'categ_id': fields.many2one('product.category', 'Category', required=True),
        'limit': fields.float('Limit', required=True),
        'debt_limit_id': fields.many2one('feosco.contract.debt.limit', 'Debt Limit', required=True),
    }