# -*- coding: utf-8 -*-
from openerp.osv import osv
import logging
from openerp.tools.translate import _

class sale_order(osv.Model):

    _inherit = "sale.order"
    __logger = logging.getLogger(_inherit)

    _columns = {

    }

    def action_button_confirm(self, cr, uid, ids, context=None):
        self.__logger.info('Begin action_button_confirm')
        orm_contract = self.pool.get('account.analytic.account')
        for sale in self.browse(cr, uid, ids):
            if sale.project_id:
                amount_total = sale.amount_total
                contract_info = orm_contract._invoice_count(cr, uid, [sale.project_id.id], None, None, context=context)
                sum_debt = contract_info.get(sale.project_id.id).get('feosco_invoice_debt')
                limit = sale.project_id.feosco_debt_limitation
                if amount_total > (limit - sum_debt):
                    raise osv.except_osv(_('Lổi lập đơn bán hàng'), _(u'Giới hạn công nợ vượt mức tối đa cam kết trong Hợp Đồng, không thể tạo đơn bán hàng tiếp theo dành cho nhà phân phối này'))
        res = super(sale_order, self).action_button_confirm(cr, uid, ids, context=context)
        self.__logger.info('End action_button_confirm')
        return res

    def _make_invoice(self, cr, uid, order, lines, context=None):
        self.__logger.info('Begin _make_invoice')
        self.__logger.info('order --> %s' % order)
        orm_contract = self.pool.get('account.invoice')
        inv_id = super(sale_order, self)._make_invoice(cr, uid, order, lines, context=context)
        orm_contract.write(cr, uid, inv_id, {'feosco_contract_id': order.project_id.id if order.project_id else None})
        self.__logger.info('End _make_invoice')
        return inv_id

    def default_get(self, cr, uid, fields, context=None):
        self.__logger.info('Begin default_get')
        res = super(sale_order, self).default_get(cr, uid, fields, context=context)
        if context and context.has_key('contract_id'):
            orm_contract = self.pool.get('account.analytic.account')
            contract = orm_contract.browse(cr, uid, context.get('contract_id'))
            res.update({
                'partner_id': contract.partner_id.id if contract.partner_id else None,
                'project_id': contract.id,
                'payment_term': contract.feosco_payment_term.id if contract.feosco_payment_term else None,
            })
        self.__logger.info('End default_get')
        return res

