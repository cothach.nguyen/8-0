# coding: utf-8

from openerp.osv import osv, fields

class feosco_contract_payment(osv.Model):

    _name = "feosco.contract.payment"

    _columns = {
        'name': fields.char('Name', size=64, required=True),
        'payment_type': fields.selection([('before', 'Before'), ('after', 'After')], 'Payment Type', required=True),
        'rule_ids': fields.one2many('feosco.contract.payment.rule', 'payment_id', 'Rule'),
    }



class feosco_contract_payment_rule(osv.Model):

    _name = "feosco.contract.payment.rule"

    _columns = {
        'payment_id': fields.many2one('feosco.contract.payment', 'Payment', required=True),
        'categ_id': fields.many2one('product.category', 'Category', required=True),
        'deadline': fields.integer('DeadLine (days)', required=True),
    }



