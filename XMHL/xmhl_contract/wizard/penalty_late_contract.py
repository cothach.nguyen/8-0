#-*- coding:utf-8 -*-
from openerp.osv import osv, fields
import logging
from openerp.tools.translate import _

class feosco_penalty_late_contract(osv.osv_memory):

    def default_get(self, cr, uid, fields, context={}):
        self.__logger.info('Begin default_get')
        res = super(feosco_penalty_late_contract, self).default_get(cr, uid, fields, context=context)
        if context.has_key('inv_id') and context.get('inv_id'):
            orm_invoice = self.pool.get('account.invoice')
            obj = orm_invoice.browse(cr, uid, context.get('inv_id'))
            res.update({'invoice_id': obj.id})
            invoice_line = []
            for inv_line in obj.invoice_line:
                if not inv_line.product_id:
                    raise osv.except_osv(_(u'Lổi'), _(u'Đây là hoá đơn không có sản phẩm, không thể thực hiện phạt được'))
                invoice_line.append((0, 6, {
                    'product_from_id': inv_line.product_id.id,
                    'invoice_line_id': inv_line.id,
                    'quantity': inv_line.quantity,
                    'uos_id': inv_line.uos_id.id,
                }))
            if invoice_line:
                res.update({
                    'invoice_line': invoice_line
                })
        self.__logger.info(res)
        self.__logger.info('End default_get')
        return res

    _name = "feosco.penalty.late.contract"

    __logger = logging.getLogger(_name)

    _columns = {
        'invoice_id': fields.many2one('account.invoice', u'Hoá đơn bị phạt', required=True),
        'invoice_line': fields.one2many('feosco.penalty.late.contract.line', 'parent_id', string=u'Phạt trên sản phẩm'),
        'state': fields.selection([('draft', u'Tính tiền'), ('done', u'Hoàn tất')], string=u'Trạng thái')
    }

    _defaults = {
        'state': 'draft',
    }



    def compute(self, cr, uid, ids, context={}):
        self.__logger.info('Begin compute')
        orm_line = self.pool.get('feosco.penalty.late.contract.line')
        for line in self.browse(cr, uid, ids).invoice_line:
            if not line.product_from_id:
                raise osv.except_osv(_(u'Lổi'), _(u'Đây là hoá đơn không có sản phẩm, không thể thực hiện phạt được'))
            if not line.product_to_id:
                raise osv.except_osv(_(u'Lổi'), _(u'Vui lòng chọn chính sách phạt cho %s' % line.product_from_id.name))
            if not line.quantity:
                raise osv.except_osv(_(u'Lổi'), _(u'Chưa chọn số lượng cho %s' % line.product_from_id.name))
            quantity = line.invoice_line_id.quantity
            price_unit = line.product_to_id.list_price
            amount = quantity * price_unit
            tax_total = 0
            for tax in line.tax:
                tax_total += tax.amount
            discount = amount * line.discount / 100 if line.discount else 0.00
            after_discount = amount - discount
            sum_tax = after_discount * tax_total
            sum = after_discount + sum_tax
            orm_line.write(cr, uid, [line.id], {
                'sum': sum,
                'price_unit': price_unit,
            })
        self.__logger.info('End compute')
        return self.write(cr, uid, ids, {'state': 'done'}, context=context)


    def push_new_account_invoice(self, cr, uid, ids, context={}):
        inv_new_id = None

        for wiz in self.browse(cr, uid, ids):

            inv_new_id = self.pool.get('account.invoice').copy(cr, uid, wiz.invoice_id.id, {
                'feosco_penatly': True,
                'invoice_line': [],
                'origin': wiz.invoice_id.number,
                'payment_term': None,
                'name': u'Phạt [%s]' % wiz.invoice_id.number,
            })
            for wiz_line in wiz.invoice_line:
                new_line_id = self.pool.get('account.invoice.line').create(cr, uid, {
                    'name': wiz_line.product_to_id.name,
                    'origin': wiz.invoice_id.number,
                    'invoice_id': inv_new_id,
                    'product_id': wiz_line.product_to_id.id,
                    'uos_id': wiz_line.uos_id.id,
                    'price_unit': wiz_line.price_unit,
                    'price_subtotal': wiz_line.amount,
                    'discount': wiz_line.discount,
                    'quantity': wiz_line.quantity,
                })
                for tax in wiz_line.tax:
                    sql = "INSERT INTO account_invoice_line_tax (invoice_line_id, tax_id) VALUES (%s, %s)" % (new_line_id, tax.id)
                    cr.execute(sql)


        model, view_id = self.pool['ir.model.data'].get_object_reference(cr, uid, 'account', 'invoice_form')

        return {
            'name': _(u'Hoá đơn phạt'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'account.invoice',
            'views': [(view_id, 'form')],
            'view_id': view_id,
            'res_id': inv_new_id,
            'target': 'current',
            'context': context,
        }

feosco_penalty_late_contract()

class feosco_penalty_late_contract_line(osv.osv_memory):

    _name = "feosco.penalty.late.contract.line"

    _columns = {
        'product_from_id': fields.many2one('product.product', string=u'Sản phẩm'),
        'invoice_line_id': fields.many2one('account.invoice.line', string=u'Hoá đơn chi tiết'),
        'product_to_id': fields.many2one('product.product', string=u'Chính sách phạt', domain="[('sale_ok', '=', False)]"),
        'price_unit': fields.float(u'Giá phạt'),
        'parent_id': fields.many2one('feosco.penalty.late.contract', string=u'Phạt từ'),
        'quantity': fields.float(u'Số lượng'),
        'amount': fields.float(u'Giá chưa thuế'),
        'uos_id': fields.many2one('product.uom', string=u'Đơn vị'),
        'tax': fields.many2many('account.tax', 'invoice_line_rel', 'invoice_line_id', 'tax_id', u'VAT'),
        'discount': fields.float(u'Chiết khấu % trước thuế'),
        'sum': fields.float(u'Tổng tiền phạt'),

    }


feosco_penalty_late_contract_line()

