#-*- coding:utf-8 -*-

from openerp.osv import osv, fields

class hr_department(osv.Model):

    _name = "feosco.department"

    _columns = {
        'code': fields.char(u'Mã cục'),
        'name': fields.char(u'Tên chi cục'),
        'city_id': fields.many2one('feosco.city', string=u'Thành phố (tỉnh thành)'),
        'ref': fields.char(u'Tên viết tắt'),
        'manager_id': fields.many2one('res.users', string=u'Cán bộ quản lý'),
        'members': fields.many2many('res.users', 'department_user_rel', 'department_id', 'uid', u'Thành viên cục'),
        'parent_id': fields.many2one('feosco.department', string=u'Cấp trên'),
        'child_ids' :fields.one2many('feosco.department','parent_id',string=u'Các cấp dưới'),
        'team': fields.char(u'Tên đội'),
        'code_team':fields.char(u'Mã đội'),
        'description': fields.text(u'Ghi chú'),
        'type': fields.selection([
            ('tc_hq', u'Tổng cục Hải Quan'),
            ('cuc_hq', u'Cục Hải Quan'),
            ('chi_cuc_hq', u'Chi cục Hải Quan'),
            ('cuc_giam_sat_hq', u'Cục Giám Sát'),
            ],string = u'Cấp bậc'),
        
        'sequence_id': fields.many2one('ir.sequence', string=u'Thông số cấp mã địa điểm'),
    }
    
    def name_get(self, cr, uid, ids, context=None):
        if not len(ids):
            return []
        if context is None:
            context = {}
        res = []
        for r in self.read(cr, uid, ids, ['name', 'team'], context):
            rc_id = r.get('id')
            team = r.get('team', False)
            name = r.get('name', False)
            if r.get('team', False) and team.strip() != name.strip():
                res.append((rc_id, name + ' / ' + team ))
            else:
                res.append((rc_id, name))
        return res
    
    def create(self, cr, uid, values, context=None):
        
        # Create sequence
        sequence_values = {
                           'name': values.get('name', u'Thông số cấp địa điểm'),
                           'prefix': values.get('code', None),
                           }
        
        sequence_id = self.pool.get('ir.sequence').create(cr, uid, sequence_values, context=context)
        if sequence_id:
            values.update({'sequence_id': sequence_id})
        
        return super(hr_department, self).create(cr, uid, values, context)
    
    def write(self, cr, uid, ids, vals, context={}):
        return super(hr_department, self).write(cr, uid, ids, vals, context=context)