# -*- coding: utf-8 -*-


from openerp.osv import fields, osv
from openerp import SUPERUSER_ID


class feosco_custom(osv.osv):
    _name = 'feosco.custom'

    _columns = {
        'code': fields.char('Code', size=64, required=True),
        'name': fields.char('Customs Name', size=64, required=True),
        'note': fields.text('Note'),
    }


feosco_custom()


class feosco_custom_branch(osv.osv):
    _name = 'feosco.custom.branch'

    _columns = {
        'code': fields.char('Code', size=64, required=True),
        'name': fields.char('Customs Name', size=64, required=True),
        'note': fields.text('Note'),
        'custom_id': fields.many2one('feosco.custom', 'Custom'),
    }


feosco_custom_branch()