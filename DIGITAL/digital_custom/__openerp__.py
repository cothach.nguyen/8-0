# -*- coding: utf-8 -*-
{
    'name': 'FEOSCO Digital Department',
    'version': '0.1',
    'author': 'FEOSCO',
    'category': 'FEOSCO',
    'website': 'http://www.feosco.com',
    'images': [],
    'depends': ['digital_base'],
    'data': [
        'security/custom_security.xml',
        'security/ir.model.access.csv',
#         'data/department.xml',
        'view/department.xml',
#         'view/crm_claim_customs_view.xml',
    ],
    'installable': True,
    'auto_install': False,
}
