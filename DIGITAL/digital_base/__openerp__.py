# -*- coding: utf-8 -*-
{
    'name': 'FEOSCO Digital CORE Module',
    'version': '0.1',
    'author': 'FEOSCO',
    'category': 'FEOSCO',
    'website': 'http://www.feosco.com',
    'images': [],
    'depends': [
        'feosco_base',
        'feosco_web_adblock',

    ],
    'data': [
        'data/group_data.xml',
        'security/ir.model.access.csv',
        'view/res_partner_view.xml',
        'view/menu.xml',
    ],
    'installable': True,
    'auto_install': False,
}
