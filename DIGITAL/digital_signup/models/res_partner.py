# -*- coding: utf-8 -*-

from openerp.osv import osv, fields
import logging

class res_partner(osv.Model):

    _inherit = "res.partner"
    __logger = logging.getLogger(_inherit)

    _columns = {
        'feosco_token_serial': fields.char('Token Serial', size=128),
    }

    def create(self, cr, uid, vals, context={}):

        context = dict(context)
        self.__logger.info('BEGIN create')
        self.__logger.info('vals:  %s' % vals)

        self.__logger.info('END create')
        return super(res_partner, self).create(cr, uid, vals, context=context)