# -*- coding: utf-8 -*-
##############################################################################
#
#    FEOSCO, ERP Management Solution
#    Copyright (C) 2013-today FEOSCO (<http://www.feosco.com>)
#
##############################################################################
import logging
import werkzeug

import openerp
from openerp.addons.auth_signup.res_users import SignupError
from openerp import http
from openerp.http import request
from openerp.tools.translate import _
from .validate_email import validate_email


_logger = logging.getLogger(__name__)

class AuthSignupHome(openerp.addons.web.controllers.main.Home):
    
    @http.route('/web', type='http', auth="none")
    def web_client(self, s_action=None, **kw):
        
        cr, uid, context, registry = request.cr, request.uid, request.context, request.registry
        
        return super(AuthSignupHome, self).web_client(s_action, **kw)


    def _default_value(self, error=None):
        cr, uid, context, registry = request.cr, request.uid, request.context, request.registry

        orm_country = registry.get('res.country')
        country_ids = orm_country.search(cr, openerp.SUPERUSER_ID, [('code', '=', 'VN')], context=context)

        orm_city = registry.get('feosco.city')
        city_ids = orm_city.search(cr, openerp.SUPERUSER_ID, [('country_id', 'in', country_ids)], context=context)
        cities = orm_city.browse(cr, openerp.SUPERUSER_ID, city_ids, context=context)

        orm_district = registry.get('feosco.district')
        district_ids = orm_district.search(cr, openerp.SUPERUSER_ID, [('city_id','in', city_ids)], context=context)
        districts = orm_district.browse(cr, openerp.SUPERUSER_ID, district_ids, context=context)


        orm_title = registry.get('res.partner.title')
        title_ids = orm_title.search(cr, openerp.SUPERUSER_ID, [])
        titles = orm_title.browse(cr, openerp.SUPERUSER_ID, title_ids)

        return {
            'cities': cities,
            'districts': districts,
            'titles': titles,
        }


    @http.route('/web/signup', type='http', auth='public', website=True)
    def web_auth_signup(self, *args, **kw):
        _logger.info('BEGIN web_auth_signup')
        _logger.info(args)
        _logger.info(kw)
        cr, uid, context, registry = request.cr, request.uid, request.context, request.registry
        qcontext = self.get_auth_signup_qcontext()
        
        #Redirect to homepage
        if not kw.get('redirect',False):
            kw.update({'redirect' : '/'})

        orm_customer = request.registry['res.partner']
        qcontext.update(self._default_value())
        customer_id = None
        contact_id = None
        process = True
        # try:
        _logger.info('1')
        if kw.get('confirm_password') != kw.get('password'):
            qcontext['error'] = u'Thông tin password không khớp nhau'
            return request.render('auth_signup.signup', qcontext)
        _logger.info('-->2')
        if kw.get('feosco_account_num'):
            customer_ids = orm_customer.search(request.cr, openerp.SUPERUSER_ID, [('feosco_account_num', '=', kw.get('feosco_account_num'))])
            if customer_ids:
                qcontext['error'] = u'Mã Số Thuế trên đã được đăng ký'
                return request.render('auth_signup.signup', qcontext)
        _logger.info('-->3')
#         if not kw.has_key('feosco_token_serial') or not kw.get('feosco_token_serial'):
#             qcontext['error'] = u'Kiểm tra token USB, chạy Java JRE cho trình duyệt, thực hiện Chứng Thực Token để lấy thông tin đăng ký Doanh Nghiệp'
        # if kw.has_key('email'):
        #
        #     is_valid = validate_email(kw.get('email'), '')
        #     if is_valid == False:
        #         qcontext['error'] = u'Email không hợp lệ'
        #         return request.render('auth_signup.signup', qcontext)
        _logger.info('-->4')

        if not qcontext.get('token') and not qcontext.get('signup_enabled'):
            raise werkzeug.exceptions.NotFound()
        _logger.info('-->5')
        _logger.info('request: %s' % request.httprequest.method)

        if 'error' not in qcontext and request.httprequest.method == 'POST':
            try:
                res_id = self.do_signup(qcontext)
                _logger.info('res_id:  %s' % res_id)
                if process == True and kw and kw.has_key('city_id') and kw.has_key('title') and kw.has_key('feosco_district_id'):
                    _logger.info('-->update customer')
                    orm_partner = request.registry['res.partner']
                    partner_ids = orm_partner.search(cr, openerp.SUPERUSER_ID, [('name', '=', kw.get('name'))])
                    _logger.info('partner_ids: %s' % partner_ids)

                    print kw.get('file'),
                    print '-' * 100
                    kw.update({
                        'city_id': int(kw.get('city_id')),
                        'feosco_district_id': int(kw.get('feosco_district_id')),
                        'feosco_account_num': kw.get('login', ''),
                        'email': kw.get('email', ''),
                        'district_id': int(kw.get('feosco_district_id')) if kw.get('feosco_district_id') else None,
                        'type': 'default',
                        'is_company': True,

                    })

                    if partner_ids and len(partner_ids) == 1:
                        _logger.info('--> update customer')
                        orm_partner.write(cr, openerp.SUPERUSER_ID, partner_ids, kw)

                        if kw.has_key('contact_name'):
                            _logger.info('--> create new contact')
                            orm_partner.create(cr, openerp.SUPERUSER_ID, {
                                'title': int(kw.get('title')),
                                'name': kw.get('contact_name'),
                                'parent_id': partner_ids[0],
                                'website': '',
                                'type': 'contact',
                            })
                        cr.commit()
                _logger.info('END TRUE --> web_auth_signup')
                return super(AuthSignupHome, self).web_login(*args, **kw)


            except (SignupError, AssertionError), e:
                qcontext['error'] = _(e.message)
                process = False
                _logger.error('-->Error do_signup() with %s' % e.message)
        # except Exception, ex:
        #
        #     qcontext['error'] = ex


        _logger.info('customer_id: %s' % customer_id)
        _logger.info('contact_id: %s' % contact_id)
        _logger.info('END FALSE --> web_auth_signup')
        return request.render('auth_signup.signup', qcontext)

