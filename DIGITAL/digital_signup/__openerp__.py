# -*- coding: utf-8 -*-
{
    'name': 'FEOSCO SignUp',
    'version': '0.1',
    'author': 'FEOSCO',
    'category': 'FEOSCO',
    'website': 'http://www.feosco.com',
    'images': [],
    'depends': [
        'digital_website',
        'auth_signup',
    ],
    'data': [
        'views/digital_signup.xml',
        'views/main.xml',
    ],
    'installable': True,
    'auto_install': False,
}
