# -*- coding: utf-8 -*-
from openerp.osv import fields, osv

class ticket_assign(osv.osv_memory):

    _name = 'ticket.assign'
    _description = 'Ticket Assign'

    def fields_view_get(self, cr, user, view_id=None, view_type='form', context=None, toolbar=False, submenu=False):

        res = super(ticket_assign, self).fields_view_get(cr, user, view_id, view_type,
                                                         context, toolbar=toolbar, submenu=submenu)
        return res

    _columns = {
        'group_id': fields.many2one('feosco.claim.groups', 'Group'),
        'user_ids': fields.many2many('res.users', 'res_groups_users_rel_memory', 'group_id', 'user_id', 'Users'),
        'user_id': fields.many2one('res.users', 'User'),
        'assign_reason': fields.text('Assign Reason'),
    }

    def onchange_group_id(self, cr, uid, ids, group_id, context=None):
        if group_id:
            group_obj = self.pool.get('feosco.claim.groups').browse(cr, uid, group_id, context=context)
            return {'value': {'user_ids': [user.id for user in group_obj.users], 'user_id': False}}
        else:
            return {'value': {'user_ids': [], 'user_id': False}}

    def _get_group(self, cr, uid, context=None):
        if context is None:
            context = {}
        ticket_pool = self.pool.get('crm.claim')
        active_id = context.get('active_id', False)
        if active_id:
            ticket_obj = ticket_pool.browse(cr, uid, active_id, context=context)
            return ticket_obj.feosco_group_id.id if ticket_obj.feosco_group_id else False
        else:
            return False

    _defaults = {
        'group_id': _get_group
    }

    def assign_ticket(self, cr, uid, ids, context=None):
        ticket_pool = self.pool.get('crm.claim')
        ticket_ids = context.get('active_ids', None)
        if ticket_ids:
            ticket_pool.tracking_history(cr, uid, ticket_ids, 'Assign', context=context)
        for this in self.browse(cr, uid, ids, context=context):
            if ticket_ids:
                write_agrs = {'user_id': this.user_id.id if this.user_id else None}
                if this.group_id:
                    write_agrs.update({'feosco_group_id': this.group_id.id})
                context.update({'feosco_assign': this.assign_reason})
                res = ticket_pool.write(cr, uid, ticket_ids, write_agrs, context=context)


        return {
            'type': 'ir.actions.act_window_close'
        }


