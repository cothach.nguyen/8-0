# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import time

from openerp.osv import fields, osv
from openerp.tools.translate import _


class select_faq(osv.osv_memory):
    _name = 'select.faq'
    _description = 'Select FAQ to fill ticket'

    _columns = {
        'faq_id': fields.many2one('feosco.faq', 'FAQ'),
    }

    def insert_faq_to_ticket(self, cr, uid, ids, context=None):

        active_id = context.get('active_id', False)
        if active_id:
            this_wizard = self.browse(cr, uid, ids[0], context=context)
            faq_obj = self.pool.get('feosco.faq').browse(cr, uid, this_wizard.faq_id.id, context=context)
            create_data = {
                'cause': faq_obj.problem,
                'resolution': faq_obj.solution,
                'type_action': faq_obj.type_action if faq_obj.type_action else None
            }
            self.pool.get('crm.claim').write(cr, uid, [active_id], create_data, context=context)

        result = {
            'type': 'ir.actions.act_window_close',
        }

        return result


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
