#coding:utf-8

from openerp.osv import fields, osv


class feosco_crm_claim_history(osv.osv):

    _name = "feosco.crm.claim.history"

    _columns = {
        'create_date': fields.datetime('Start Date', readonly=True),
        'create_uid': fields.many2one('res.users', 'Create by', readonly=True),
        'end_date': fields.datetime('End Date', readonly=True),
        'description': fields.text('Description'),
        'ticket_id': fields.many2one('crm.claim', 'Ticket', readonly=True),
    }



    # _defaults = {
    #     # 'feosco_owner_id': lambda s, cr, uid, c: uid,
    # }

    # def create_ticket_history(self, cr, uid, ticket_ids=[], description=None, context=None):
    #     '''
    #         If user_id is None, use uid as default
    #     '''
    #     date_now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    #     for ticket_id in ticket_ids:
    #         history_ids = self.search(cr, uid, [('end_date', '=', False), ('ticket_id', '=', ticket_id)],
    #                                   context=context)
    #         if history_ids:
    #             self.write(cr, uid, history_ids, {'end_date': date_now}, context=context)
    #         create_data = {
    #             'description': description,
    #             'ticket_id': ticket_id
    #         }
    #         return self.create(cr, SUPERUSER_ID, create_data, context=context)

