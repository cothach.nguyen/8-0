# -*- coding: utf-8 -*-

from openerp.osv import fields, osv
import time


class feosco_faq(osv.osv):
    """ FAQ
    """
    _name = "feosco.faq"
    _columns = {
        'code': fields.char('Code', size=64, help='Code', readonly=True),
        'name': fields.char('Subject', size=128, readonly=True),
        'symptom': fields.text('Symptom', readonly=True),
        'problem': fields.text('Problem', readonly=True),
        'solution': fields.text('Solution', readonly=True),
        'comment': fields.text('Note', readonly=True),
        'date': fields.datetime('Date', readonly=True),
        'categ_id': fields.many2one('crm.case.categ', 'Category', readonly=True,
                                    domain="[('object_id.model', '=', 'crm.claim')]"),
        'type_action': fields.selection([('correction', 'Corrective Action'), ('prevention', 'Preventive Action')],
                                        'Action Type', readonly=True),
    }
    _order = "date desc"
    _defaults = {
        'date': lambda self, cr, uid, context={}: context.get('date', time.strftime("%Y-%m-%d %H:%M:%S")),
    }


    def _get_sequence(self, cr, uid, context=None):
        obj_seq = self.pool.get('ir.sequence')
        args_search = [('code', '=', 'feosco.faq')]
        sequence_ids = obj_seq.search(cr, uid, args_search, context=context)

        return obj_seq.next_by_id(cr, uid, sequence_ids[0], context=context)

    def create(self, cr, uid, vals, context=None):
        if context == None:
            context = {}

        vals['code'] = self._get_sequence(cr, uid, context)

        return super(feosco_faq, self).create(cr, uid, vals, context=context)


feosco_faq()