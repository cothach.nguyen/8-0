# -*- coding: utf-8 -*-

from openerp.osv import fields, osv


class feosco_claim_groups(osv.osv):

    _name = "feosco.claim.groups"

    _inherits = {
        'res.groups': 'group_id',
    }
    _order = 'sequence'

    _columns = {
        'group_id': fields.many2one('res.groups', 'Groups', ondelete="cascade", required=1),
        'sequence': fields.integer('Sequence', required=True),
        'vendor': fields.selection([('yes', 'Yes'), ('no', 'No')], 'Vendor', help="This group is Vendor",
                                   required=True),
        'email': fields.char('Email', size=256, required=True)
    }

feosco_claim_groups()