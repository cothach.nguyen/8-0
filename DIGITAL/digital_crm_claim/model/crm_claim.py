#-*- coding: utf-8 -*-

from datetime import datetime
from dateutil.relativedelta import relativedelta

from openerp.osv import fields, osv, orm
import logging
from openerp.tools.translate import _
from openerp import SUPERUSER_ID

STATE = [('new', 'New'),
         ('in_progress', 'In progress'),
         ('settled', 'Settled')]


class crm_claim(osv.osv):

    _name = "crm.claim"
    _inherit = ["crm.claim", 'ir.needaction_mixin']

    _log = logging.getLogger(_name)
    __logger = logging.getLogger(_name)

    def _get_url(self, cr, uid, ids, f_name, arg=None, context={}):
        self._log.info('==> begin _get_url')
        result = {}
        if not ids: return result
        orm_params = self.pool.get('ir.config_parameter')
        param_ids = orm_params.search(cr, uid, [('key', '=', 'feosco_crm_claim.url_server')])
        self._log.info(param_ids)
        if not param_ids: return result
        for self_id in ids:
            result[self_id] = orm_params.browse(cr, uid, param_ids[
                0]).value + '/web#id=%s&view_type=form&model=crm.claim' % self_id
        self._log.info('==> end _get_url')
        return result

    def _referentce_support(self, cr, uid, ids, f_name, arg, context={}):
        res = dict([(i, {}) for i in ids])

        for this in self.browse(cr, uid, ids):
            history_ids = []
            if this.partner_id:
                history_ids = self.search(cr, uid, [('partner_id', '=', this.partner_id.id), ('id', '!=', this.id)])
            res[this.id] = history_ids


        return res


    _columns = {

        'feosco_url': fields.function(_get_url, string='Url', type='char', store=False),
        'feosco_code': fields.char('Code', size=64, help='Code'),
        'feosco_group_id': fields.many2one('feosco.claim.groups', 'Groups', track_visibility='onchange'),
        'feosco_discussion': fields.text('Discussion'),
        # 'feosco_discussion_ids': fields.one2many('feosco.claim.discussion', 'claim_id', 'Discussions'),
        'feosco_tracking_medium_id': fields.many2one('crm.tracking.medium', 'Tracking Medium', help="Tracking Medium"),
        'state': fields.selection(STATE, 'State', track_visibility='onchange'),
        # 'feosco_owner_id': fields.many2one('res.users', 'Owner', track_visibility='always'),
        'feosco_customs_id': fields.many2one('feosco.department', 'Customs',domain="[('type','=','cuc_hq')]"),
        'feosco_customs_branch_id': fields.many2one('feosco.department', 'Customs Branch',
                                                    domain="[('parent_id','=',feosco_customs_id),('type','=','chi_cuc_hq')]"),
        'feosco_method_connect': fields.many2one('feosco.master.data', 'Method Connect / Used Sofware',
                                                 domain="[('type','=','method_connect')]"),
        'color': fields.integer('Color Index'),
        'feosco_inserted_faq': fields.boolean('Inserted FAQ'),
        'feosco_history_tracking': fields.one2many('feosco.crm.claim.history', 'ticket_id', 'Discussions'),
        'feosco_attachment_ids': fields.one2many('ir.attachment', 'res_id', 'Files'),
        # Override field to add track_visibility
        'description': fields.text('Description', track_visibility='onchange'),
        'resolution': fields.text('Resolution', track_visibility='onchange'),
        'cause': fields.text('Root Cause', track_visibility='onchange'),
        'date_deadline': fields.date('Deadline', track_visibility='onchange'),
        'user_id': fields.many2one('res.users', 'Responsible', track_visibility='onchange'),
        'feosco_history_ids': fields.one2many('feosco.crm.claim.history', 'ticket_id', 'Histories'),
        'feosco_reference_ids': fields.function(_referentce_support, relation="crm.claim", type="many2many", string='Histories Support'),
    }

    _track = {
        'state': {
            'feosco_crm_claim.mt_ticket_state': lambda self, cr, uid, obj, ctx=None: obj.state,
        },
        'date_deadline': {
            'feosco_crm_claim.mt_ticket_deadline': lambda self, cr, uid, obj, ctx=None: obj.date_deadline,
        },
        'feosco_group_id': {
            'feosco_crm_claim.mt_ticket_assigned': lambda self, cr, uid, obj,
                                                          ctx=None: obj.feosco_group_id and obj.feosco_group_id.id,
        },
        'user_id': {
            'feosco_crm_claim.mt_ticket_assigned_responsible': lambda self, cr, uid, obj,
                                                                      ctx=None: obj.user_id and obj.user_id.id,
        },
        'description': {
            'feosco_crm_claim.mt_ticket_description': lambda self, cr, uid, obj,
                                                             ctx=None: obj.description and obj.description,
        },
        'cause': {
            'feosco_crm_claim.mt_ticket_cause': lambda self, cr, uid, obj,
                                                       ctx=None: obj.cause and obj.cause,
        },
        'resolution': {
            'feosco_crm_claim.mt_ticket_cause': lambda self, cr, uid, obj,
                                                       ctx=None: obj.resolution and obj.resolution,
        }
    }

    def _get_default_group_id(self, cr, uid, context=None):
        group_id = self.pool.get('feosco.claim.groups').search(cr, uid, [], context=context)
        return group_id[0] if group_id else False

    _defaults = {
        'feosco_group_id': lambda s, cr, uid, c: s._get_default_group_id(cr, uid, c),
        'state': 'new',
    }

    def tracking_history(self, cr, uid, ids, action, context={}):
        self.__logger.info('===> begin tracking_history')
        self.__logger.info('ids %s' % ids)
        self.__logger.info('action %s' % action)
        track = None
        orm_history = self.pool.get('feosco.crm.claim.history')
        date_now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        for this in self.browse(cr, uid, ids):
            history_ids = orm_history.search(cr, uid, [('ticket_id', '=', this.id), ('end_date', '=', None)])
            self.__logger.info('history_ids %s' % history_ids)
            if history_ids and len(history_ids) == 1:

                orm_history.write(cr, uid, history_ids, {'end_date': date_now})
            val = {
                'description': action,
                'ticket_id': this.id
            }
            if action == 'Setted':
                val.update({'end_date': date_now})
            track = orm_history.create(cr, uid, val, context=context)
        self.__logger.info('return %s' % track)
        self.__logger.info('===> end tracking_history')
        return track

    def set_new(self, cr, uid, ids, context={}):
        self.tracking_history(cr, uid, ids, 'Reset', context=context)
        return self.write(cr, uid, ids, {'state': 'new'}, context=context)

    def set_ass_to_me(self, cr, uid, ids, context={}):
        self.set_in_progress(cr, uid, ids, context=context)
        return self.write(cr, uid, ids, {'user_id': uid}, context=context)


    def set_in_progress(self, cr, uid, ids, context={}):
        self.tracking_history(cr, uid, ids, 'Process', context=context)
        return self.write(cr, uid, ids, {'state': 'in_progress'}, context=context)

    def set_settled(self, cr, uid, ids, context={}):
        self.tracking_history(cr, uid, ids, 'Setted', context=context)
        self._check_solution_cause(cr, uid, ids, context=context)
        date_closed = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        return self.write(cr, uid, ids, {'state': 'settled', 'date_closed': date_closed}, context=context)

    def _check_solution_cause(self, cr, uid, ids, context=None):
        '''
            This function used to constraint:
                User must be input Root Case and Resolution Action before settling the ticket
        '''

        for ticket_obj in self.browse(cr, uid, ids, context):
            if not ticket_obj.cause or not ticket_obj.resolution:
                raise osv.except_osv(_('Error!'),
                                     _("Please enter Root Cause and Resolution Action before settling the ticket."))
        else:
            return True

    def set_rejected(self, cr, uid, ids, context={}):
        self.tracking_history(cr, uid, ids, 'Reject', context=context)
        return self.write(cr, uid, ids, {'user_id': None, 'state': 'new'}, context=context)

    def _get_sequence(self, cr, uid, context={}):
        obj_seq = self.pool.get('ir.sequence')
        args_search = [('code', '=', 'crm.claim')]
        sequence_ids = obj_seq.search(cr, uid, args_search, context=context)

        return obj_seq.next_by_id(cr, uid, sequence_ids[0], context=context)

    def _add_user_to_follower(self, cr, uid, ids, user_id=None, context={}):
        user_list = []
        if user_id:
            user_list.append(user_id)

        return self.message_subscribe_users(cr, uid, ids, user_ids=user_list, subtype_ids=None, context=None)

    def auto_compute_deadline(self, cr, uid, context={}):
        total_date = self.pool['ir.config_parameter'].get_param(cr, uid, 'feosco_deadline_config')
        if total_date:
            return (datetime.today() + relativedelta(days=int(total_date))).strftime('%Y-%m-%d')
        else:
            return None


    def create(self, cr, uid, vals, context={}):
        if context is None:
            context = {}
        vals['feosco_code'] = self._get_sequence(cr, uid, context)
        #Auto compute deadline
        vals.update({'date_deadline': self.auto_compute_deadline(cr, uid, context=context)})
        # Create ticket
        claim_id = super(crm_claim, self).create(cr, uid, vals, context=context)
        # Add user to follower
        if claim_id:
            user_id = vals.get('user_id', False)
            self._add_user_to_follower(cr, uid, [claim_id], user_id=user_id, context=context)

        # self.send_mail(cr, uid, claim_id, 'crm.claim.new', context)
        self.tracking_history(cr, uid, [claim_id], 'Create', context=context)
        return claim_id

    def write(self, cr, uid, ids, vals, context={}):
        res = super(crm_claim, self).write(cr, uid, ids, vals, context=context)
        # Add user to follower
        if vals.has_key('user_id'):
            user_id = vals.get('user_id', False)
            self._add_user_to_follower(cr, uid, ids, user_id=user_id, context=context)
        # if vals.has_key('state'):
        #     for claim_id in ids:
        #         if vals.get('state') != 'settled':
        #             self.send_mail(cr, uid, claim_id, 'crm.claim.state', context)
        #         else:
        #             self.send_mail(cr, uid, claim_id, 'crm.claim.final', context)
        return res

    def create_faq(self, cr, uid, ids, context=None):
        res = []
        faq_pool = self.pool.get('feosco.faq')
        for this in self.browse(cr, uid, ids, context=context):
            create_agrs = {
                'name': this.name,
                'symptom': this.description,
                'problem': this.cause,
                'solution': this.resolution,
                'categ_id': this.categ_id.id if this.categ_id else None,
                'type_action': this.type_action
            }
            faq_id = faq_pool.create(cr, uid, create_agrs, context=context)
            if faq_id:
                res.append(faq_id)
                self.write(cr, uid, [this.id], {'feosco_inserted_faq': True}, context=context)
        # self.send_mail(cr, uid, res, 'crm.claim.new', context)
        return res

    def message_get_suggested_recipients(self, cr, uid, ids, context=None):
        recipients = super(crm_claim, self).message_get_suggested_recipients(cr, uid, ids, context=context)
        try:
            for claim in self.browse(cr, uid, ids, context=context):

                if claim.email_from:
                    self._message_add_suggested_recipient(cr, uid, recipients, claim, email=claim.email_from,
                                                          reason=_('Customer Email'))
                elif claim.partner_id:
                    self._message_add_suggested_recipient(cr, uid, recipients, claim, partner=claim.partner_id,
                                                          reason=_('Customer'))

        except (osv.except_osv,
                orm.except_orm):  # no read access rights -> just ignore suggested recipients because this imply modifying followers
            pass
        return recipients

    def message_subscribe(self, cr, uid, ids, partner_ids, subtype_ids=None, context=None):

        # Check if partner is customer of ticket (never add customer to follower). Return
        for this in self.browse(cr, uid, ids, context=context):
            if this.partner_id and this.partner_id.id in partner_ids:
                partner_ids.remove(this.partner_id.id)
            else:
                continue

        return super(crm_claim, self).message_subscribe(cr, uid, ids, partner_ids, subtype_ids=subtype_ids,
                                                        context=context)

    def message_post(self, cr, uid, thread_id, body='', subject=None, type='notification', subtype=None,
                     parent_id=False, attachments=None, context=None, **kwargs):
        '''
        Inherit this function to add reason when assign a ticket
        The Reason will be add to log
        '''

        feosco_assign = context.get('feosco_assign', False)
        if feosco_assign:
            body = body + u'<div> &nbsp; &nbsp; &bull; <b>Reason</b>: %s</div>' % feosco_assign
            uid = SUPERUSER_ID
        return super(crm_claim, self).message_post(cr, uid, thread_id, body=body, subject=subject, type=type,
                                                   subtype=subtype, parent_id=parent_id, attachments=attachments,
                                                   context=context, **kwargs)

    def _needaction_domain_get(self, cr, uid, context=None):
        return []  #['|', ('user_id', '=', uid), ('state', '=', 'new')]

    def send_mail(self, cr, uid, object_id, template_name, context={}):
        self._log.info('===>: begin send_mail_with_template()')
        template_pool = self.pool.get('email.template')
        template_ids = template_pool.search(cr, uid, [('name', '=', template_name)])
        self._log.info('--> object_id %s' % object_id)
        self._log.info('--> template_name %s' % template_name)
        self._log.info('--> template_ids %s' % template_ids)
        template_pool.send_mail(cr, uid, template_ids[0], object_id, True, context=context)
        self._log.info('===> end send_mail_with_template')
        return True


crm_claim()