from openerp.osv import fields, osv, orm


class crm_case_categ(osv.osv):
    """ Category of Case """

    _inherit = "crm.case.categ"

    _columns = {
        'feosco_code': fields.char('Code', size=64),
    }

