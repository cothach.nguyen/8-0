# -*- coding: utf-8 -*-
from openerp.osv import fields, osv, orm
import logging
from datetime import datetime, date, timedelta


class feosco_crm_claim_cronjob(osv.osv):
    _name = "feosco.crm.claim.cronjob"
    __logger = logging.getLogger(_name)
    _auto = False

    def auto_check_time_ticket_owner(self, cr, uid, automatic=False, use_new_cursor=False, context=None):
        self.__logger.info('START: auto_check_time_ticket_owner')

        try:
            if context is None:
                context = {}
            over_deadline_ticket_ids = []
            ticket_pool = self.pool.get('crm.claim')

            agrs = [
                ('date_deadline', '<', datetime.now().strftime('%Y-%m-%d %H:%M:%S')),
                ('user_id', '!=', False)
            ]

            over_deadline_ticket_ids = ticket_pool.search(cr, uid, agrs, context=context)

            if over_deadline_ticket_ids:
                write_args = {
                    'user_id': None
                }

                ticket_pool.write(cr, uid, over_deadline_ticket_ids, write_args, context=context)

        except Exception as exc:
            self.__logger.info('Can NOT remove owner ticket %s' % (exc))

        self.__logger.info('Remove owner of ticket %s' %(str(over_deadline_ticket_ids)))
        self.__logger.info('END: auto_check_time_ticket_owner')
        return True