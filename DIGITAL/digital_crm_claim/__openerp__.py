{
    'name': 'FEOS: Feosco Claims Management',
    'version': '1.0',
    'category': 'Feosco',
    'description': """

Manage Customer Claims.

This application allows you to track your customers/suppliers claims and grievances.

It is fully integrated with the email gateway so that you can create
automatically new claims based on incoming emails.

    """,
    'author': 'Feosco',
    'website': 'http://www.feosco.com',
    'depends': ['crm_claim', 'document', 'feosco_base','digital_custom'],
    'data': [
        'security/claim_security.xml',
        'security/ir.model.access.csv',
        'view/menu.xml',

        'wizard/assign.xml',
        'wizard/select_faq.xml',
        'report/crm_claim_report_view.xml',
        'view/crm_claim_group_view.xml',
        'view/crm_claim_view.xml',
        'view/crm_case_categ_view.xml',
        'view/crm_claim_faq_view.xml',
        'view/crm_claim_config_view.xml',

        'data/sequence.xml',
        'data/data.xml',
        'data/cronjob_data.xml',
        'data/email_template.xml',


    ],
    'demo': ['demo/demo.xml', 'demo/user_demo.xml'],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
    'images': [

    ],
}
