# -*- coding: utf-8 -*-
from openerp.osv import fields, osv

class ticket_assign(osv.osv_memory):

    _name = 'assign.cucgiamsat'
    _description = u'Bàn giao cuc giám sát'

    def fields_view_get(self, cr, user, view_id=None, view_type='form', context=None, toolbar=False, submenu=False):

        res = super(ticket_assign, self).fields_view_get(cr, user, view_id, view_type,
                                                         context, toolbar=toolbar, submenu=submenu)
        return res

    _columns = {
         'cuc_hai_quan_id':fields.many2one('feosco.department', u'Cục Hải Quan'),
         'cucgiamsat_id': fields.many2one('feosco.department', u'Cục giám sát',
                                            domain="[('type','=','cuc_giam_sat_hq')]"), # ,('parent_id','=',cuc_hai_quan_id)
    }
    
    def _get_cuc_hai_quan_id(self, cr, uid, context=None):
        if context is None:
            context = {}
        reuisition_pool = self.pool.get('feosco.digital.requisition')
        active_id = context.get('active_id', False)
        if active_id:
            requisition_obj = reuisition_pool.browse(cr, uid, active_id, context=context)
            return requisition_obj.customs_id.id if requisition_obj.customs_id else False
        else:
            return False

    _defaults = {
        'cuc_hai_quan_id': _get_cuc_hai_quan_id
    }


    def assign(self, cr, uid, ids, context=None):
        requisition_pool = self.pool.get('feosco.digital.requisition')
        warehouse_orm = self.pool('feosco.digital.warehouse')
        requisition_ids = context.get('active_ids', None)
        
        this = self.browse(cr, uid, ids[0], context=context)
        if requisition_ids:
            # Write requisition
            res = requisition_pool.write(cr, uid, requisition_ids, {'cuc_giam_sat_id': this.cucgiamsat_id.id, 'state': 'approved1'}, context=context)
            # Write warehouse
            for requisition in requisition_pool.browse(cr, uid, requisition_ids, context=context):
                warehouse_orm.write(cr, uid, [requisition.warehouse_id.id], {'state': 'approved1'}, context=context)
            
        return {
            'type': 'ir.actions.act_window_close'
        }