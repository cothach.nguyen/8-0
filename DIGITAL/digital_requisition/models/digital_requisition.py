#-*- coding: utf-8 -*-
from openerp.osv import osv, fields
from openerp import _

class feosco_digital_requisition(osv.Model):

    _name = "feosco.digital.requisition"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = u"Đơn xin cấp địa điểm kho của Doanh Nghiệp"

    
    _columns = {
        'name': fields.char(u'Tên'),
        'warehouse_id': fields.many2one('feosco.digital.warehouse', u'Kho'),
        'chi_cuchq_id': fields.many2one('feosco.department', u'Chi cục Hải Quan'),
        'customs_id': fields.many2one('feosco.department', u'Cục Hải Quan'),
        'cuc_giam_sat_id': fields.many2one('feosco.department', u'Cục Giám Sát'),
        'attachment_ids': fields.one2many('ir.attachment','res_id', u'File đính kèm', required='True'),
        'customer_id': fields.related('warehouse_id','customer_id', 
                                      string=u"Doanh nghiệp", type="many2one",relation="res.partner",store=False),
        'state': fields.selection([
            ('new', u'Yêu cầu mới'),
            ('assigned', u'Chờ chi cục duyệt'),
            ('approved1', u'Chờ giám sát duyệt'),
            ('approved2', u'Chờ tổng cục duyệt và cấp mã'),
            ('approved3', u'Đã cấp mã'),
            ('cancel', u'Từ chối')
            ],
            string = u'Trạng thái'),
       'provider_latitude': fields.related('warehouse_id', 'provider_latitude', type='float', string=u'Kinh độ', readonly=True),
       'provider_longitude': fields.related('warehouse_id', 'provider_longitude', type='float', string=u'Vĩ độ', readonly=True),
       
        'code': fields.related('warehouse_id', 'code', type='char', string=u'Mã địa điểm', readonly=True),
        
        'warehouse_street1': fields.related('warehouse_id', 'street1', type='char', string=u'Địa chỉ', readonly=True),
        'warehouse_district_id': fields.related('warehouse_id','district_id', string=u"Quận / Huyện", type="many2one",relation="feosco.district",store=False, readonly=True),
        'warehouse_city_id' : fields.related('warehouse_id','city_id', string=u"Tỉnh / Thành", type="many2one",relation="feosco.city",store=False, readonly=True),
        'warehouse_is_employ': fields.related('warehouse_id', 'is_employ', type='boolean', string=u'Kho của công ty?', readonly=True),
        'warehouse_owner': fields.related('warehouse_id', 'owner', type='char', string=u'Chủ sở hữu', readonly=True),
        'warehouse_superficies': fields.related('warehouse_id', 'superficies', type='float', string=u'Diện tích', readonly=True),
        'warehouse_assets': fields.related('warehouse_id', 'assets', type='text', string=u'Trang thiết bị', readonly=True),
    }

    _defaults = {
        'state': 'new',
    }
    _order = "create_date DESC"
    
    def approved1(self, cr, uid, ids, context=None):
        warehouse_orm = self.pool('feosco.digital.warehouse')
        for this in self.browse(cr, uid, ids):
            if this.warehouse_id:
                warehouse_orm.write(cr, uid, [this.warehouse_id.id], {'state': 'approved1'}, context=context)
            else:
                raise osv.except_osv(_('Error'),_('None Warehouse in this requisition'))
        return self.write(cr, uid, ids, {'state': 'approved1'}, context=context)
    
    def approved2(self, cr, uid, ids, context=None):
        warehouse_orm = self.pool('feosco.digital.warehouse')
        for this in self.browse(cr, uid, ids):
            if this.warehouse_id:
                warehouse_orm.write(cr, uid, [this.warehouse_id.id], {'state': 'approved2'}, context=context)
            else:
                raise osv.except_osv(_('Error'),_('None Warehouse in this requisition'))
        return self.write(cr, uid, ids, {'state': 'approved2'}, context=context)
    
    def _cap_ma_dia_diem_by_chi_cuc(self, cr, uid, sequence_id, context=None):
        code = self.pool('ir.sequence').next_by_id(cr, uid, sequence_id, context=context)
        
        return code
    
    def approved3(self, cr, uid, ids, context=None):
        warehouse_orm = self.pool('feosco.digital.warehouse')
        for this in self.browse(cr, uid, ids):
            if this.warehouse_id:
                # Cấp mã
                if this.chi_cuchq_id:
                    sequence_id = this.chi_cuchq_id.sequence_id.id
                    code = self._cap_ma_dia_diem_by_chi_cuc(cr, uid, sequence_id, context)
                    warehouse_orm.write(cr, uid, [this.warehouse_id.id], {'state': 'approved3', 'code': code}, context=context)
                else:
                    raise osv.except_osv(_('Error'),_(u'Không tìm thấy chi cục hải quan'))
            else:
                raise osv.except_osv(_('Error'),_('None Warehouse in this requisition'))
            
        return self.write(cr, uid, ids, {'state': 'approved3'}, context=context)
    

    def feedback(self, cr, uid, ids, context=None):
        warehouse_orm = self.pool('feosco.digital.warehouse')
        for this in self.browse(cr, uid, ids):
            if this.warehouse_id:
                warehouse_orm.write(cr, uid, [this.warehouse_id.id], {'state': 'feedback'}, context=context)
            else:
                raise osv.except_osv(_('Error'),_('None Warehouse in this requisition'))
        return self.write(cr, uid, ids, {'state': 'feedback'}, context=context)

    def cancel(self, cr, uid, ids, context=None):
        warehouse_orm = self.pool('feosco.digital.warehouse')
        for this in self.browse(cr, uid, ids):
            if this.warehouse_id:
                warehouse_orm.write(cr, uid, [this.warehouse_id.id], {'state': 'cancel'}, context=context)
            else:
                raise osv.except_osv(_('Error'),_('None Warehouse in this requisition'))
        return self.write(cr, uid, ids, {'state': 'cancel'}, context=context)

    def _update_warehouse_name(self, cr, uid, ids, warehouse_name,context=None):
        warehouse_orm = self.pool('feosco.digital.warehouse')
        for this in self.browse(cr, uid, ids):
            if this.warehouse_id:
                warehouse_orm.write(cr, uid, [this.warehouse_id.id], {'name': warehouse_name}, context=context)

        return True

    def write(self, cr, uid, ids, vals, context=None):
        warehouse_name = vals.get('name',False)
        if warehouse_name:
            self._update_warehouse_name(cr, uid, ids, warehouse_name, context=context)
        return super(feosco_digital_requisition, self).write(cr, uid, ids, vals, context=context)
    
    def _needaction_domain_get(self, cr, uid, context=None):
        dom = False
        user_pool = self.pool.get('res.users')
        
        dom = []
        if user_pool.has_group(cr, uid, 'digital_base.nhom_cuc_hai_quan'):
            dom.append(('state', '=', 'new'))
        if user_pool.has_group(cr, uid, 'digital_base.nhom_chi_cuc_hai_quan'):
            dom.insert(0, '|')
            dom.append(('state', '=', 'assigned'))
        if user_pool.has_group(cr, uid, 'digital_base.nhom_giam_sat'):
            dom.insert(0, '|')
            dom.append(('state', '=', 'approved1'))
        if user_pool.has_group(cr, uid, 'digital_base.nhom_tong_cuc_hai_quan'):
            dom.insert(0, '|')
            dom.append(('state', '=', 'approved2'))
            
        return dom
        
        
        
        
        
        