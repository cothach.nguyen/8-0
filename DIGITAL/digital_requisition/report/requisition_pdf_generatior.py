#-*- coding:utf-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import time

from openerp.osv import osv
from openerp.report import report_sxw


class wrapped_generate_data(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(wrapped_generate_data, self).__init__(cr, uid, name, context)
        self.REQUISITION_ID = []
        self.localcontext.update({
            'time': time,
            'get_requisition_data': self.get_requisition_data,
        })
        
    def set_context(self, objects, data, ids, report_type=None):
        if data:
            ids = data.get('ids',[])
            self.REQUISITION_ID = data.get('ids',[])
        return super(wrapped_generate_data, self).set_context(objects, data, ids, report_type=report_type)
        
    def get_requisition_data(self):
        requisition_pool = self.pool.get('feosco.digital.requisition')
        res = []
        requisition_id = self.REQUISITION_ID if self.REQUISITION_ID else False
        if requisition_id:
            res = requisition_pool.browse(self.cr, self.uid, requisition_id)
        return res

class wrapped_report_requisition_pdf_generatior(osv.AbstractModel):
    _name = 'report.digital_requisition.pdf_generatior'
    _inherit = 'report.abstract_report'
    _template = 'digital_requisition.pdf_generatior'
    _wrapped_report_class = wrapped_generate_data

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
