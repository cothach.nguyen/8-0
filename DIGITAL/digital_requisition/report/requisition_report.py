# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields,osv
from openerp import tools


class feosco_digital_requisition_report(osv.osv):
    """ CRM Claim Report"""

    _name = "feosco.digital.requisition.report"
    _auto = False
    _description = "Warehouse Report"

    _columns = {
        'name': fields.char(u'Tên',readonly=True),
        'street1': fields.char(u'Địa chỉ',readonly=True),
        'district_id': fields.many2one('feosco.district', u'Quận (huyện)',readonly=True),
        'city_id': fields.many2one('feosco.city', u'Tỉnh / Thành',readonly=True),
        'customer_id':fields.many2one('res.partner', u'Doanh nghiệp',readonly=True),
        'is_employ': fields.boolean(u'Kho công ty ?',readonly=True),
        'state': fields.selection([
            ('new', u'Yêu cầu mới'),
            ('assigned', u'Chờ chi cục duyệt'),
            ('approved1', u'Chờ giám sát duyệt'),
            ('approved2', u'Chờ tổng cục duyệt và cấp mã'),
            ('approved3', u'Đã cấp mã'),
            ('cancel', u'Từ chối')
            ],
            string = u'Trạng thái', readonly=True),
        'create_date': fields.datetime('Create Date', readonly=True, select=True),
        'chi_cuchq_id': fields.many2one('feosco.department', u'Chi cục Hải Quan'),
        'customs_id': fields.many2one('feosco.department', u'Cục Hải Quan'),
        'cuc_giam_sat_id': fields.many2one('feosco.department', u'Cục Giám Sát'),
        'nbr': fields.integer('# of Cases', readonly=True),
        'date_from':fields.function(lambda *a,**k:{}, method=True, type='date',string=u"Từ Ngày"),
        'date_to':fields.function(lambda *a,**k:{}, method=True, type='date',string=u"Đến Ngày"),
    }

    def init(self, cr):

        tools.drop_view_if_exists(cr, 'feosco_digital_requisition_report')
        cr.execute("""
            create or replace view feosco_digital_requisition_report as (
                select
                    min(c.id) as id,
                    w.name,
                    w.street1,
                    w.district_id,
                    w.city_id,
                    w.customer_id,
                    w.is_employ,
                    w.state,
                    count(1) as nbr,
                    date_trunc('day',w.create_date) as create_date,
                    c.chi_cuchq_id,
                    c.customs_id,
                    c.cuc_giam_sat_id
                from
                    feosco_digital_requisition c
                inner join feosco_digital_warehouse w on w.id = c.warehouse_id
                group by
                    w.name, w.street1, w.city_id, w.district_id, w.customer_id, w.is_employ,w.state,w.create_date,c.chi_cuchq_id,c.customs_id,c.cuc_giam_sat_id
            )""")


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
