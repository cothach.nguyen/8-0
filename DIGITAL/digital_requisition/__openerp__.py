# -*- coding: utf-8 -*-
{
    'name': 'FEOSCO digital requisition',
    'version': '0.1',
    'author': 'FEOSCO',
    'category': 'FEOSCO',
    'website': 'http://www.feosco.com',
    'images': [],
    'depends': ['base', 'digital_custom','digital_warehouse'],
    'data': [
        'security/ir.model.access.csv',
        'security/requisition_security.xml',
        'sequence/sequence.xml',
        'wizard/assign.xml',
        'wizard/assign_cucgiamsat.xml',
        'wizard/assign_cuchaiquan.xml',
        'view/digital_requisition_view.xml',
        'view/ir_attachment_view.xml',
        'report/requisition_pdf_generatior.xml',
        'report/requisition_report_view.xml',
        'view/requisition_report.xml',
        
    ],
    'installable': True,
    'auto_install': False,
}
