{
    'name': 'FEOSCO: Digital GoogleMap',
    'version': '0.1',
    'summary': 'Google Map in Form View Extend',
    'sequence': '19',
    'category': 'FEOSCO',
    'complexity': 'easy',
    'description': "Google Map Form View Extend new wiget map",
    'data': [
        "views_template/digital_gmap.xml",
        "views_backend/res_partner_view.xml",
    ],
    'depends': ['base'],

    'qweb': [
        'static/src/xml/digital.xml',
        ],
    'installable': True,
    'auto_install': False,
    'application': False,
}
