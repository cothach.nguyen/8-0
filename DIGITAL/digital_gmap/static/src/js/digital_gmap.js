
openerp.digital_gmap = function(instance) {
    var _t = instance.web._t,
        _lt = instance.web._lt;
    var QWeb = instance.web.qweb;

    instance.digital_gmap = {};

    instance.digital_gmap.WidgetCoordinates = instance.web.form.FormWidget.extend({

        init: function() {
            console.log('begin render widget');
            this._super.apply(this, arguments);
        },
        start: function() {
            console.log('begin start()');
            this._super();
            this.field_manager.on("field_changed:provider_latitude", this, this.display_map);
            this.field_manager.on("field_changed:provider_longitude", this, this.display_map);
            this.on("change:effective_readonly", this, this.display_map);
            this.display_map();
            console.log('end start()');
        },
        display_map: function() {
            var self = this;
            console.log('begin display_map()');
            console.log(this.field_manager.get_field_value("provider_latitude"));
            console.log(this.field_manager.get_field_value("provider_longitude"));
            this.$el.html(QWeb.render("WidgetCoordinates", {
                "latitude": this.field_manager.get_field_value("provider_latitude") || 0,
                "longitude": this.field_manager.get_field_value("provider_longitude") || 0,
            }));
            this.$("button").toggle(! this.get("effective_readonly"));
            this.$("button").click(function() {
                navigator.geolocation.getCurrentPosition(_.bind(self.received_position, self));
            });
            console.log('end display_map()');
        },
        received_position: function(obj) {
            console.log('begin received_position()');
            var la = obj.coords.latitude;
            var lo = obj.coords.longitude;
            console.log(la);
            console.log(lo);
            console.log('end render widget');
            this.field_manager.set_values({
                "provider_latitude": la,
                "provider_longitude": lo
            });
        }
    });

    instance.web.form.custom_widgets.add('coordinates', 'instance.digital_gmap.WidgetCoordinates');

}