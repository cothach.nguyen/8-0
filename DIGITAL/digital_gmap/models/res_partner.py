# -*- coding: utf-8 -*-
from openerp.osv import osv, fields
import urllib2, urllib, StringIO, json
from openerp.tools.translate import _
import logging

_logger = logging.getLogger(__name__)

class res_partner(osv.Model):

    _inherit = "res.partner"

    # werkzeug.Href(url)(params or None)

    def _get_lat_long_gmap(self, cr, uid, ids, name, arg, context=None):

        url = "http://maps.google.com/maps/api/geocode/json?"

        res = {}
        if not ids:
            return res
        else:
            for this in self.browse(cr, uid, ids):
                res[this.id] = {
                    'provider_latitude': 0.00,
                    'provider_longitude': 0.00,
                }
                street = this.street.encode('utf-8') if this.street else ''
                city = this.feosco_city_id.name.encode('utf-8') if this.feosco_city_id else ''
                district = this.feosco_district_id.name.encode('utf-8') if this.feosco_district_id else ''
                country = this.country_id.name.encode('utf-8') if this.country_id else ''
                address_full = ''
                if street:
                    address_full += street
                if district:
                    if not address_full:
                        address_full += district
                    else:
                        address_full += ','
                        address_full += district
                if city:
                    if not address_full:
                        address_full += city
                    else:
                        address_full += ','
                        address_full += city
                if country:
                    if not address_full:
                        address_full += country
                    else:
                        address_full += ','
                        address_full += country
                address_full += ','

                urlParams = {
                    'address': address_full,
                    'sensor': 'false',
                }
                url = 'http://maps.google.com/maps/api/geocode/json?' + urllib.urlencode( urlParams )
                response = urllib2.urlopen(url)
                responseBody = response.read()

                body = StringIO.StringIO(responseBody)
                result = json.load(body)

                if 'status' not in result or result['status'] != 'OK':
                    print '---false get data from gooogle map-----'
                    pass
                else:
                    print result['results'][0]['geometry']['location']['lat']
                    print result['results'][0]['geometry']['location']['lng']
                    res[this.id] = {
                        'provider_latitude': result['results'][0]['geometry']['location']['lat'],
                        'provider_longitude': result['results'][0]['geometry']['location']['lng']
                    }
            return res


    _columns = {
        'max_quantity': fields.float(string="Max Quantity"),
        'provider_latitude': fields.function(_get_lat_long_gmap, type='float', digits=(12,6), multi='_get_latlong', string=u'Kinh độ'),
        'provider_longitude': fields.function(_get_lat_long_gmap, type='float', digits=(12,6), multi='_get_latlong', string=u'Vĩ độ'),
    }