function addRow(tableID) {

    var table = document.getElementById(tableID);
    var rowCount = table.rows.length;
    var row = table.insertRow(rowCount);

    var cell0 = row.insertCell(0);
    var element0 = document.createElement("input");
    element0.type = "text";
    element0.name = "link" + rowCount;
    element0.class = "form-control";
    element0.require = "True";
    cell0.appendChild(element0);

    var cell1 = row.insertCell(1);
    var element1 = document.createElement("input");
    element1.type = "text";
    element1.name = "size" + rowCount;
    element1.class = "form-control";
    cell1.appendChild(element1);


    var cell2 = row.insertCell(2);
    var element2 = document.createElement("input");
    element2.type = "text";
    element2.name = "color" + rowCount;
    element2.class = "form-control";
    cell2.appendChild(element2);

    var cell3 = row.insertCell(3);
    var element3 = document.createElement("input");
    element3.type = "text";
    element3.name = "price" + rowCount;
    element3.class = "form-control";
    cell3.appendChild(element3);

    var cell4 = row.insertCell(4);
    var element4 = document.createElement("input");
    element4.type = "text";
    element4.name = "quantity" + rowCount;
    element4.class = "form-control";
    cell4.appendChild(element4);

    rowCount = rowCount + 1;

}

function deleteRow(tableID) {
    try {
        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;

        for (var i = 0; i < rowCount; i++) {
            var row = table.rows[i];
            var chkbox = row.cells[0].childNodes[0];
            if (null != chkbox && true == chkbox.checked) {
                table.deleteRow(i);
                rowCount--;
                i--;
            }


        }
    } catch (e) {
        alert(e);
    }
}
