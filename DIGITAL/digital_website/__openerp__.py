{
    'name': 'FEOS: Digital Website Frontend',
    'category': 'Website',
    'version': '1.0',
    'summary': 'Website Forms',
    'description': """""",
    'author': 'Feosco',
    'website': 'http://www.feosco.com',
    'depends': [
        'website',
        'digital_base',
        'digital_warehouse',
        'digital_requisition',
        'digital_custom',
    ],
    'data': [
        'views/template.xml',
        'views/main.xml',
        'views/themes.xml',
        'views/website_templates.xml'
    ],

    'installable': True,
}
