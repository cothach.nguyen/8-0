# -*- coding: utf-8 -*-
import base64
import os
import time

from openerp import SUPERUSER_ID
from openerp.addons.web import http
from openerp.addons.web.http import request
import flask
import json

import logging

_ticket_per_page = 1  # ticket Per Page


class website_helpdesk(http.Controller):
    __logger = logging.getLogger(__name__)


    def _default_value(self, error=None):
        cr, uid, context, registry = request.cr, request.uid, request.context, request.registry

        orm_country = registry.get('res.country')
        country_ids = orm_country.search(cr, SUPERUSER_ID, [('code', '=', 'VN')], context=context)

        orm_city = registry.get('feosco.city')
        city_ids = orm_city.search(cr, SUPERUSER_ID, [('country_id', 'in', country_ids)], context=context)
        cities = orm_city.browse(cr, SUPERUSER_ID, city_ids, context=context)

        orm_district = registry.get('feosco.district')
        district_ids = orm_district.search(cr, SUPERUSER_ID, [('city_id', 'in', city_ids)], context=context)
        districts = orm_district.browse(cr, SUPERUSER_ID, district_ids, context=context)

        orm_custom = registry.get('feosco.department')
        custom_ids = orm_custom.search(cr, SUPERUSER_ID, [('type', '=', 'cuc_hq')], context=context)
        customs = orm_custom.browse(cr, SUPERUSER_ID, custom_ids, context=context)

        orm_title = registry.get('res.partner.title')
        title_ids = orm_title.search(cr, SUPERUSER_ID, [])
        titles = orm_title.browse(cr, SUPERUSER_ID, title_ids)

        orm_user = registry.get('res.users')
        current_users = orm_user.browse(cr, SUPERUSER_ID, uid)

        return {
            'cities': cities,
            'districts': districts,
            'customs': customs,
            'titles': titles,
            'company': current_users.partner_id,
            'current_date': time.strftime('Ngày  %d tháng %m năm %Y'),
            'error': {}
        }

    @http.route(['/thong_tin_doanh_nghiep'], type='http', auth="public", website=True, multilang=True)
    def thong_tin_doanh_nghiep(self):
        self.__logger.info('==> begin registration_page()')
        vals = self._default_value()
        self.__logger.info('==> end registration_page()')
        return request.website.render("digital_website.thong_tin_doanh_nghiep", vals)


    def _create_partner(self, contact, company):
        cr, context, registry = request.cr, request.context, request.registry
        orm_partner = registry.get('res.partner')
        company_id = False
        contact_id = False
        company_ref = company.get('ref', False)
        # create company
        if company_ref:
            company_ids = orm_partner.search(cr, SUPERUSER_ID, [('ref', '=', company_ref)])
            if not company_ids:
                company_id = orm_partner.create(cr, SUPERUSER_ID, company, context=context)
            else:
                company_id = company_ids[0]
        # Create contact
        if company_id:
            contact.update({'parent_id': company_id})
            contact_ids = orm_partner.search(cr, SUPERUSER_ID,
                                             [('email', '=', contact.get('email')), ('type', '=', 'contact')])
            if not contact_ids:
                contact_id = orm_partner.create(cr, SUPERUSER_ID, contact, context=context)
            else:
                orm_partner.write(cr, SUPERUSER_ID, contact_ids, {'parent_id': contact.get('parent_id')},
                                  context=context)
                contact_id = contact_ids[0]

        return company_id, contact_id

    @http.route(['/create_partner'], type='http', auth="public", website=True, multilang=True)
    def create_partner(self, **post):

        cr, context, registry = request.cr, request.context, request.registry

        self.__logger.info('===>begin create_partner')
        self.__logger.info('--> post data')
        self.__logger.info(post)

        orm_country = registry.get('res.country')
        country_ids = orm_country.search(cr, SUPERUSER_ID, [('code', '=', 'VN')], context=context)
        company = {
            'ref': post.get('company_ref'),
            'name': post.get('company_name'),
            'email': post.get('company_email'),
            'phone': post.get('company_phone'),
            'fax': post.get('company_fax'),
            'street': post.get('company_address'),
            'district_id': post.get('company_district'),
            'city_id': post.get('company_city'),
            'phone': post.get('company_phone'),
            'mobile': post.get('company_mobile'),
            'fax': post.get('company_fax'),
            'email': post.get('company_email'),
            'country_id': country_ids[0] if country_ids else False,
            'type': 'default',
            'is_company': True,
        }

        contact = {
            'name': post.get('company_contact'),
            'email': post.get('company_contact_email'),
            'mobile': post.get('company_contact_mobile'),
            'type': 'contact',

        }

        company_id, contact_id = self._create_partner(contact, company)

        self.__logger.info('===>end create_partner')
        return request.redirect('/dang_ky_to_khai/' + str(company_id))


    @http.route(['/dang_ky_to_khai/'], type='http', auth="public", website=True, multilang=True)
    def dang_ky_to_khai(self, *args, **kw):
        self.__logger.info('BEGIN dang_ky_to_khai')
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        vals = self._default_value()
        if not request.session.uid and request.httprequest.method == 'GET':
            self.__logger.info('reject to signup page, uid login public')
            return http.local_redirect('/web/login?redirect=/dang_ky_to_khai')

        self.__logger.info('END dang_ky_to_khai')
        return request.website.render("digital_website.feosco_registration_template", vals)


    @http.route(['/nop_to_khai'], type='http', auth="public", website=True, multilang=True)
    def create_dispatch(self, **post):
        uid, cr, context, registry = request.uid, request.cr, request.context, request.registry

        self.__logger.info('===>begin create_dispatch')
        self.__logger.info('--> post data')
        self.__logger.info(post)

        orm_country = registry.get('res.country')
        orm_warehouse = registry.get('feosco.digital.warehouse')
        orm_department = registry.get('feosco.department')

        user = registry['res.users'].browse(cr, SUPERUSER_ID, uid, context=context)
        country_ids = orm_country.search(cr, SUPERUSER_ID, [('code', '=', 'VN')], context=context)

        requisition_id = None
        self.__logger.info(user)
        self.__logger.info(user.partner_id)
        if user.id == 3:
            return request.website.render("digital_website.feosco_registration_template", {
                'error': u'1. Phải đăng nhập hệ thống để khai báo hoặc đang ký tài khoản Doanh Nghiệp'})
        if user.partner_id:
            customer = user.partner_id
            warehouse = {
                'name': u'(chưa cập nhật)', # post.get('warehouse_name'), # Phan name bo trong de cuc HQ tu dien vao (requirement nhu vay)
                'street1': post.get('warehouse_address'),
                'district_id': post.get('warehouse_district'),
                'city_id': post.get('warehouse_city'),
                'country_id': country_ids[0] if country_ids else False,
                'superficies': post.get('warehouse_area'),
                'assets': post.get('warehouse_equipment'),
                'customer_id': customer.id,
                'is_employ': True if post.get('warehouse_status_owner') in (False, 1) else 2,
                'owner': post.get('warehouse_owner')
            }
            warehouse_id = orm_warehouse.create(cr, SUPERUSER_ID, warehouse, context=context)

            # Auto fill customs_id to requisition
            # city_id = int(post.get('warehouse_city'))
            # custom_ids = orm_department.search(cr, SUPERUSER_ID, [('type','=','cuc_hq'),('city_id','=',city_id)])

            requisition = {
                'name': u'(chưa cập nhật)', # post.get('warehouse_name'), # Phan name bo trong de cuc HQ tu dien vao (requirement nhu vay)
                'warehouse_id': warehouse_id,
                'customs_id': post.get('custom'),
            }

            requisition_id = request.registry['feosco.digital.requisition'].create(cr, SUPERUSER_ID, requisition,
                                                                                   context=context)

        self.__logger.info('===>end create_dispatch')
        return request.redirect('/sign_dispatch/' + str(requisition_id))


    @http.route(['/sign_dispatch/<int:requisition_id>'], type='http', auth="public", website=True, multilang=True)
    def sign_dispatch(self, requisition_id=None):
        try:
            self.__logger.info('BEGIN post data to sign_dispatch')
            if requisition_id:
                vals = {}
                report_obj = request.registry['report']
                cr, uid, context = request.cr, request.uid, request.context
                data = {'ids': [requisition_id]}
                pdf = report_obj.get_pdf(cr, 1, [requisition_id], 'digital_requisition.pdf_generatior', data=data,
                                         context=None)
                base64String = base64.b64encode(pdf)
                self.__logger.info(base64String)
                vals.update({'pdf_base64': base64String, 'requisition_id': requisition_id})
                self.__logger.info('END sign_dispatch')
                return request.website.render("digital_website.signing2", vals)
            else:
                return request.registry['ir.http']._handle_exception("Không tìm thấy văn bản, vui lòng nhập tờ khai")

        except Exception, exc:
            return request.registry['ir.http']._handle_exception(exc)


    @http.route(['/create_dispatch'], type='http', auth="public", website=True, multilang=True)
    def requisition_signed(self, **post):
        cr, context, registry = request.cr, request.context, request.registry

        self.__logger.info('===>BEGIN requisition_signed')
        self.__logger.info('--> post data')

        vals = {}
        requisition_id = post['requisition_id']
        if post and post.has_key('pdfSignedBase64'):

            dispatch_value = {
                'name': requisition_id + u'.pdf',
                'res_name': "donxincapmadiadiem",
                'res_model': 'feosco.digital.requisition',
                'res_id': requisition_id,
                'datas': post.get('pdfSignedBase64'),
                'datas_fname': requisition_id + u'.pdf',
            }
            dispatch_id = request.registry['ir.attachment'].create(cr, SUPERUSER_ID, dispatch_value, context=context)
            vals.update({'file_id': dispatch_id})
            self.__logger.info('===>END requisition_signed')
            return request.website.render("digital_website.thank_you", vals)
        else:
            vals = {}
            report_obj = request.registry['report']
            cr, uid, context = request.cr, request.uid, request.context
            data = {'ids': [requisition_id]}
            pdf = report_obj.get_pdf(cr, 1, [requisition_id], 'digital_requisition.pdf_generatior', data=data, context=None)
            base64String = base64.b64encode(pdf)
            vals.update({'pdf_base64': base64String, 'requisition_id': requisition_id,
                         'error': 'Vui lòng ký File để hoàn tất quá trình đăng ký.'})
            self.__logger.error('===>END requisition_signed')
            return request.website.render("digital_website.sign_dispatch", vals)


    @http.route(['/view_signed'], type='http', auth="public", website=True, multilang=True)
    def view_signed(self, **post):
        self.__logger.info('BEGIN view_signed')
        if post and post.has_key('pdfSignedBase64'):
            vals_decode = post.get('pdfSignedBase64').decode('utf-8')
            print vals_decode
            pdfhttpheaders = [('Content-Type', 'application/pdf'), ('Content-Length', len(vals_decode))]
            self.__logger.info('END view_signed')
            return request.make_response(vals_decode, headers=pdfhttpheaders)

        else:
            self.__logger.info('END view_signed')
            return request.website.render("digital_website.sign_dispatch", {})


    @http.route(['/call_orm'], type='http', auth="public", website=True, multilang=True)
    def call_orm(self, **post):
        self.__logger.info('BEGIN call_orm')
        self.__logger.info(post)
        self.__logger.info('END call_orm')
        return json.dumps({'id': 1, 'name': 'number one'})


    @http.route(['/filter_district'], type='http', auth="public", website=True, multilang=True)
    def filter_district(self, **post):
        self.__logger.info('BEGIN filter_district')
        self.__logger.info(post)
        district_orm = request.registry['feosco.district']
        cr, uid, context = request.cr, request.uid, request.context
        district_ids = district_orm.search(cr, uid, [('city_id', '=', (int(post.get('city_id'))))])
        if district_ids:
            district_val = district_orm.read(cr, uid, district_ids, ['id', 'name'])
            self.__logger.info(district_val)
            self.__logger.info('END filter_district')
            return json.dumps(district_val)


    @http.route(['/my_warehouse'], type='http', auth="public", website=True, multilang=True)
    def get_warehouse_by_user(self):
        self.__logger.info('==> begin get_warehouse_by_user()')

        if not request.session.uid and request.httprequest.method == 'GET':
            self.__logger.info('reject to signup page, uid login public')
            return http.local_redirect('/web/login?redirect=/my_warehouse')

        values = {'warehouses': []}
        cr, uid, context, registry = request.cr, request.uid, request.context, request.registry
        requisition_orm = request.registry['feosco.digital.requisition']
        warehouse_orm = request.registry['feosco.digital.warehouse']

        user_obj = request.registry['res.users'].browse(cr, SUPERUSER_ID, uid, context=None)
        partner_id = user_obj.partner_id.id if user_obj.partner_id else False
        if partner_id:
            warehouse_ids = warehouse_orm.search(cr, SUPERUSER_ID, [('customer_id', '=', partner_id)], context=None)
            if warehouse_ids:
                warehouses = warehouse_orm.browse(cr, SUPERUSER_ID, warehouse_ids, context=None)
                values.update({'warehouses': warehouses})

        self.__logger.info('==> end get_warehouse_by_user()')
        self.__logger.info(values)
        return request.website.render("digital_website.warehouse_list", values)





