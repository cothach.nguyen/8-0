# -*- coding: utf-8 -*-
import base64

from openerp import SUPERUSER_ID
from openerp.addons.web import http
from openerp.addons.web.http import request
import json
import logging

_ticket_per_page = 1  # ticket Per Page


class website_helpdesk(http.Controller):
    __logger = logging.getLogger(__name__)


    def _default_value(self, error=None):
        cr, uid, context, registry = request.cr, request.uid, request.context, request.registry

        orm_title = registry.get('res.partner.title')
        title_ids = orm_title.search(cr, SUPERUSER_ID, [], context=context)
        titles = orm_title.browse(cr, SUPERUSER_ID, title_ids, context=context)

        orm_custom = registry.get('feosco.department')
        custom_ids = orm_custom.search(cr, SUPERUSER_ID, [('type','=','cuc_hq')], context=context)
        customs = orm_custom.browse(cr, SUPERUSER_ID, custom_ids, context=context)

#         orm_branch = registry.get('feosco.department')
#         branch_ids = orm_branch.search(cr, SUPERUSER_ID, [('type','=','chi_cuc_hq')], context=context)
#         branches = orm_branch.browse(cr, SUPERUSER_ID, branch_ids, context=context)

        orm_categ = registry.get('crm.case.categ')
        categ_ids = orm_categ.search(cr, SUPERUSER_ID, [('object_id.model', '=', 'crm.claim')], context=context)
        categs = orm_categ.browse(cr, SUPERUSER_ID, categ_ids, context=context)

        orm_business_type = registry.get('feosco.master.data')
        business_type_ids = orm_business_type.search(cr, SUPERUSER_ID, [('type', '=', 'business_type')],
                                                     context=context)
        business_type = orm_business_type.browse(cr, SUPERUSER_ID, business_type_ids, context=context)

        orm_method_connect = registry.get('feosco.master.data')
        method_connect_ids = orm_method_connect.search(cr, SUPERUSER_ID, [('type', '=', 'method_connect')],
                                                       context=context)
        method_connect = orm_business_type.browse(cr, SUPERUSER_ID, method_connect_ids, context=context)

        orm_partner = registry.get('res.partner')
        orm_country = registry.get('res.country')
        country_ids = orm_country.search(cr, SUPERUSER_ID, [], context=context)
        countries = orm_country.browse(cr, SUPERUSER_ID, country_ids, context)
        partner_ids = orm_partner.search(cr, SUPERUSER_ID, [], context=context)
        partners = orm_partner.browse(cr, uid, partner_ids, context=context)
        state_orm = registry.get('res.country.state')
        states_ids = state_orm.search(cr, SUPERUSER_ID, [], context=context)
        states = state_orm.browse(cr, SUPERUSER_ID, states_ids, context)
        return {
            'countries': countries,
            'partners': partners,
            'states': states,
            'customs': customs,
             'branches': [],
            'categs': categs,
            'business_types': business_type,
            'method_connects': method_connect,
            'titles': titles,
            'error': {},
        }

    @http.route(['/helpdesk'], type='http', auth="public", website=True, multilang=True)
    def helpdesk(self):
        self.__logger.info('==> begin helpdesk()')
        uid = request.uid
#         if uid == 3:
#             return request.redirect('/web/login')
        if not request.session.uid:
            return http.local_redirect('/web/login?redirect=/helpdesk')
        vals = self._default_value()
        self.__logger.info('==> end helpdesk()')
        return request.website.render("digital_website_helpdesk.helps_desk", vals)
    
    @http.route(['/chi_cuc_hq'], type='http', auth="public", website=True, multilang=True)
    def get_chicuchq_by_cuchq(self, **post):
        self.__logger.info('BEGIN get_chicuchq_by_cuchq')
        self.__logger.info(post)
        cr, uid, context, registry = request.cr, request.uid, request.context, request.registry
        
        orm_branch = registry.get('feosco.department')
        cuc_hq_id = post.get('cuc_hq_id',False)
        if cuc_hq_id:
            cuc_hq_id = int(cuc_hq_id)
            branch_ids = orm_branch.search(cr, SUPERUSER_ID, [('type','=','chi_cuc_hq'),('parent_id','=',cuc_hq_id)], context=context)
            if branch_ids:
                branches = orm_branch.read(cr, uid, branch_ids, ['id', 'name'])
                self.__logger.info(branches)
                self.__logger.info('END get_chicuchq_by_cuchq')
                return json.dumps(branches)
            else:
                return json.dumps([])
        else:
            return json.dumps([])
    
    def _create_partner(self, contact, company):
        cr, context, registry = request.cr, request.context, request.registry
        orm_partner = registry.get('res.partner')
        company_id = None
        if company and company.has_key('ref') and company.get('ref'):
            company_ids = orm_partner.search(cr, SUPERUSER_ID, [('ref', '=', company.get('ref'))])
            if not company_ids:
                company_id = orm_partner.create(cr, SUPERUSER_ID, company, context=context)
            else:
                company_id = company_ids[0]
        if company_id:
            contact.update({'parent_id': company_id})
            contact_ids = orm_partner.search(cr, SUPERUSER_ID, [('email', '=', contact.get('email'))])
            if not contact_ids:
                return orm_partner.create(cr, SUPERUSER_ID, contact, context=context)
            else:
                orm_partner.write(cr, SUPERUSER_ID, contact_ids,
                                  {'parent_id': contact.get('parent_id'),
                                   'feosco_business_type': contact.get('feosco_business_type')}, context=context)
                return contact_ids[0]

    @http.route(['/thank_you'], type='http', auth="public", website=True, multilang=True)
    def send_helpdesk(self, **post):

        cr, uid, context, registry = request.cr, request.uid, request.context, request.registry
        
        self.__logger.info('===>begin send_helpdesk')
        self.__logger.info('--> post data')
        self.__logger.info(post)
#         company = {
#             'ref': post.get('company_ref'),
#             'name': post.get('company_name'),
#             'email': post.get('company_email'),
#             'phone': post.get('company_phone'),
#             'fax': post.get('company_fax'),
#             'is_company': True,
#             'feosco_business_type': int(post.get('business_type')) if post.get('business_type', False) else None
#         }
# 
#         contact = {
#             'name': post.get('contact_name'),
#             'title': post.get('contact_title'),
#             'mobile': post.get('contact_mobile'),
#             'email': post.get('contact_email'),
#             'feosco_business_type': int(post.get('business_type')) if post.get('business_type', False) else None
#         }
# 
#         orm_user = registry.get('res.users')
# 
#         user_ids = orm_user.search(cr, SUPERUSER_ID, [('login', '=', post.get('contact_email'))])
# 
#         if user_ids:
#             user = orm_user.browse(cr, SUPERUSER_ID, user_ids[0])
#             self.__logger.info('--> user.password: %s' % user.password)
#             self.__logger.info('--> password fontend user input: %s' % post.get('password'))
#             if user.password != post.get('password'):
#                 post['error'] = u"Sai Email Đăng Ký cá nhân hoặc password, Vui lòng nhập lại."
#                 del post['password']
#                 del post['confirm_password']
#                 del post['contact_email']
#                 vals = self._default_value()
#                 vals.update(post)
#                 self.__logger.info('===>error, auto reloading first page')
#                 return request.website.render('website_helpdesk.helps_desk', vals)
#         else:
#             orm_user.signup(cr, SUPERUSER_ID, {
#                 'login': post.get('contact_email'),
#                 'password': post.get('password'),
#                 'name': post.get('contact_name')
#             })

#         partner_id = self._create_partner(contact, company)

        # Get partner id by uid
        user = registry.get('res.users').browse(cr, SUPERUSER_ID, uid, context=context)
        partner_id = user.partner_id.id
        
        self.__logger.info('--> partner_id %s' % partner_id)
        
        post.update(
            {'partner_id': partner_id,'user_id': None})
        orm_claim = registry.get('crm.claim')
        ticket_id = orm_claim.create(cr, SUPERUSER_ID, post, context=context)

        if ticket_id and post['ufile']:
            attachment_value = {
                'name': post['ufile'].filename,
                'res_name': post['name'],
                'res_model': 'crm.claim',
                'res_id': ticket_id,
                'datas': (post['ufile'].read()).encode('base64'),  #base64.encodestring(post['ufile'].read()),
                'datas_fname': post['ufile'].filename,
            }
            request.registry['ir.attachment'].create(cr, SUPERUSER_ID, attachment_value, context=context)

        self.__logger.info('===>end send_helpdesk')
        return request.website.render("digital_website_helpdesk.thank_you", {})

    @http.route(['/search_ticket'], type='http', auth="public", website=True, multilang=True)
    def search_ticket(self):
        self.__logger.info('==> begin helpdesk()')
        self.__logger.info('==> end helpdesk()')
        return request.website.render("digital_website_helpdesk.search_ticket")

    def _get_ticket_per_page_config(self):
        ticket_per_page = request.registry['ir.config_parameter'].get_param(request.cr, SUPERUSER_ID, 'ticket_per_page')
        return int(ticket_per_page) if int(ticket_per_page) > 0 else 1

    @http.route(['/all_ticket',
                 '/all_ticket/page/<int:page>'
                ], type='http', auth="public", website=True, multilang=True)
    def all_ticket(self, ticket_code=None, customer_code=None, page=1):
        self.__logger.info('==> begin all_ticket()')
        if type(page) != int:
            page = int(page)
        ticket_pool = request.registry['crm.claim']
        url = "/all_ticket"
        ticket_per_page = self._get_ticket_per_page_config()

        search_agrs = []
        if ticket_code:
            search_agrs.append(('feosco_code', '=', ticket_code))
        if customer_code:
            search_agrs.append(('partner_id.ref', '=', customer_code))
        self.__logger.info('Search Agrs = %s' % search_agrs)
        # Pager
        total = ticket_pool.search(request.cr, SUPERUSER_ID, search_agrs, count=True)
        pager = request.website.pager(
            url=url,
            total=total,
            page=page,
            step=ticket_per_page,
        )
        ticket_ids = ticket_pool.search(request.cr, SUPERUSER_ID, search_agrs, offset=(page - 1) * ticket_per_page,
                                        limit=ticket_per_page)
        if ticket_ids:
            ticket_obj = ticket_pool.browse(request.cr, SUPERUSER_ID, ticket_ids)
            values = {
                'tickets': ticket_obj,
                'ticket_code': ticket_code,
                'customer_code': customer_code,
                'pager': pager
            }
        else:
            values = {
                'tickets': [],
                'pager': pager
            }
        self.__logger.info('==> end all_ticket()')
        return request.website.render("digital_website_helpdesk.ticket_list", values)

    @http.route(['/ticket/<model("crm.claim"):ticket>'], type='http', auth="public", website=True, multilang=True)
    def ticket_detail(self, ticket, ticket_code=None, customer_code=None, page=1):
        self.__logger.info('==> begin ticket_detail()')
        self.__logger.info('Ticket = %s' % ticket)
        values = {}
        ticket_pool = request.registry['crm.claim']
        if ticket:
            values = {
                'ticket': ticket,
                'ticket_code': ticket_code,
                'customer_code': customer_code,
                'page': page
            }
            self.__logger.info('==> end ticket_detail()')
            self.__logger.info(values)
            return request.website.render("digital_website_helpdesk.ticket_details", values)
        else:
            self.__logger.info('==> No ticket Found')
            self.__logger.info('==> end ticket_detail()')
            return "<h1>Ticket not found</h1>"






