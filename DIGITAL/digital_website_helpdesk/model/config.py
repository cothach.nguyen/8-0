# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Business Applications
#    Copyright (C) 2004-2012 OpenERP S.A. (<http://openerp.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv


class website_helpdesk_config_settings(osv.osv_memory):
    _name = 'claim.config.settings'
    _inherit = 'claim.config.settings'
    _columns = {
        'ticket_per_page': fields.integer('Ticket Per Page'),
    }

    def get_default_ticket_per_page(self, cr, uid, fields, context=None):
        deadline_config = self.pool['ir.config_parameter'].get_param(cr, uid, 'ticket_per_page')
        return {'ticket_per_page': int(deadline_config)}

    def set_default_ticket_per_page(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ticket_per_page = int(config.ticket_per_page)
        if int(ticket_per_page) < 1:
            ticket_per_page = 10
        self.pool['ir.config_parameter'].set_param(cr, uid, 'ticket_per_page', ticket_per_page,
                                                   context=context)

        # vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: