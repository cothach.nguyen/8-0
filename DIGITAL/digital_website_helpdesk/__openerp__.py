{
    'name': 'FEOS: HelpDesk',
    'category': 'Website',
    'version': '1.0',
    'summary': 'HelpDesk Forms',
    'description': """""",
    'author': 'Feosco',
    'website': 'http://www.feosco.com',
    'depends': ['website', 'feosco_base', 'digital_crm_claim'],
    'data': [
        'views/template.xml',
        'views/main.xml',
        'views/config_view.xml'
    ],

    'installable': True,
}
