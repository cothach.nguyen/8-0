# -*- coding: utf-8 -*-
{
    'name': 'FEOSCO digital warehouse',
    'version': '0.1',
    'author': 'FEOSCO',
    'category': 'FEOSCO',
    'website': 'http://www.feosco.com',
    'images': [],
    'depends': ['base', 'digital_base'],
    'data': [
        'security/ir.model.access.csv',
        'view/digital_warehouse_view.xml',
        'view/partner.xml',
    ],
    'installable': True,
    'auto_install': False,
}
