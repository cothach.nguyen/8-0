#-*- coding:utf-8 -*-
from openerp.osv import osv, fields

class res_partner(osv.Model):

    _inherit = "res.partner"

    _columns = {
        'feosco_warehouse_ids': fields.one2many('feosco.digital.warehouse', 'customer_id', u'Kho công ty'),
    }