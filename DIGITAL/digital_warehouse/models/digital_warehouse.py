#-*- coding: utf-8 -*-
from openerp.osv import osv, fields
import urllib2, urllib, StringIO, json

class feosco_digital_warehouse(osv.Model):

    _name = "feosco.digital.warehouse"
    
    def _get_country_id(self, cr, uid, ids, context=None):
        search_country=[('code','=','VN')]
        country_ids = self.pool.get('res.country').search(cr, uid, search_country)
        return country_ids[0] if country_ids else False
    
    def event_country_change(self, cr, uid, ids, country_id, context={}):
        if country_id:
            return {'value': {'city_id': None, 'district_id': None}}

    def event_city_change(self, cr, uid, ids, city_id, context={}):
        if city_id:
            return {'value': {'district_id': None}}
        else:
            return {}

    def _get_lat_long_gmap(self, cr, uid, ids, name, arg, context=None):

        url = "http://maps.google.com/maps/api/geocode/json?"

        res = {}
        if not ids:
            return res
        else:
            for this in self.browse(cr, uid, ids):
                res[this.id] = {
                    'provider_latitude': 0.00,
                    'provider_longitude': 0.00,
                }
                street = this.street1.encode('utf-8') if this.street1 else ''
                city = this.city_id.name.encode('utf-8') if this.city_id else ''
                district = this.district_id.name.encode('utf-8') if this.district_id else ''
                country = this.country_id.name.encode('utf-8') if this.country_id else ''
                address_full = ''
                if street:
                    address_full += street
                if district:
                    if not address_full:
                        address_full += district
                    else:
                        address_full += ','
                        address_full += district
                if city:
                    if not address_full:
                        address_full += city
                    else:
                        address_full += ','
                        address_full += city
                if country:
                    if not address_full:
                        address_full += country
                    else:
                        address_full += ','
                        address_full += country
                address_full += ','

                urlParams = {
                    'address': address_full,
                    'sensor': 'false',
                }
                url = 'http://maps.google.com/maps/api/geocode/json?' + urllib.urlencode( urlParams )
                response = urllib2.urlopen(url)
                responseBody = response.read()

                body = StringIO.StringIO(responseBody)
                result = json.load(body)

                if 'status' not in result or result['status'] != 'OK':
                    print '---false get data from gooogle map-----'
                    pass
                else:
                    print result['results'][0]['geometry']['location']['lat']
                    print result['results'][0]['geometry']['location']['lng']
                    res[this.id] = {
                        'provider_latitude': result['results'][0]['geometry']['location']['lat'],
                        'provider_longitude': result['results'][0]['geometry']['location']['lng']
                    }
            return res
    
    
    _columns = {
        'name': fields.char(u'Tên'),
        'customer_id':fields.many2one('res.partner', u'Doanh nghiệp'),
        'is_employ': fields.boolean(u'Kho công ty ?'),
        'owner': fields.char(u'Người quản lý'),
        
        'street1': fields.char(u'Địa chỉ'),
        'street2': fields.char(u'Địa chỉ phụ'),
        'district_id': fields.many2one('feosco.district', u'Quận (huyện)', domain="[('city_id', '=', city_id)]"),
        'city_id': fields.many2one('feosco.city', u'Tỉnh / Thành'),
        'country_id': fields.many2one('res.country', u'Quốc gia'),
        
        'superficies': fields.float(u'Diện tích'),
        'assets': fields.text(u'Tình trạng kho'),
        
        'state': fields.selection([
            ('new', u'Yêu cầu mới'),
            ('assigned', u'Chờ chi cục duyệt'),
            ('approved1', u'Chờ giám sát duyệt'),
            ('approved2', u'Chờ tổng cục duyệt và cấp mã'),
            ('approved3', u'Đã cấp mã'),
            ('cancel', u'Từ chối')
            ],
            string = u'Trạng thái'),
                
        'max_quantity': fields.float(string="Max Quantity"),
        'provider_latitude': fields.function(_get_lat_long_gmap, type='float', digits=(12,6), multi='_get_latlong', string=u'Kinh độ'),
        'provider_longitude': fields.function(_get_lat_long_gmap, type='float', digits=(12,6), multi='_get_latlong', string=u'Vĩ độ'),
        
        'code': fields.char(u'Mã địa điểm'),
    }

    _defaults = {
        'state': 'new',
        'country_id': _get_country_id,
        'is_employ': True
    }
  
