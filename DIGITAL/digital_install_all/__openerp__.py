{
    'name': 'FEOSCO: Digital Signature Applications',
    'version': '0.1',
    'summary': 'Digital Install All Module Depends',
    'sequence': '19',
    'category': 'FEOSCO',
    'complexity': 'easy',
    'description': "",
    'data': [],
    'depends': [
        'digital_base',
        'digital_website_helpdesk',
        'digital_website',
        'digital_warehouse',
        'digital_signup',
        'digital_requisition',
        'digital_gmap',
        'digital_custom',
    ],

    'qweb': [

        ],
    'installable': True,
    'auto_install': True,
    'application': True,
}
